From stdpp Require Import prelude options.

Inductive phantom (T : Type) (t : T) : Type :=
  Phantom.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Coercion Z.of_nat : nat >-> Z.

(* ziplist applicative *)
Definition zip_apply A B (fs : list (A → B)) (xs : list A) : list B :=
	zip_with ($) fs xs.

(* Should be in lib *)
Lemma tail_length A (l : list A) : length (tail l) = pred (length l).
Proof. by destruct l. Qed.

Lemma nth_lookup_total A `{Inhabited A} (l : list A) i :
	nth i l inhabitant = l !!! i.
Proof. induction l in i |- *; destruct i; simpl; done. Qed.

Lemma list_omap_filter A B (f : A -> option B) (g : B -> A) :
	(∀ a, from_option g a (f a) = a) →
	∀ (l : list A), g <$> omap f l = filter (is_Some ∘ f) l.
Proof.
	intros Cancel l.
	induction l as [| a l IHl]; [done|].
	specialize (Cancel a).
	unfold filter, list_filter.
	remember (filter (is_Some ∘ f) l) as l' eqn:Hl'; rewrite <- IHl; clear IHl Hl' l'.
	simpl. destruct (f a) eqn:H; simpl; [|done].
	eauto with f_equal.
Qed.

Lemma list_total_eq A `{Inhabited A} (l1 l2 : list A)  :
	length l1 = length l2 →
	(∀ i, l1 !!! i = l2 !!! i) →
	l1 = l2.
Proof.
	intros Hl Hi. eapply list_eq_same_length; [done..|].
	intros. specialize (Hi i).
	by erewrite 2 list_lookup_total_correct in Hi.
Qed.

Lemma list_lookup_total_ge {A} `{Inhabited A} (l : list A) i :
	length l ≤ i →
	l !!! i = inhabitant.
Proof. intros Hl. by rewrite list_lookup_total_alt, lookup_ge_None_2. Qed.

Lemma filter_Forall A (P : A → Prop) l {PDec : ∀ x, Decision (P x)} :
		Forall P l → filter P l = l.
	Proof.
		intros H.
		induction l; [done|]; inversion_clear H.
		rewrite filter_cons_True; eauto using f_equal.
	Qed.

#[export] Instance Z_inhabited : Inhabited Z := populate 0%Z.

Section IndexDef.
  Variable b : nat.

  Structure index := Index {
    nat_of_index :> nat ;
    index_lt : nat_of_index < b ; (* is_true here? Or maybe Is_true? *)
  }.

  Implicit Type i : index.

  Lemma index_eq {i1 i2} : i1 =@{nat} i2 → i1 = i2.
  Proof.
    destruct i1 as [? H1], i2 as [? H2]; simpl; intros ->.
    by rewrite (nat_lt_pi _ _ H1 H2).
  Qed.

  Global Instance index_eq_dec : EqDecision index.
  Proof.
    intros i1 i2. destruct (nat_eq_dec i1 i2); [left | right].
    - by apply index_eq.
    - intros H. simplify_eq.
  Qed.

  Definition mk_index (n : nat) (H : n <? b = true) : index :=
    Index ltac:(by apply Nat.ltb_lt).

  Lemma mk_indexE (n : nat) H : @mk_index n H =@{nat} n.
  Proof. reflexivity. Qed.

  Lemma S_indexP i : S i < S b.
  Proof. apply lt_n_S, index_lt. Qed.

	Definition index_option (k : nat) : option index.
	Proof.
		(* pose proof (f := @mk_index k). *)
		destruct (k <? b) eqn:H.
		- exact (Some (@mk_index k H)).
		- exact None.
	Defined.


	Print index_option.

	Lemma index_option_Some i k : index_option k = Some i → i =@{nat} k.
	unfold index_option.
	Fail destruct (k <? b).
	Admitted.

	Lemma index_option_Some_lt1 k : is_Some $ index_option k → k < b.
	Proof. Admitted.

	Lemma index_option_Some_lt2 k : k < b → ∃ k', index_option k = Some k' /\ nat_of_index k' = k.
	Proof. Admitted.

	Lemma cancel1 i : index_option (nat_of_index i) = Some i.
	Proof.
		destruct i as [i H]; simpl.
		unfold index_option.
		assert (i <? b = true); [by apply Nat.ltb_lt|].

		Fail rewrite H0.
	Admitted.

	Lemma cancel2 (k : nat) : from_option nat_of_index k (index_option k) = k.
	Proof.
		destruct (index_option k) eqn:H; simpl; [|done].
		by apply index_option_Some.
	Qed.

	Definition index_enum : list index :=
		omap index_option (seq 0 b).

End IndexDef.

Arguments Index [b i] : rename.
Arguments nat_of_index {b}.

Notation "''I_' n" := (index n)
  (at level 8, n at level 2, format "''I_' n").

Check @mk_index 10 2 eq_refl.

Global Hint Resolve index_lt | 0 : core.

Canonical S_index b (i : index b) : index (S b) :=
  Index (S_indexP i).

(* Make canonical? *)
Canonical index_bump {b} (i : index b) : index (S b).
Proof. apply Index with i. by apply Nat.lt_lt_succ_r. Defined.

(* Canonical index_le {b1 b2} (H : b1 <= b2) (i : index b1) : index b2.
Proof. apply Index with i. eapply lt_le_trans; done. Defined. *)

Definition index_plus_bump {b x} (i : index b) : index (b + x).
Proof. apply Index with i. by apply Nat.lt_lt_add_r. Defined.

Definition index_plus {b x} (i : index b) : index (x + b).
Proof. apply Index with (x + i). by apply plus_lt_compat_l. Defined.

Canonical index_pred b (i : index b) : index b.
	apply Index with (pred i).
	by apply Nat.lt_lt_pred.
Defined.

Example index_compose_pred b (i : index b) : pred (pred i) < b.
Proof. done. Qed.

Definition ind0 {n} : 'I_(S n) := Index (Nat.lt_0_succ n).
Definition ind_max {n} : 'I_(S n) := Index (Nat.lt_succ_diag_r n).
Definition ind_default0 {n} k : 'I_(S n) := default ind0 (index_option _ k).

Compute (ind_default0 3 : 'I_6) : nat.

Lemma index_0_false (i : 'I_0) : False.
Proof. inversion i. lia. Qed.

Lemma val_index_enum b : nat_of_index <$> index_enum b = seq 0 b.
	Proof.
		unfold index_enum.
		rewrite list_omap_filter; [| apply cancel2 ].
		apply filter_Forall, Forall_seq; simpl; intros j [_ H].
		by edestruct index_option_Some_lt2 as (? & ? & ?); [done|].
	Qed.

	Lemma index_enum_length b : length (index_enum b) = b.
	Proof.
		rewrite <- (fmap_length nat_of_index _), val_index_enum.
		apply seq_length.
	Qed.

Lemma index_enum_In b (i : 'I_b) : In i (index_enum b).
Proof.
	destruct i as [i lei]. induction b in i, lei |- *.
	{ by edestruct Nat.nlt_0_r. }
	destruct i.
	+ left. by apply index_eq.
	+ right. unshelve epose proof (IHb i _). { by apply lt_S_n. } clear IHb.
Admitted.

Lemma index_enum_lookup n i : index_enum n !! i = index_option n i.
unfold index_enum.
Check list_lookup_alt.
unfold index_option.
Admitted.

Local Instance index_S_inhabited n : Inhabited 'I_(S n) := populate ind0.

Lemma index_enum_lookup_total n (i : 'I_(S n)) : index_enum (S n) !!! (nat_of_index i) = i.
Admitted.

Section vector.
  Variable A : Type.
  Variable n : nat.

  Structure vector := Vector {
    list_of_vector :> list A ;
    vector_length : length list_of_vector = n ;
	}.
	Arguments Vector l length_l : rename.

  Implicit Type v : vector.
	Implicit Type i : 'I_n.

  Lemma vector_eq v1 v2 : v1 =@{list A} v2 → v1 = v2.
  Proof.
    destruct v1 as [? H1], v2 as [? H2]; simpl; intros He.
    subst; f_equal. apply eq_pi, _.
  Qed.

  Global Instance vector_eq_dec `{EqDecision A} : EqDecision vector.
  Proof.
    intros [l1 H1] [l2 H2]. destruct (list_eq_dec l1 l2); [left | right].
    - by apply vector_eq.
    - intro; simplify_eq.
  Qed.

	(* Lemma vnth_default v : 'I_n → A.
	Proof.
		rewrite <-(vector_length v).
		destruct (list_of_vector v); simpl; [|done].
	  intros []. by edestruct Nat.nlt_0_r.
	Defined. *)

	(* Definition vnth v i := nth i v (vnth_default v i). *)

	(* Global Instance vector_lookup_total `{Inhabited A} : LookupTotal 'I_n A vector :=
		fun i v => (list_of_vector v) !!! (nat_of_index i). *)

	(* We should really follow math-comp and use vnth_default so
	   we don't need the Inhabited requirement. But since our
		 in our applications A will always be inhabited, we don't bother *)
	Definition vector_lookup_total `{Inhabited A} v i : A :=
		(list_of_vector v) !!! (nat_of_index i).
	
  (* Definition mk_vector (l : list A) (H : length l =? n = true) : vector :=
    Vector l ltac:(by apply Nat.eqb_eq). *)

	Definition phantom_vector {v} (H : phantom (list A) v) := v.

	Definition vectorize v mkT : vector :=
  	mkT (let '@Vector _ tP := v return length v = n in tP).

End vector.

Global Arguments list_of_vector {A n}.
Global Hint Resolve vector_length : core.

Notation "[ 'vec' 'of' v ]" := (phantom_vector (Phantom _ v))
  (at level 0, format "[ 'vec'  'of'  v ]").
Notation "[ 'vector2' 'of' v ]" := (vectorize (@Vector _ _ v))
  (at level 0, format "[ 'vector2'  'of'  v ]").
Notation "[ 'vec' x1 ; .. ; xn ]" := [vec of x1 :: .. [xn] ..]
  (at level 0, format "[ 'vec' '['  x1 ; '/'  .. ; '/'  xn ']' ]").
Notation "[ 'vec' ]" := [vec of []]
  (at level 0, format "[ 'vec' ]").
Notation "v !# i" := (vector_lookup_total v i)
	(at level 10). (* todo level? *)

(* need this? *)
Definition destruct_vector A n (v : vector A (S n)) : A * vector A n.
Proof.
	destruct v as [[|a l'] H]; [done|].
	injection H as H.
	split; [exact a | eapply Vector, H].
Defined.

Canonical nil_vector {T} : vector T 0 := Vector nil_length.

Canonical cons_vector n T (x : T) (v : vector T n) : vector T (S n).
Proof. apply Vector with (x :: v); simpl. by f_equal. Defined.

Canonical tail_vector A n (v : vector A n) : vector A (pred n).
Proof. apply Vector with (tail v). by rewrite tail_length; f_equal. Defined.

Canonical app_vector {A n m} (v : vector A n) (w : vector A m) : vector A (n + m).
Proof. apply Vector with (v ++ w). by rewrite app_length; f_equal. Defined.

Canonical drop_vector A n m (v : vector A (n + m)) : vector A m.
Proof. apply Vector with (drop n v). rewrite drop_length, vector_length. apply minus_plus. Defined.

Canonical take_vector A n m (v : vector A (n + m)) : vector A n.
Proof. apply Vector with (take n v). rewrite take_length, vector_length. auto with arith. Defined.

Canonical replicate_vector A (x : A) n : vector A n.
Proof. apply Vector with (replicate n x). apply replicate_length. Defined.

Canonical seq_vector (n start : nat) : vector nat n :=
	Vector (seq_length n start).

Canonical rotate_vector A n (k : nat) (v : vector A n) : vector A n.
Proof. apply Vector with (rotate k v). by rewrite rotate_length. Defined.

Canonical zip_with_vector A B C n (f : A → B → C)
	(v : vector A n) (w : vector B n) : vector C n.
Proof.
	apply Vector with (zip_with f v w).
	rewrite zip_with_length_r_eq; [done|].
	by trans n.
Defined.

Global Instance vector_insert A n : Insert 'I_n A (vector A n).
Proof. intros i a' v. apply Vector with (<[ (nat_of_index i) := a']> (list_of_vector v)).
	by rewrite insert_length. Defined.
Canonical vector_insert.

Local Instance vector_inhabited A `{Inhabited A} n : Inhabited (vector A n) :=
	populate [vec of replicate n inhabitant].

Global Instance vector_fmap {n} : FMap (λ A, vector A n).
Proof.
  intros A B f v.
  apply Vector with (f <$> (list_of_vector v)).
  by rewrite fmap_length.
Defined.

Canonical vector_fmap.

Check [vec of S <$> [1;2;3]].
Fail Check S <$> [vec 1;2;3]. (* sigh *)
Check @fmap _ vector_fmap _ _ S [vec 1;2;3].

Definition vec_of_fun A n (f : nat → A) : vector A n :=
	[vec of f <$> (seq 0 n)].

Compute vec_of_fun 10 (λ n, n) : list nat.

Canonical index_enum_vector n : vector 'I_n n :=
	Vector (@index_enum_length n).

Definition vec_of_finfun A n (f : 'I_n → A) : vector A n :=
	[vec of f <$> (index_enum n)].

Compute vec_of_finfun (λ i, [vec of seq 3 5] !# i) : list nat.
Compute (vec_of_fun 5 (λ i, i + 3)) !# (ind_default0 2).

Lemma vector_induction A (P : ∀ n, vector A n → Type) :
	P 0 nil_vector →
	(∀ a n (v : vector A n), P n v → P (S n) (cons_vector a v)) →
	∀ n (v : vector A n), P n v.
Proof.
	intros H0 HS n v.
	destruct v as [l Hl].
	induction l in n, Hl |- *; simpl in *; subst.
	- exact H0.
	- erewrite vector_eq.
		+ apply HS, IHl.
		+ done.
	Unshelve. done. (* ugly *)
Qed.

Section vector_recursion.
	Variable (A : Type) (B : nat → Type).
	Context `{∀ n, Inhabited (B n)}.
	Variable (Bnil : B 0).
	Variable (Bcons : A → ∀ n, list A → B n → B (S n)).

	Definition vector_recursion n (v : vector A n) : B n :=
		(fix go (n : nat) (v : list A) : B n :=
		match n, v with
		| S n, a :: v => Bcons a v (go n v)
		| 0, [] => Bnil
		| _, _ => inhabitant
		end) n (list_of_vector v).

	Lemma vector_recursion_nil : vector_recursion nil_vector = Bnil.
	Proof. reflexivity. Defined.

	Lemma vector_recursion_cons (a : A) n (v : vector A n) :
		vector_recursion (cons_vector a v) = Bcons a (list_of_vector v) (vector_recursion v).
	Proof. reflexivity. Defined.
End vector_recursion.

Lemma vec_of_lookup A `{IH : Inhabited A} n (v : vector A n) :
	vec_of_finfun (λ i, v !# i) = v.
Proof.
	apply vector_eq; simpl.
	apply list_eq_same_length with (n:=n); [done..|]; intros i x y Hin.
	rewrite list_lookup_fmap. rewrite index_enum_lookup.
	destruct (index_option_Some_lt2 Hin) as (i' & -> & Hi'); simpl.
	intros [=<-] Hy.
	unfold vector_lookup_total. rewrite Hi'.
	by apply list_lookup_total_correct.
Qed.

Lemma lookup_vec_of_finfun A `{Inhabited A} n (f : 'I_n → A) i :
	(vec_of_finfun f) !# i = f i.
Proof.
  unfold vector_lookup_total; simpl.
  destruct n.
  - exfalso; by apply index_0_false.
  - erewrite list_lookup_total_fmap;
      [|by rewrite index_enum_length].
    by rewrite index_enum_lookup_total.
Qed.

Lemma lookup_vec_of_fun A `{Inhabited A} n (f : nat -> A) i :
	(vec_of_fun n f) !# i = f i.
Proof.
  unfold vector_lookup_total; simpl.
  erewrite list_lookup_total_fmap;
    [|by rewrite seq_length].
  by rewrite lookup_total_seq_lt.
Qed.

Lemma vector_lookup_total_eq A `{Inhabited A} n (v1 v2 : vector A n) :
	(∀ i, v1 !# i = v2 !# i) → v1 = v2.
Proof.
	intros. apply vector_eq.
	eapply list_total_eq; [by rewrite vector_length|].
	intros i'. unfold vector_lookup_total in H0.
	destruct (le_lt_dec n i').
	- rewrite 2 list_lookup_total_ge; by rewrite ?vector_length.
	- apply (H0 (Index l)).
Qed.

Lemma vec_of_finfun_ext A n (f g : 'I_n → A) :
	(∀ i, f i = g i) → vec_of_finfun f = vec_of_finfun g.
Proof.
	intros E.
	apply vector_eq; simpl.
	by apply list_fmap_ext.
Qed.

Section MatrixDef.
	Variable m n : nat.

	(* Matrices are stored in row major form *)
	Definition matrix := vector (vector Z n) m.

	Implicit Type M : matrix.

	Coercion list_of_matrix (M : matrix) : list (list Z) :=
		list_of_vector <$> (list_of_vector M).

	(*
		TODO: 
		Try extracting and see if size data gets erased
		Maybe look at stdpp finite maps
	*)

	Definition mx_val M (i : 'I_m) (j : 'I_n) : Z := M !# i !# j.

	Definition mx_of_fun (f : nat → nat → Z) : matrix :=
		vec_of_fun _ (λ i, vec_of_fun _ (f i)).
	
	(* Kinda annoying that we have both fun and finfun *)
	Definition mx_of_finfun (f : 'I_m → 'I_n → Z) : matrix :=
		vec_of_finfun (λ i, vec_of_finfun (f i)).

	Lemma mx_of_fun_val f i j :
	mx_val (mx_of_fun f) i j = f i j.
	Proof.
		unfold mx_val, mx_of_fun.
		by rewrite 2 lookup_vec_of_fun.
	Qed.

	Definition get_row M i : vector Z n := M !# i.
	Definition get_col M j : vector Z m :=
	fmap (M := λ A, vector A m) (λ v : vector Z n, v !# j) M. (* explicit M *)

	Definition mx_const (val : Z) : matrix :=
		[vec of replicate _ [vec of replicate _ val]].
	
	Lemma mx_const_correct val : mx_const val = mx_of_finfun (fun _ _ => val).
	Proof.
		do 2 (apply vector_eq; simpl; erewrite const_fmap; [|done]; f_equal; try done).
	Qed.

	Definition mx_zero := mx_const 0%Z.

	Definition mx_add M1 M2 : matrix :=
		vec_of_finfun (λ i, [vec of zip_with Z.add (M1 !# i) (M2 !# i)]).
	
	Lemma mx_add_eq M1 M2 :
		mx_add M1 M2 = mx_of_finfun (λ i j, (mx_val M1 i j) + (mx_val M2 i j))%Z.
	Proof.
		unfold mx_add, mx_of_finfun.
		apply vec_of_finfun_ext; intros i.
		eapply vector_lookup_total_eq; intros j.
		rewrite lookup_vec_of_finfun.	cbn.
		erewrite lookup_total_zip_with.
			2,3: by rewrite vector_length.
		done.
	Qed.
End MatrixDef.

Arguments mx_const {m n}.
Arguments mx_zero {m n}.

Declare Scope matrix_scope.
Delimit Scope matrix_scope with M.
Bind Scope matrix_scope with matrix.
Open Scope matrix_scope.
Open Scope Z_scope.
Open Scope nat_scope.

Notation "0" := mx_zero : matrix_scope.
Notation "x + y" := (mx_add x y) (at level 50, left associativity) : matrix_scope.

Example example_mx : matrix 2 3 := [vec [vec 1;2;3]; [vec 6;7;8]]%Z.

Definition row_mx m n1 n2 (A : matrix m n1) (B : matrix m n2) : matrix m (n1 + n2) :=
	[vec of zip_with app_vector (list_of_vector A) (list_of_vector B)].
Definition col_mx m1 m2 n (A : matrix m1 n) (B : matrix m2 n) : matrix (m1+m2) n :=
	app_vector A B.
Definition block_mx m1 m2 n1 n2
	(A : matrix m1 n1) (B : matrix m1 n2)
	(C : matrix m2 n1) (D : matrix m2 n2) : matrix (m1+m2) (n1 + n2) :=
	col_mx (row_mx A B) (row_mx C D).

Definition usubmx m1 m2 n (M : matrix (m1+m2) n) : matrix m1 n :=
	take_vector M.
Definition dsubmx m1 m2 n (M : matrix (m1+m2) n) : matrix m2 n :=
	drop_vector M.
Definition lsubmx m n1 n2 (M : matrix m (n1 + n2)) : matrix m n1 :=
	fmap (M := λ A, vector A m) (λ v, take_vector v) M. (* explicit *and* eta! *)
Definition rsubmx m n1 n2 (M : matrix m (n1 + n2)) : matrix m n2 :=
	fmap (M := λ A, vector A m) (λ v, drop_vector v) M. (* explicit *and* eta! *)

Definition ulsubmx m1 m2 n1 n2 (M : matrix (m1+m2) (n1 + n2)) : matrix m1 n1 :=
	lsubmx (usubmx M).
Definition ursubmx m1 m2 n1 n2 (M : matrix (m1+m2) (n1 + n2)) : matrix m1 n2 :=
	rsubmx (usubmx M).
Definition dlsubmx m1 m2 n1 n2 (M : matrix (m1+m2) (n1 + n2)) : matrix m2 n1 :=
	lsubmx (dsubmx M).
Definition drsubmx m1 m2 n1 n2 (M : matrix (m1+m2) (n1 + n2)) : matrix m2 n2 :=
	rsubmx (dsubmx M).

Lemma usub_col_mx m1 m2 n (A : matrix m1 n) (B : matrix m2 n) :
	usubmx (col_mx A B) = A.
Proof. by apply vector_eq; simplify_list_eq. Qed.

Lemma lsub_row_mx m n1 n2 (A : matrix m n1) (B : matrix m n2) :
	lsubmx (row_mx A B) = A.
Proof.
	apply vector_eq; simpl.
	rewrite fmap_zip_with_l;
		[done| |by rewrite !vector_length].
	intros; by apply vector_eq; simplify_list_eq.
Qed.

Lemma ulsub_block_mx m1 m2 n1 n2
	(A : matrix m1 n1) (B : matrix m1 n2)
	(C : matrix m2 n1) (D : matrix m2 n2) :
	ulsubmx (block_mx A B C D) = A.
Proof. unfold block_mx, ulsubmx. by rewrite usub_col_mx, lsub_row_mx. Qed.
(* todo... dsub_col_mx, rsub_row_mx, ul/dl/ur/drsub_block_mx *)

(* needed? *)
Fixpoint list_vec_transpose' n (M : list (vector Z n)) : vector (list Z) n :=
	match M with
	| [] => [vec of replicate n []]
	| r :: rs => [vec of zip_with (::) r (list_vec_transpose' rs)]
	end.

Definition mx_transpose (m n : nat) (M : matrix m n) : matrix n m.
refine (vector_recursion _ _ M); clear M.
- exact [vec of replicate n nil_vector].
- intros r m' _ M'.
	exact [vec of zip_with (@cons_vector m' _) r (list_of_vector M')].
Defined.

Notation "m ^T" := (mx_transpose m) (at level 20, format "m '^T'") : matrix_scope.

(* 
 (fix go (m : nat) (M : list (vector Z n)) : vector (vector Z m) n :=
	match m, M with
	| S m', r :: rs => [vec of zip_with (@cons_vector m' _) r (go m' rs)]
	| 0   , []      => [vec of replicate n nil_vector]
	| _   , _       => mx_const inhabitant
	end) m (list_of_vector M).  *)

Lemma list_vec_transpose'_length n (M : list (vector Z n)) i :
	length ((list_vec_transpose' M) !# i) = length M.
Proof.
	induction M.
	{ simpl. by apply length_zero_iff_nil, lookup_total_replicate_2. }
	simpl; unfold vector_lookup_total; simpl.
	erewrite lookup_total_zip_with; [| by rewrite vector_length..].
	simpl; by f_equal.
Qed.

Lemma list_vec_transpose'_length_Forall n (M : list (vector Z n)) :
	Forall (λ l, length l = length M) (list_vec_transpose' M).
Proof.
	induction M; simpl.
	1:by apply Forall_replicate.
	eapply Forall_zip_with_snd; [|apply IHM]; simpl.
  apply Forall_true; intros; subst; simpl. by f_equal.
Qed.

Compute list_of_matrix (mx_transpose nil_vector : matrix 3 0).

Lemma mx_tranpose_eq m n (M : matrix m n) : mx_transpose M = mx_of_finfun (λ i j, mx_val M j i).
Proof.
	induction M using vector_induction.
	- unfold mx_transpose, mx_of_finfun, vec_of_finfun; simpl.
		unfold mx_transpose; rewrite vector_recursion_nil.
		apply vector_eq; simpl.
		erewrite const_fmap.
		+ by rewrite index_enum_length.
		+ intros; by apply vector_eq.
	- (* hard *) admit.
Admitted.

Definition mx_id {n} : matrix n n :=
	mx_of_finfun (λ i j, if Nat.eqb i j then 1 else 0).

Notation "1" := mx_id : matrix_scope.

(* Lemma lookup_total_mx_of_fun m n f i :
	i < m →
	(mx_of_fun m n f) !!! i = f i <$> (seq 0 n).
Proof.
	intros H. unfold mx_of_fun.
	rewrite (list_lookup_total_fmap (λ i0, (λ j, f i0 j) <$> seq 0 n) (seq 0 m) i).
	2:{ by rewrite seq_length. }
	rewrite lookup_total_seq_lt; [|done].
	done.
Qed. *)

About mx_of_fun.
Example example_mx1 := mx_of_fun 100 130 (λ i j, i + j * 10).
(* Example example_mx2 := mx_of_fun 100 100 (λ i j, i - j * 10). *)
Time Compute ((list_of_vector example_mx1) !!! 10).

Time Compute (list_vec_transpose' (list_of_vector example_mx1)) !# (ind_default0 10) !! 10.

Definition row_from_vector n (v : vector Z n) : matrix 1 n := [vec v].

Definition col_from_vector n (v : vector Z n) : matrix n 1 :=
	fmap (M := λ A, _) (λ x : Z, [vec x]) v. (* why explicit M ? *)


Fail Lemma mx_row_ind m (P : ∀ n, matrix m n → Type) :
  (∀ M, P 0 M) →
  (∀ m r M, P m M → P (S m)).

Definition inner_prod n (v1 v2 : vector Z n) : Z :=
	foldr Z.add 0%Z $ zip_with Z.mul v1 v2.

Definition mx_apply n m (M : matrix n m) (v : vector Z m) : vector Z n :=
	fmap (M := λ A, _) (λ Mrow, inner_prod Mrow v) M.

Compute list_of_vector ( mx_apply example_mx [vec 4;5;6]%Z).

Definition mx_mult p q r (M1 : matrix p q) (M2 : matrix q r) : matrix p r :=
	mx_transpose (fmap (M := λ A, _) (mx_apply M1) (mx_transpose M2)).

Notation "x *m y" := (mx_mult x y) (at level 40, left associativity) : matrix_scope.

Compute list_of_matrix (mx_mult example_mx (mx_transpose example_mx)).

Fail Lemma mx_apply_add n m (A : matrix n m) (v w : vector Z n) : mx_apply .

Lemma mx_apply_id n (v : vector Z n) : mx_apply mx_id v = v.
Proof.
	apply vector_eq.
	induction n. { by rewrite nil_length_inv. }
	destruct v as [[|x xs] Hv]; [done|].
	injection Hv as Hxs.
	simpl.
Admitted.

Definition divides (a b : Z) : bool := (b mod a =? 0)%Z.
Infix "%|" := divides (at level 30): Z_scope.

(* We would like to have an efficient binary algorithm that also returns Bezout
	but for right now we just copy the euclidean algorithm from the stdlib and 
	extend it (trivially) *)
Fixpoint Zgcd_ex_fuel (n : nat) (a b : Z) : Z * (Z * Z) :=
	match n with
		| O => (1, (0,0))%Z (* arbitrary, since n should be big enough *)
		| S n => match a with
				| Z0 => (Z.abs b, (0, Z.sgn b))%Z
				| Zpos _ =>
						let (q,r) := Z.div_eucl b a in
						let '(g, (x,y)) := Zgcd_ex_fuel n r a in
						(g, (y - q*x, x))%Z
				| Zneg a => let a := (Zpos a) in
						let (q,r) := Z.div_eucl b a in
						let '(g, (x,y)) := Zgcd_ex_fuel n r a in
						(g, (-(y - q*x), x))%Z
				end
	end.

Definition Zgcd_ex_bound (a : Z) :=
	match a with
		| Z0 => S O
		| Zpos p => let n := Pos.size_nat p in (n+n)%nat
		| Zneg p => let n := Pos.size_nat p in (n+n)%nat
	end.

Definition Zgcd_ex a b := Zgcd_ex_fuel (Zgcd_ex_bound a) a b.

Module gcd.
	Require Import Zgcd_alt.

	Lemma Zgcd_alt_ex n a b : (Zgcd_ex_fuel n a b).1 = Zgcdn n a b.
	Proof.
		induction n in a,b |- *; simpl; [done|].
		unfold Z.modulo.
		destruct a; simpl; [done|rewrite <- IHn..].
		all: by repeat match goal with |- context G [let '_ := ?f in _] => destruct f end.
	Qed.

	Definition pairs A B (aa : list A) (bb : list B) : list (A*B) :=
		flat_map (fun a => (fun b => (a,b)) <$> bb) aa.
	Example ints := [-135301852344706746049;135301852344706746049;-218922995834555169026;218922995834555169026;
		-15;-10;-6;-5;-4;-3;-2;-1;0;1;2;3;4;5;6;10;15]%Z.
	Example intpairs := pairs ints ints.
	Time Example gcdpairs := (fun '(a,b) => ((a,b),Zgcd_ex a b)) <$> intpairs.
	Time Compute List.filter (negb ∘ snd) ((fun '((a,b),(g,(x,y))) => ((a,b),a*x + b*y =? g)) <$> gcdpairs).
End gcd.



(*        (kth column)
	/ a .... b ..... \
	| . 1 .......... |
	| ....1......... |
	| c .... d ..... | (kth row)
	| ..........1... |
	\ .............1 / *)
	Check index_plus.

	
Definition combine_mx (a b c d : Z) m (k : 'I_m) : matrix (1+m) (1+m) :=
	let k' : 'I_(1+m) := index_plus k in
	mx_of_finfun (λ i j,
		if i =? j then if i =? 0 then a
			else if i =? k' then d else 1
		else if (i =? 0) && (j =? k') then b
		else if (i =? k') && (j =? 0) then c else 0
	).

Compute list_of_matrix (combine_mx 5 6 7 8 (ind_default0 2 : 'I_4)).
Compute list_of_matrix (combine_mx 5 6 7 8 (ind0 : 'I_1)).

Definition Bezout_mx (a b : Z) m (k : 'I_m) : matrix (1+m) (1+m) :=
	let '(g, (u,v)) := Zgcd_ex a b in
	let a1 := (a/g)%Z in let b1 := (b/g)%Z in
	combine_mx u v (-b1) a1 k.

Compute list_of_matrix (Bezout_mx 6 15 (ind_default0 0 : 'I_1)).

Definition Bezout_step (a b : Z) m n (M : matrix (1+m) (1+n)) (k : 'I_m) : matrix (1+m) (1+n) :=
	mx_mult (Bezout_mx a b k) M.
(* TODO: Define Bezout_step without mx_mult *)

Definition vector_find {A} n (P : A → Prop) `{∀ x, Decision (P x)}
	(v : vector A n) : option ('I_n * A).
Proof.
	destruct (list_find P v) as [[i a]|] eqn:E;
		[apply Some|exact None]. (* monadic *)
	split; [|exact a]; apply Index with i.
	destruct (proj1 (list_find_Some _ _ _ _) E) as [H' _]; clear E. (* hate this *)
	erewrite <- vector_length; by eapply lookup_lt_Some.
Defined.
Global Arguments vector_find {A} {n} P {_} v.

Definition matrix_find m n (P : Z → Prop) `{∀ x, Decision (P x)}
	(M : matrix m n) : option ('I_m * 'I_n). (* return value? *)
Proof.
	unfold matrix in M.
	destruct (vector_find (λ r, is_Some $ vector_find P r) M) as [[i v]|]; [|apply None].
		(* TODO: dont want to find twice... *)
	destruct (vector_find P v) as [[j _]|]; [|apply None (*impossible*)].
	exact (Some (i,j)).
Defined.

Global Arguments matrix_find {m n} P {_} M.

Section vector_x.
	Context {A : Type}.
	Context `{Inhabited A}.
	Context {n : nat}.
	Implicit Types i : 'I_n.
	Implicit Types v : vector A n.

	Definition vector_x i1 i2 v :=
		let a1 := v !# i1 in let a2 := v !# i2 in
		<[ i1 := a2 ]> (<[ i2 := a1 ]> v). (* want {[ ; ]} *)

	Lemma vector_x_commute i1 i2 v : vector_x i1 i2 v = vector_x i2 i1 v.
	Proof.
		destruct (decide (i1 =@{nat} i2)).
		- by rewrite (index_eq e).
		-	apply vector_eq; simpl.
			by apply list_insert_commute.
	Qed.

	Lemma vector_x_alias i v : vector_x i i v = v.
	Admitted. (* easy *)
End vector_x.

Definition xrow m n (i1 i2 : 'I_m) (M : matrix m n) : matrix m n :=
	vector_x i1 i2 M.

Definition xcol m n (j1 j2 : 'I_n) (M : matrix m n) : matrix m n :=
	fmap (M := λ A, vector A m) (vector_x j1 j2) M. (* explicit M *)

Definition mx1x1 (a : Z) : matrix 1 1 := [vec [vec a]].
Definition scalar_mx {n} (a : Z) : matrix n n :=
	mx_of_finfun (λ i j, if Nat.eqb i j then a else 0).

Definition map_mx m n (f : Z → Z) (M : matrix m n) : matrix m n :=
	fmap (M := λ A, vector A m) (fmap (M := λ A, vector A n) f) M. (* explicit *)

Definition lift0_mx n (M : matrix n n) : matrix (1+n) (1+n) :=
	block_mx (mx1x1 1) 0 0 M.

Section smith.

Definition find1 m n (M : matrix (1+m) (1+n)) (a : Z) : option 'I_m :=
	let (_,C') := destruct_vector (get_col M ind0) in
	fst <$> (vector_find (λ b, negb (a %| b)%Z) C').

Definition find2 m n (M : matrix (1+m) (1+n)) (a : Z) : option ('I_(1+m) * 'I_n) :=
	let M' := rsubmx M in
	matrix_find (λ b, negb (a %| b)%Z) M'.

Definition find_pivot m n (M : matrix (1+m) (1+n)) : option ('I_(1+m) * 'I_(1+n)) :=
	matrix_find (λ b, negb (b =? 0))%Z M.

Fixpoint improve_pivot_rec (k : nat) {m n} :
	matrix (1+m) (1+m) → matrix (1+m) (1+n) → matrix (1+n) (1+n) →
	matrix (1+m) (1+m) * matrix (1+m) (1+n) * matrix (1+n) (1+n) :=
  match k with
  | 0 => fun P M Q => (P,M,Q)
  | S p => fun P M Q =>
      let a := (mx_val M ind0 ind0) in (* todo make index notation scope with 0 *)
			match find1 M a with
			| Some i =>
        let Mi0 := mx_val M (index_plus i) ind0 in
        let P := Bezout_step a Mi0 P i in
        let M := Bezout_step a Mi0 M i in
        improve_pivot_rec p P M Q
      | None =>
				let u  := dlsubmx M in let vM := ursubmx M in let vP := usubmx P in
				let u' := map_mx (fun x => 1 - x / a)%Z u in
				let P  := col_mx (usubmx P) (u' *m vP + dsubmx P) in
				let M  := block_mx (mx1x1 a) vM
													(mx_const a) (u' *m vM + drsubmx M) in
				match find2 M a with
				| Some (i,j) =>
					let M := xrow ind0 i M in let P := xrow ind0 i P in
					let a := mx_val M ind0 ind0 in
					let M0ij := mx_val M ind0 (index_plus j) in
					let Q := (Bezout_step a M0ij (Q^T) j)^T in (* why parens? (Q^T) *)
					let M := (Bezout_step a M0ij (M^T) j)^T in
					improve_pivot_rec p P M Q
				| None => (P, M, Q)
				end
			end
  end.

Definition improve_pivot k m n (M : matrix (1+m) (1+n)) :=
  improve_pivot_rec k 1 M 1.

Fixpoint Smith m n : matrix m n → matrix m m * list Z * matrix n n :=
  match m, n with
  | S _, S _ => fun M : matrix (1+_) (1+_) =>
    match find_pivot M with | Some (i, j) =>
      let a := mx_val M i j in let M := xrow i ind0 (xcol j ind0 M) in
      (* this is where Euclidean norm eases termination argument *)
      let '(P,M,Q) := improve_pivot (Z.abs_nat a) M in
      let a  := mx_val M ind0 ind0 in
      let u  := dlsubmx M in let v := ursubmx M in
      let v' := map_mx (fun x => x / a)%Z v in
      let M  := (drsubmx M + map_mx (λ a, -a)%Z (mx_const 1 *m v))%M in (* todo define minus *)
      let '(P', d, Q') := Smith (map_mx (fun x => x / a)%Z M) in
      (lift0_mx P' *m block_mx 1 0 (mx_const (-1)) 1 *m xcol i ind0 P,
       a :: ((λ x, x * a)%Z <$> d),
       xrow j ind0 Q *m block_mx 1 (map_mx (λ a, -a)%Z v') 0 1 *m lift0_mx Q')
    | None => (1%M, [], 1%M)
		end
  | _, _ => fun M => (1%M, [], 1%M)
  end.

Compute list_of_matrix example_mx.
Compute let '(A,d,B) := Smith example_mx in (list_of_matrix A, d, list_of_matrix B).

Example example_mx2 : matrix 3 3 :=
	[vec [vec  2; 4; 4];
	     [vec -6; 6;12];
			 [vec 10; 4;16]]%Z.

Time Compute let '(A,d,B) := Smith example_mx2 in (list_of_matrix A, d, list_of_matrix B).

Example example_mx3 : matrix 5 5 :=
[vec [vec    25;   -300;    1050;   -1400;    630];
     [vec  -300;   4800;  -18900;   26880; -12600];
     [vec  1050; -18900;   79380; -117600;  56700];
     [vec -1400;  26880; -117600;  179200; -88200];
     [vec   630; -12600;   56700;  -88200;  44100]]%Z.
Time Compute let '(A,d,B) := Smith example_mx3 in d.

Example example_mx4 : matrix 5 5 :=
[vec [vec 3; 5; 2; 4; 51];
     [vec 1; 21; 8; 7; 0];
     [vec 2; 4; 17; 3; 13];
     [vec 23; 9; 1; 3; 9];
     [vec 1; 4; 9; 5; 11]]%Z.
(* Time Compute let '(A,d,B) := Smith example_mx4 in d. *)


Example example_mx5 : matrix 10 10 :=
[vec [vec 8; 7; 2; 3; 5; 6; 1; 9; 0; 3];
     [vec 4; 7; 7; 6; 8; 9; 9; 8; 5; 3];
     [vec 9; 5; 2; 2; 7; 4; 5; 6; 8; 1];
     [vec 3; 7; 4; 3; 3; 5; 2; 3; 4; 5];
     [vec 7; 0; 4; 9; 8; 6; 8; 0; 8; 6];
     [vec 6; 4; 2; 9; 6; 3; 1; 8; 0; 8];
     [vec 0; 6; 4; 9; 8; 8; 2; 6; 7; 3];
     [vec 7; 8; 0; 7; 2; 2; 9; 5; 4; 6];
     [vec 8; 0; 2; 4; 0; 7; 2; 5; 6; 3];
     [vec 8; 9; 0; 5; 0; 4; 6; 2; 3; 9]]%Z.
(* Time Compute let '(A,d,B) := Smith example_mx4 in d. *)

End smith.