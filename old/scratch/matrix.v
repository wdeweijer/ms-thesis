From stdpp Require Import prelude.
(* Require Import List ZArith.
Import ListNotations. *)

Coercion Z.of_nat : nat >-> Z.

Local Instance Zlist_inhabited : Inhabited (list Z) := populate [].
Local Instance Z_inhabited : Inhabited Z := populate 0%Z.

(* Matrices are stored in row major form
   mx_values does not have to be correct shape
   indices not present are interpreted as 0
   elements out of bounds are ignored *)
Definition matrix := list (list Z). (* Do we want this? *)
Definition hmatrix (m n : nat) := matrix.

(*
	Freeks idea: remove matrix, make hmatrix a record 
	that includes mx_well_sized.
	Maybe follow canonical structures from mathcomp
	Make all operations work on sized matrices
	Try extracting and see if size data gets erased
	Maybe look at stdpp finite maps

	Use hvector as a record
	and make hmatrix a hvector of hvector

	look at proof relevance
*)

Definition mx_well_sized (M : matrix) (m n : nat) :=
	length M = m /\ (∀ i, i < m -> length (M !!! i) = n).

Definition mx_val (M : matrix) i j : Z :=
	(M !!! i) !!! j. (* Problem: doesnt ignore out of bounds elements *)

(* Make mx_val for hmatrix? *)
(* Make mx_eq for hmatrix taking care of size *)

Definition mx_of_fun m n (f : nat → nat → Z) : hmatrix m n :=
	(λ i, (λ j, f i j) <$> (seq 0 n)) <$> (seq 0 m).

Example example_mx : hmatrix 2 3 := [[1;2;3]; [6;7;8]]%Z.
Compute mx_val example_mx 1 0.
Compute mx_of_fun 2 3 (λ i j, 10*i + j).
Compute mx_of_fun 3 4 (λ i j, i).
Goal mx_val (mx_of_fun 10 10 (λ i j, i)) 5 1 = 5. done. Qed.

Lemma lookup_total_mx_of_fun m n f i :
	i < m ->
	(mx_of_fun m n f) !!! i = f i <$> (seq 0 n).
Proof.
	intros H. unfold mx_of_fun.
	rewrite (list_lookup_total_fmap (λ i0, (λ j, f i0 j) <$> seq 0 n) (seq 0 m) i).
	2:{ by rewrite seq_length. }
	rewrite lookup_total_seq_lt; [|done].
	done.
Qed.

Definition mx_of_fun_length_1 m n f:
	length (mx_of_fun m n f) = m.
Proof.
	unfold mx_of_fun.
	by rewrite map_length, seq_length.
Qed.

Definition mx_of_fun_length_2 m n f i :
	i < m ->
	length (mx_of_fun m n f !!! i) = n.
Proof.
	intros.
	rewrite lookup_total_mx_of_fun; [|done].
	by rewrite map_length, seq_length.
Qed.

Lemma mx_of_fun_val m n f i j :
	i < m -> j < n ->
	mx_val (@mx_of_fun m n f) i j = f i j.
Proof.
	intros le_im le_jn.
	unfold mx_val.
	Check lookup_total_mx_of_fun.
	Fail rewrite lookup_total_mx_of_fun.
	Fail rewrite (lookup_total_mx_of_fun m n f i). (* ??? *)
	unfold mx_of_fun.
	remember ((λ i0 : nat, (λ j0 : nat, f i0 j0) <$> seq 0 n)).
		(* Why do we need to apply? *)
	rewrite (list_lookup_total_fmap l (seq 0 m) i). subst.
	2:{ by rewrite seq_length. }
	remember (λ j0 : nat, f (seq 0 m !!! i) j0).
	rewrite (list_lookup_total_fmap z (seq 0 n) j); subst.
	2:{ by rewrite seq_length. }
	rewrite lookup_total_seq_lt; [|done].
	rewrite lookup_total_seq_lt; [|done].
	done.
Qed.

Definition mx_ensize (M : matrix) m n : hmatrix m n :=
	mx_of_fun m n (mx_val M).

Definition mx_resize (M : matrix) m n : hmatrix m n :=
	resize m (replicate n 0%Z)
		((resize n 0%Z) <$> M).

Compute mx_resize [] 1 5.
Compute mx_ensize [] 1 5.

(* Should be in lib? *)
Lemma list_total_eq A `{Inhabited A} (l1 l2 : list A)  :
	length l1 = length l2 ->
	(∀ i, l1 !!! i = l2 !!! i) ->
	l1 = l2.
Proof.
	intros Hl Hi.	apply list_eq.
	intros i; specialize (Hi i).
	destruct (nat_lt_dec i (length l1)).
	- rewrite 2 list_lookup_lookup_total_lt, Hi; [done|lia..].
	- rewrite 2 lookup_ge_None_2; [done|lia..].
Qed.

(* Should be in lib? *)
Lemma list_lookup_total_ge {A} `{Inhabited A} (l : list A) i :
	length l ≤ i->
	l !!! i = inhabitant.
Proof. intros Hl. by rewrite list_lookup_total_alt, lookup_ge_None_2. Qed.

(* TODO: Automate this! *)
Lemma mx_ensize_resize_eq M m n :
	mx_ensize M m n = mx_resize M m n.
Proof.
	unfold mx_ensize, mx_resize.
	eapply list_total_eq.
	{ by rewrite mx_of_fun_length_1, resize_length. }
	intros i; destruct (nat_lt_dec i m).
	- rewrite (lookup_total_mx_of_fun m n _ i); [|done].
		destruct (nat_lt_dec i (length M)).
		+ rewrite lookup_total_resize.
			2:done.
			2:by rewrite fmap_length.
			erewrite list_lookup_total_fmap; [|done].
			eapply list_total_eq.
			{ by rewrite fmap_length, seq_length, resize_length. }
			{ intros j; destruct (nat_lt_dec j n).
				- erewrite list_lookup_total_fmap.
					2:{ by rewrite seq_length. }
					rewrite lookup_total_seq_lt; [|done]; simpl.
					destruct (nat_lt_dec j (length (M !!! i))).
					+ rewrite lookup_total_resize; done.
					+ rewrite lookup_total_resize_new.
						2:{ by apply Nat.nlt_ge. }
						2:done.
						unfold mx_val. rewrite list_lookup_total_ge; [done|lia].
				- rewrite list_lookup_total_ge.
					2:{ rewrite fmap_length, seq_length; lia. }
					rewrite list_lookup_total_ge; [done|].
					rewrite resize_length; lia.
			}
		+ rewrite lookup_total_resize_new; [..|done].
			2:{ rewrite fmap_length; lia. }
			unfold mx_val. rewrite (list_lookup_total_ge M i); [|lia].
			simpl. (* Want to use lookup_total_nil, but have to go through list_eq *)
			admit.
	- rewrite 2 list_lookup_total_ge; [done|..]. 
	 	2:{ rewrite mx_of_fun_length_1; lia. }
		rewrite resize_length; lia.
Admitted.

Lemma mx_ensize_well_sized M m n : mx_well_sized (mx_ensize M m n) m n.
Proof.
	unfold mx_ensize; split.
	- apply mx_of_fun_length_1.
	- apply mx_of_fun_length_2.
Qed.


(* Definition zip_with_default_upto {A B C} (dA : A) (dB : B) (f : A → B → C) :
	nat -> list A → list B → list C :=
	fix go n l1 l2 :=
	match n with | 0 => [] | S n =>
	match l1, l2 with
	| x1 :: l1, x2 :: l2 => (f x1 x2) :: go n l1 l2
	| [], x2 :: l2 => f dA x2 :: go n [] l2
	| x1 :: l1, [] => f x1 dB :: go n l1 []
	| [], [] => f dA dB :: go n [] []
	end
	end.

Lemma cons_length_inv {A} {l : list A} {n} :
	length l = S n -> ∃ x l', l = x :: l'.
Proof. destruct l; simpl; [done|]; eauto. Qed. 

(* This is terrible *)
Lemma zip_with_default_upto_sized {A B C} {dA dB f l1 l2} :
	length l1 = length l2 ->
	@zip_with_default_upto A B C dA dB f (length l1) l1 l2 = zip_with f l1 l2.
Proof.
	remember (length l1) as n eqn:Hl1; intros Hl2.
	symmetry in Hl1; symmetry in Hl2.
	induction n; simpl.
	erewrite nil_length_inv with l1; done.
	destruct (cons_length_inv Hl1) as (x1 & l1' & ->).
	destruct (cons_length_inv Hl2) as (x2 & l2' & ->).
	(* IHn not general enough, but i dont know how to fix *)
Admitted. *)

Definition mx_zero : matrix := [].

Definition mx_const m n (val : Z) : hmatrix m n :=
	mx_of_fun m n (fun _ _ => val).

Definition mx_add {m n} (A B : hmatrix m n) : hmatrix m n :=
	(* zip_with (zip_with Z.add) A B. *) (* This won't work with undersized matrices! *)
	mx_of_fun m n (λ i j, (mx_val A i j) + (mx_val B i j))%Z. (* This is slow *)

Definition mx_add' (A B : matrix) : matrix :=
	zip_with (zip_with Z.add) A B.

Lemma mx_add_well_sized m n A B :
	mx_well_sized A m n -> mx_well_sized B m n ->
	mx_well_sized (mx_add' A B) m n.
Proof.
	intros [wsA1 wsA2] [wsB1 wsB2].
	unfold mx_add'.	split.
	rewrite zip_with_length. lia.
	intros i Him; specialize (wsA2 i Him); specialize (wsB2 i Him).
	rewrite 



Definition mx_transpose {m n} (M : hmatrix m n) : hmatrix n m :=
	mx_of_fun n m (λ i j, mx_val M j i).

Definition mx_id n :=
	mx_of_fun n n (fun i j => if Nat.eqb i j then 1 else 0).

Compute mx_add (mx_const 3 3 (- 1)%Z) (mx_id 3).
Compute mx_add (mx_zero) (mx_id 3).



Print Pos.ggcdn.
