%
%            SETUP
%
% !TEX TS-program = pdflatex
%\RequirePackage{fix-cm}
\documentclass[11pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
%\input{ix-utf8enc.dfu}

% \usepackage[maxnames=4,minnames=2]{biblatex}
% \addbibresource{sources.bib}

%
%            SYMBOLS
%
\usepackage{textcomp}
\usepackage{amsmath, amsfonts, amssymb, amsthm}
\usepackage{mathtools}

%
%            TYPEFACE
%
\usepackage[osf,sc]{mathpazo} \linespread{1.1}
%\usepackage{helvet}

%\usepackage{lmodern}
%\usepackage{cfr-lm}

%\usepackage{textgreek}

%
%            GEOMETRY
%
\newlength{\karakters}
\settowidth{\karakters}{\normalsize\fontfamily{\rmdefault}\selectfont abcdefghijklmnopqrstuvwxyz}

\usepackage[textwidth=2.53846\karakters,top=2cm,bottom=2cm]{geometry} % 66
%\usepackage[textwidth=3.19231\karakters,top=2cm,bottom=2cm]{geometry} % 83

%
%            LANGUAGE
%
% !TEX spellcheck = en_GB
\usepackage[english]{babel}
\frenchspacing
\usepackage{csquotes}

%
%            TYPOGRAPHY
%
\usepackage{microtype}
\usepackage{parskip}
\usepackage{enumitem}


\usepackage{titling}
\setlength{\droptitle}{-2cm}


\renewcommand\ij{i\kern-0.3ptj} % Maybe use something other than pt here?, this should be 0.57pt for cm
\newcommand\aij{í\kern-0.3pt\'\j} % Accent on j is higher than on the i in palatino

\newcommand*{\abbrev}[1]{\textls[50]{\textsc{#1}}} % spaced small caps


%
%            VARIOUS
%
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{babel, arrows, cd, positioning, automata} % makes compiletime longer
\usepackage{amsthm}
\usepackage{cancel}
\usepackage{color}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{stmaryrd}

%
%            DEFINITIONS
%

% Now define a bunch of theorem-type environments.
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

%\usepackage{float}
%\restylefloat{figure}

\input{macro}

%\setlist{nosep}
% \setlist{noitemsep}

\hyphenation{se-mi-ring}


%
%            DOCUMENT
%
\title{\textsc{Smith Normal Form}}
\author{Wessel de We\ij{}er}

\begin{document}

%\input{titlepage.tex}
\section{Smith normal form}
For now, we only look at the the $\ZZ$ case. This can be generalized however to an arbitrary PID.

A morphisms $f : \ZZ^k \to \ZZ^n$ of free abelian groups of finite rank can be written as a matrix $F$ with $k$ columns and $n$ rows.

\begin{definition}
	The \emph{Smith normal form} (\textsc{smf}) of an $n\times k$ matrix $F$ is a product of three matrices $S M T = F$ such that $S$ and $T$ are invertible and $M$ is a diagonal matrix with diagonal $(d_i)_{i\le m}$ such that each subsequent entry divides the next $d_1 \mid d_2 \mid \dots \mid d_m$.
	% \[ \begin{pmatrix*}
	% 	d_1    & 0   & \cdots & \\
	% 	0      & d_2 &        & \\
	% 	\vdots &     & \ddots & \\
	% 	       &     &        & d_n
	% \end{pmatrix*}\]
\end{definition}

Note that $T$ acting on the right can act on the columns of $M$ and $S$ acting on the left can act on the rows. Therefore some of the actions we can perform are permutations of columns and rows or adding arbitrary multiples of columns and rows to eachother.

Any $n \times k$ matrix $F$ with entries in $\ZZ$ has a \textsc{smf} by the following algorithm.

\subsection{algorithm}

If $F = 0$ or if either $n=0$ or $k=0$ then we are already done. So we will first pick a non-zero pivot $p$ and position it in the top left corner of the matrix by swapping rows and columns.

\paragraph{Step 1.} We will transform the matrix so that the top row consists only of zeroes except for the pivot in the top left as follows. If there is any element $q$ in the top row with $p \mid q$ we can simply subtract the pivot column from it until it becomes zero.

If $p\nmid q$ we can calculate their gcd $g \coloneqq \gcd(p,q)$. Using the Bézout identity there exist $x, y$ such that $px + qy = g$. Since $g$ divides $p$ and $q$ we also have $\alpha \coloneqq p/g$ and $\beta \coloneqq q/g$ with $\alpha x + \beta y = 1$. Therefore the matrix
\[ \begin{pmatrix*}x & -\beta \\ y & \alpha \end{pmatrix*} \]
has determinant $1$ and hence is invertible so that we can use it to act on our matrix (when it is fit into an identity matrix of the right size). We can see how this works by isolating the relevant entries:
\[
    \begin{pmatrix*} p & q \\ r & s \end{pmatrix*}%
    \begin{pmatrix*} x & -\beta \\ y & \alpha \end{pmatrix*} =
    \begin{pmatrix*} px + qy & -p\beta + q\alpha \\ rx + sy & -r\beta + s\alpha \end{pmatrix*} =
    \begin{pmatrix*} g & 0 \\ rx + sy & -r\beta + s\alpha \end{pmatrix*}
\]

Note that the new pivot $g$ has strictly fewer prime divisors than the original $p$. We will use this as a termination measure later.

\paragraph{Step 2.} Now that the top row has all zeroes except for the pivot, we wish to do the same for the left column. For this we will repeat step 1 mutatis mutandis. Note that this may reintroduce non-zero elements into the first row: we must iterate step 1 alternating on rows and columns.

\paragraph{Step 3.} Step 1 and 2 must eventually terminate by the termination measure from before. Now the matrix will have all zeroes in both the first row and column except for the topleft pivot. Hence we can iterate our producedure on the rest of the matrix until it is diagonal.

\paragraph*{Step 4.} Call the diagonal elements $p_1,\dots,p_m$. To finalize our algorithm we must ensure that $p_i \mid p_{i+1}$. [TODO]
\qed

We can now use the \textsc{snf} to prove the structure theorem of abelian groups. Given a f.g. abelian group $N$ with presentation $f : \ZZ^k \to \ZZ^n$ we compute its \textsc{snf} $SMT$ with diagonal $d_i$. Now $N \iso \ZZ^n / \im M$ but this is particularly easy to compute given the $d_i$. [TODO] First we can discard any unit $d_i$ since they do not contribute to $N$. Additionally, the number of zero $d_i$ will be the rank $k \coloneqq \#\{d_i = 0\}$, we can now discard them as well. The rest of the non-zero, non-unit $d_i$ will consitute the torsion subgroup.

The uniqueness is given by an (easy) algebra argument [TODO].

\subsection{Computing homology}
A chain complex $C_\bullet$ is a $\ZZ$ indexed sequence of modules $(C_i)_{i\in \ZZ}$ together with morphisms $\del_i : C_i \to C_{i-1}$ such that $\im \del_{i+1} \subseteq \ker \del_i$ (this is often written as $\del\circ\del = 0$). A chain complex is called \emph{connective} if all the modules in negative degree are trivial:
\[ 0 \leftarrow M_0 \xleftarrow{\del_1} M_1 \xleftarrow{\del_2} M_2 \leftarrow \dots \]
\begin{definition}
	The \emph{$i$th homology} of a chain complex $C_\bullet$ is defined as
	\[ H_i(C_\bullet) \coloneqq \ker f_i / \im f_{i+1}. \]
\end{definition}


%\printbibliography

\end{document}