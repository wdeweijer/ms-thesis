From stdpp Require Import prelude options.
From thesis Require Import lib index vector matrix.

(* Require Import String. *)
(* From ReductionEffect Require Import PrintingEffect. *)


(*        (kth column)
	/ a .... b ..... \
	| . 1 .......... |
	| ....1......... |
	| c .... d ..... | (kth row) (* NB "k+1" *)
	| ..........1... |
	\ .............1 / *)

Definition combine_mx (a b c d : Z) m (k : 'I_m) : 'M_(1+m) :=
	let k' : 'I_(1+m) := S_index k in
	mx_of_finfun (λ i j,
		if i =? j then if i =? 0 then a
			else if i =? k' then d else 1
		else if (i =? 0) && (j =? k') then b
		else if (i =? k') && (j =? 0) then c else 0%Z
	).

(* Compute list_of_matrix (combine_mx 1 2 3 4 (0 : 'I_2)). *)

Definition combine_step (a b c d : Z) m n (M : 'M_(1+m, 1+n)) (k : 'I_m) : 'M_(1+m, 1+n) :=
	let k' : 'I_(1+m) := S_index k in
	let row0 := M !# 0 in let rowk := M !# k' in
		<[  k' := vector_add (vector_scale c row0) (vector_scale d rowk) ]>
		(<[ 0  := vector_add (vector_scale a row0) (vector_scale b rowk) ]> M).

(* Compute list_of_matrix (combine_step 1 2 3 4 1%M (0 : 'I_2)). *)

Definition Bezout_mx (a b : Z) m (k : 'I_m) : 'M_(1+m) :=
	let '(g, (u,v)) := Zgcd_ex a b in
	let a1 := (a/g)%Z in let b1 := (b/g)%Z in
		combine_mx u v (-b1) a1 k.

Definition Bezout_step' (a b : Z) m n (M : 'M_(1+m, 1+n)) (k : 'I_m) : 'M_(1+m, 1+n) :=
	(Bezout_mx a b k) *m M.

Definition Bezout_step (a b : Z) m n (M : 'M_(1+m, 1+n)) (k : 'I_m) : 'M_(1+m, 1+n) :=
	let '(g, (u,v)) := Zgcd_ex a b in
	let a1 := (a/g)%Z in let b1 := (b/g)%Z in
		combine_step u v (-b1) a1 M k.

Local Arguments index_enum : simpl never.
(* Local Arguments index_enum_vector : simpl never. *)
Local Arguments Nat.eqb : simpl nomatch.
Local Arguments vector_lookup : simpl never.

Lemma combine_mx_step_eq (a b c d : Z) m n (M : 'M_(1+m,1+n)) k :
	combine_step a b c d M k = (combine_mx a b c d k) *m M.
Proof.
	apply mx_lookup_eq; intros i j.
	rewrite mx_mult_lookup.
	destruct (destruct_index i) as [->|[i' ->]],
	         (destruct_index j) as [->|[j' ->]].
	- unfold combine_step, combine_mx, mx_val.
		unfold mx_of_finfun, get_row. rewrite lookup_vec_of_finfun.
		cbn; simpC_Z.
		rewrite <- (vec_of_lookup (get_col _ _)).
		cbn.
		rewrite zip_with_fmap_same.
		rewrite <- (take_drop_middle (index_enum _) (0 : 'I_(S m)) 0) by
			by apply index_enum_lookup_nat.
		rewrite take_0; cbn; rewrite get_col_lookup.
		rewrite <- (take_drop_middle (drop _ _) k (S_index k)) by
			(rewrite lookup_drop; apply index_enum_lookup_nat).
		rewrite fmap_app, Zsum_list_app; cbn; rewrite Nat.eqb_refl, get_col_lookup.
		do 2 try (rewrite Zsum_list_filter_0, filter_Forall_not; cbn).
		1: shelve.
		all: apply Forall_fmap, Forall_forall; intros x H [].
		1: apply elem_of_drop in H as (i & Hi & ltki).
		2: apply elem_of_take in H as (i & Hi & ltik).
		all: rewrite lookup_drop, index_enum_lookup in Hi; apply index_option_Some in Hi.
		all: enough ((0 =? x) = false /\ (x =? S k) = false) as [-> ->] by apply Z.mul_0_l;
			split; apply Nat.eqb_neq; lia.
		Unshelve.
		rewrite Z.add_0_r, Z.add_0_l.
		setoid_rewrite vector_lookup_list_lookup_2 at 2; simpl.
		2:{ rewrite list_lookup_insert_ne by done.
			rewrite list_lookup_insert by (rewrite vector_length; apply Nat.lt_0_succ).
			done. }
		rewrite vector_add_lookup, !vector_scale_lookup; simpC_Z. done.
Abort. (* This is already in coqeal for ints as combine_stepE *)

Definition find1 m n (M : 'M_(1+m, 1+n)) (a : Z) : option 'I_m :=
	let (_,C') := destruct_vector (get_col M 0) in
	fst <$> (vector_find (λ b, negb (a %| b)%Z) C').

Definition find2 m n (M : 'M_(1+m, 1+n)) (a : Z) : option ('I_(1+m) * 'I_n) :=
	let M' := rsubmx M in
	matrix_find (λ b, negb (a %| b)%Z) M'.

Definition find_pivot m n (M : 'M_(1+m, 1+n)) : option ('I_(1+m) * 'I_(1+n)) :=
	matrix_find (λ b, negb (b =? 0))%Z M.

Import IfNotations.

Fixpoint improve_pivot_rec (k : nat) {m n} :
	'M_(1+m) → 'M_(1+m, 1+n) → 'M_(1+n) →
	'M_(1+m) * 'M_(1+m, 1+n) * 'M_(1+n) :=
	(* let _ := print ("improve pivot"%string, k) in *)
	match k with
	| 0 => fun P M Q => (P,M,Q)
	| S p => fun P M Q =>
		(* let _ := print (list_of_matrix M) in *)
		let a := (mx_val M 0 0) in
		if find1 M a is Some i then
			(* let _ := print ("find1"%string, nat_of_index i) in *)
			let Mi0 := mx_val M (index_plus i) 0 in
			let P := Bezout_step a Mi0 P i in
			let M := Bezout_step a Mi0 M i in
			improve_pivot_rec p P M Q
		else
			let u  := dlsubmx M in let vM := ursubmx M in let vP := usubmx P in
			let u' := map_mx (fun x => 1 - x / a)%Z u in
			let P  := col_mx (usubmx P) (u' *m vP + dsubmx P) in
			let M  := block_mx (mx1x1 a) vM
			                   (mx_const a) (u' *m vM + drsubmx M) in
			(* let _ := print ("all pivots"%string, list_of_matrix M) in *)
			if find2 M a is Some (i,j) then
				(* let _ := print ("find2"%string, nat_of_index i, nat_of_index j) in *)
				let M := xrow 0 i M in let P := xrow 0 i P in
				let a := mx_val M 0 0 in
				let M0ij := mx_val M 0 (index_plus j) in
				let Q := (Bezout_step a M0ij Q^T j)^T in
				let M := (Bezout_step a M0ij M^T j)^T in
				improve_pivot_rec p P M Q
			else (P, M, Q)
  end.

Definition improve_pivot k m n (M : 'M_(1+m, 1+n)) :=
  improve_pivot_rec k 1 M 1.

Fixpoint Smith m n : 'M_(m,n) → 'M_m * list Z * 'M_n :=
  match m, n with
  | S _, S _ => fun M : 'M_(1+_, 1+_) =>
	(* let _ := print ("Smith"%string, list_of_matrix M) in *)
	match find_pivot M with | Some (i, j) =>
		(* let _ := print ("find_pivot"%string, nat_of_index i, nat_of_index j) in *)
		let a := mx_val M i j in
		let M := xrow i 0 (xcol j 0 M) in
		(* this is where Euclidean norm eases termination argument *)
		let '(P,M,Q) := improve_pivot (Z.abs_nat a) M in
		(* let _ := print ("improved pivot"%string, list_of_matrix M) in *)
		let a  := mx_val M 0 0 in
		let u  := dlsubmx M in let v := ursubmx M in
		let v' := map_mx (fun x => x / a)%Z v in
		let M  := (drsubmx M + mx_neg (mx_const 1%Z *m v))%M in
		let '(P', d, Q') := Smith (map_mx (fun x => x / a)%Z M) in
			(lift0_mx P' *m block_mx 1 0 (mx_const (-1)%Z) 1 *m xcol i 0 P,
			a :: ((λ x, x * a)%Z <$> d),
			xrow j 0 Q *m block_mx 1 (mx_neg v') 0 1 *m lift0_mx Q')
	| None => (1%M, [], 1%M)
		end
  | _, _ => fun M => (1%M, [], 1%M)
  end.

Section faster.
(* The same functions, but without calculating the transformation matrices *)
Fixpoint improve_pivot_rec_faster (k : nat) {m n} : 'M_(1+m, 1+n) → 'M_(1+m, 1+n) :=
	match k with
	| 0 => fun M => M
	| S p => fun M =>
		let a := (mx_val M 0 0) in
		if find1 M a is Some i then
			let Mi0 := mx_val M (index_plus i) 0 in
			let M := Bezout_step a Mi0 M i in
			improve_pivot_rec_faster p M
		else
			let u  := dlsubmx M in let vM := ursubmx M in
			let u' := map_mx (fun x => 1 - x / a)%Z u in
			let M  := block_mx (mx1x1 a) vM
			                   (mx_const a) (u' *m vM + drsubmx M) in
			if find2 M a is Some (i,j) then
				let M := xrow 0 i M in
				let a := mx_val M 0 0 in
				let M0ij := mx_val M 0 (index_plus j) in
				let M := (Bezout_step a M0ij M^T j)^T in
				improve_pivot_rec_faster p M
			else M
  end.

Definition improve_pivot_faster k m n (M : 'M_(1+m, 1+n)) :=
  improve_pivot_rec_faster k M.

Fixpoint Smith_faster m n : 'M_(m,n) → list Z :=
  match m, n with
  | S _, S _ => fun M : 'M_(1+_, 1+_) =>
    match find_pivot M with | Some (i, j) =>
      let a := mx_val M i j in
	  let M := xrow i 0 (xcol j 0 M) in
      (* this is where Euclidean norm eases termination argument *)
      let M := improve_pivot_faster (Z.abs_nat a) M in
      let a  := mx_val M 0 0 in
      let u  := dlsubmx M in let v := ursubmx M in
      let v' := map_mx (fun x => x / a)%Z v in
      let M  := (drsubmx M + mx_neg (mx_const 1%Z *m v))%M in
      let d  := Smith_faster (map_mx (fun x => x / a)%Z M) in
       a :: ((λ x, x * a)%Z <$> d)
    | None => []
		end
  | _, _ => fun M => []
  end.

Lemma improve_pivot_faster_eq (k : nat) {m n}
	(P : 'M_(1+m)) (M : 'M_(1+m, 1+n)) (Q : 'M_(1+n)) :
	(improve_pivot_rec k P M Q).1.2 = improve_pivot_faster k M.
Proof.
	induction k in m, n, P, M, Q |- *; simpl; [done|].
	case_match; [done|].
	case_match; [|done].
	by destruct p.
Qed.

Lemma Smith_faster_eq m n (M : 'M_(m,n)) : Smith_faster M = (Smith M).1.2.
Proof.
	induction m in n, M |- *; simpl; [done|]; destruct n; simpl; [done|].
	case_match; [|done]; destruct p.
	case_match;	case_match;	case_match;	case_match;	subst; simpl.
	erewrite <- improve_pivot_faster_eq. unfold improve_pivot in H0. rewrite H0; simpl.
	f_equal. rewrite IHm.
	by rewrite H2.
Qed.
End faster.

(*
Compute list_of_matrix example_mx.
Compute let '(A,d,B) := Smith example_mx in (list_of_matrix A, d, list_of_matrix B). *)

Example example_draft : 'M_4 :=
	[vec [vec 2; 4; 5; 12];
		[vec 0; 4; 5; 10];
		[vec 2; 2; 4; 10];
		[vec 2; 6; 8; 18]]%Z.
(* Time Compute let '(A,d,B) := Smith example_draft in (list_of_matrix A, d, list_of_matrix B). *)

Example example_mx2 : 'M_3 :=
	[vec [vec  2; 4; 4];
	     [vec -6; 6;12];
			 [vec 10; 4;16]]%Z.
(* Time Compute let '(A,d,B) := Smith example_mx2 in (list_of_matrix A, d, list_of_matrix B). *)

Example example_mx2b : 'M_3 :=
	[vec [vec  0; 4; 4];
	     [vec -6; 6;12];
			 [vec 10; 4;16]]%Z.
(* Time Compute let '(A,d,B) := Smith example_mx2b in (list_of_matrix A, d, list_of_matrix B). *)

Example example_mx2c : 'M_3 :=
	[vec [vec -6; 6;12];
		 [vec  0; 4; 4];
			 [vec 10; 4;16]]%Z.
(* Time Compute let '(A,d,B) := Smith example_mx2c in (list_of_matrix A, d, list_of_matrix B). *)

(* Compute let '(A,d,B) := Smith example_mx2 in
	let '(A2,d2,B2) := Smith A in
	d2. *)

Example example_mx3 : 'M_5 :=
[vec [vec    25;   -300;    1050;   -1400;    630];
     [vec  -300;   4800;  -18900;   26880; -12600];
     [vec  1050; -18900;   79380; -117600;  56700];
     [vec -1400;  26880; -117600;  179200; -88200];
     [vec   630; -12600;   56700;  -88200;  44100]]%Z.
(* Time Compute let '(A,d,B) := Smith example_mx3 in d. *)

Example example_mx4 : 'M_5 :=
[vec [vec 3; 5; 2; 4;  4];
     [vec 1; 21; 8; 7; 7];
     [vec 2; 4; 17; 3; 3];
     [vec 1; 9; 1; 3; 3];
     [vec 1; 4; 9; 5; 5]]%Z.
(* Timeout 5 Time Compute let d := Smith_faster example_mx4 in d. *)
(* Time Compute let '(A,d,B) := Smith example_mx4 in d. *)

(* Example example_mx5 :=
[vec [vec 8; 7; 2; 3; 5; 6; 1; 9; 0; 3];
     [vec 4; 7; 7; 6; 8; 9; 9; 8; 5; 3];
     [vec 9; 5; 2; 2; 7; 4; 5; 6; 8; 1];
     [vec 3; 7; 4; 3; 3; 5; 2; 3; 4; 5];
     [vec 7; 0; 4; 9; 8; 6; 8; 0; 8; 6];
     [vec 6; 4; 2; 9; 6; 3; 1; 8; 0; 8];
     [vec 0; 6; 4; 9; 8; 8; 2; 6; 7; 3];
     [vec 7; 8; 0; 7; 2; 2; 9; 5; 4; 6];
     [vec 8; 0; 2; 4; 0; 7; 2; 5; 6; 3];
     [vec 8; 9; 0; 5; 0; 4; 6; 2; 3; 9]]%Z. *)
(* Time Timeout 5 Compute Smith_faster example_mx5. *)
(* Time Timeout 5 Compute let '(A,d,B) := Smith example_mx5 in d. *)

Definition smith_tester {n} (M : 'M_n) : bool :=
	let '(A,d,B) := Smith M in
	bool_decide (list_of_matrix (A *m M *m B) =
	        list_of_matrix (diagonal_mx 0%Z (d ++ (repeat 0%Z (n - length d))))).

Goal smith_tester example_draft = true. reflexivity. Qed.
Goal smith_tester example_mx2 = true. reflexivity. Qed.
Goal smith_tester example_mx2b = true. reflexivity. Qed.
Goal smith_tester example_mx2c = true. reflexivity. Qed.
Goal smith_tester example_mx3 = true. reflexivity. Qed.
Goal smith_tester example_mx4 = true. reflexivity. Qed.

Definition mx_rank m n (M : 'M_(m,n)) : nat :=
	length (Smith_faster M).

Definition mx_nullity m n (M : 'M_(m,n)) : nat :=
	n - mx_rank M.

Lemma Smith_faster_square_length m (M : 'M_m) : length (Smith_faster M) ≤ m.
Proof.
	induction m; simpl; [done|].
	case_match; [|simpl;lia].
	destruct p; simpl; apply le_n_S.
	remember (improve_pivot_faster (Z.abs_nat (M i i0)) (xrow i 0%nat (xcol i0 0%nat M)) 0%nat 0%nat).
	rewrite length_fmap.
	apply IHm.
Qed.

Lemma square_rank_nullity m (M : 'M_m) : mx_rank M + mx_nullity M = m.
Proof.
	unfold mx_nullity, mx_rank.
	eauto using Smith_faster_square_length with arith.
Qed.
