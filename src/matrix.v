From stdpp Require Import prelude options.
From thesis Require Import lib index vector.

Coercion Z.of_nat : nat >-> Z.

Local Notation vmap := (fmap (FMap:=vector_fmap)) (only parsing).

Section MatrixDef.
	Context {Z : Type}.
	Context {m n : nat}.
	(* Context `{Z_inhabited : Inhabited Z}. *)

	(* Matrices are stored in row major form *)
	Definition matrix := vector (vector Z n) m.

	Implicit Type M : matrix.

	Definition mx_dim M := (m,n).

	(*
		TODO:
		Try extracting and see if size data gets erased
		Maybe look at stdpp finite maps
	*)

	Definition list_of_matrix (M : matrix) : list (list Z) :=
		list_of_vector <$> (list_of_vector M).

	Definition mx_val M (i : 'I_m) (j : 'I_n) : Z := M !# i !# j.
	Coercion mx_val : matrix >-> Funclass. (* todo: make this more transparent *)

	Lemma mx_lookup_eq M1 M2 : (∀ i j, M1 i j = M2 i j) → M1 = M2.
	Proof.
		intros H.
		do 2 (eapply vector_lookup_eq; intros ?).
		apply H.
	Qed.

	Definition get_row M i : vector Z n := M !# i.
	Definition get_col M j : vector Z m :=
		vmap (λ v : vector Z n, v !# j) M.

	Definition mx_of_fun (f : nat → nat → Z) : matrix :=
		vec_of_fun _ (λ i, vec_of_fun _ (f i)).

	(* Kind of annoying that we have both fun and finfun *)
	Definition mx_of_finfun (f : 'I_m → 'I_n → Z) : matrix :=
		vec_of_finfun (λ i, vec_of_finfun (f i)).

	Lemma mx_of_fun_val f i j :
		(mx_of_fun f) i j = f i j.
	Proof.
		unfold mx_val, mx_of_fun.
		by rewrite 2 lookup_vec_of_fun.
	Qed.

	Lemma mx_of_finfun_val f i j :
		(mx_of_finfun f) i j = f i j.
	Proof.
		unfold mx_val, mx_of_finfun.
		by rewrite 2 lookup_vec_of_finfun.
	Qed.

	Definition mx_const (val : Z) : matrix :=
		[vec of replicate _ [vec of replicate _ val]].

	Lemma mx_const_correct val : mx_const val = mx_of_finfun (λ _ _, val).
	Proof.
		do 2 (apply vector_eq; simpl; erewrite const_fmap; [|done]; f_equal; try done).
	Qed.

	Definition mx_zipwith (f : Z → Z → Z) (M1 M2 : matrix) : matrix.
		unfold matrix in *.
		exact [vec of zip_with (λ r1 r2, zip_with_vector f r1 r2) M1 M2].
	Defined.

	Lemma mx_zipwith_lookup f M1 M2 i j :
		mx_zipwith f M1 M2 i j = (f (M1 i j) (M2 i j)).
	Proof.
		unfold mx_zipwith, mx_val.
		erewrite vector_zip_with_lookup.
		fold [vec of zip_with f (M1 !# i) (M2 !# i)].
		erewrite vector_zip_with_lookup.
		done.
	Qed.

	Lemma mx_zipwith_eq f M1 M2 :
		mx_zipwith f M1 M2 = mx_of_finfun (λ i j, f (M1 i j) (M2 i j)).
	Proof.
		apply mx_lookup_eq; intros i j.
		rewrite mx_of_finfun_val.
		apply mx_zipwith_lookup.
	Qed.

End MatrixDef.

Declare Scope matrix_scope.
Delimit Scope matrix_scope with M.
Bind Scope matrix_scope with matrix.

Open Scope matrix_scope.
Open Scope Z_scope.
Open Scope nat_scope.

Notation "'M[ Z ]_( m , n )" := (@matrix Z m n)
  (at level 8, format "''M[' Z ]_( m ,  n )"). (* todo level? *)
Notation "'M[ Z ]_( m )" := 'M[Z]_(m,m)
  (at level 8, format "''M[' Z ]_( m )").
Notation "'M[ Z ]_ m" := 'M[Z]_(m)
  (at level 8, m at level 2, format "''M[' Z ]_ m").
Notation "'M_( m , n )" := 'M[_]_(m,n)
  (at level 8, format "''M_(' m ,  n )"). (* todo level? *)
Notation "'M_( m )" := 'M[_]_m
  (at level 8, format "''M_(' m )").
Notation "'M_ m" := 'M[_]_m
  (at level 8, m at level 2, format "''M_' m").

Section MatrixOps.

	Context {Z : Type}.
	Context `{Z_inhabited : Inhabited Z}.

	Definition map_mx A B m n (f : A → B) (M : 'M[A]_(m,n)) : 'M[B]_(m,n) :=
		vmap (vmap f) M.

	Definition row_mx m n1 n2 (A : 'M[Z]_(m, n1)) (B : 'M_(m, n2)) : 'M[Z]_(m, n1 + n2) :=
		[vec of zip_with app_vector (list_of_vector A) (list_of_vector B)].
	Definition col_mx m1 m2 n (A : 'M_(m1, n)) (B : 'M_(m2, n)) : 'M[Z]_(m1+m2, n) :=
		app_vector A B.
	Definition block_mx m1 m2 n1 n2
		(A : 'M_(m1, n1)) (B : 'M_(m1, n2))
		(C : 'M_(m2, n1)) (D : 'M_(m2, n2)) : 'M_(m1+m2, n1+n2) :=
		col_mx (row_mx A B) (row_mx C D).

	Definition usubmx m1 m2 n (M :'M_(m1+m2, n)) : 'M[Z]_(m1, n) :=
		take_vector M.
	Definition dsubmx m1 m2 n (M :'M_(m1+m2, n)) : 'M[Z]_(m2, n) :=
		drop_vector M.
	Definition lsubmx m n1 n2 (M : 'M_(m, n1 + n2)) : 'M[Z]_(m, n1) :=
		vmap (λ v, take_vector v) M.
	Definition rsubmx m n1 n2 (M : 'M_(m, n1 + n2)) : 'M[Z]_(m, n2) :=
		vmap (λ v, drop_vector v) M.

	Definition ulsubmx m1 m2 n1 n2 (M : 'M_(m1+m2, n1+n2)) : 'M_(m1, n1) :=
		lsubmx (usubmx M).
	Definition ursubmx m1 m2 n1 n2 (M : 'M_(m1+m2, n1+n2)) : 'M_(m1, n2) :=
		rsubmx (usubmx M).
	Definition dlsubmx m1 m2 n1 n2 (M : 'M_(m1+m2, n1+n2)) : 'M_(m2, n1) :=
		lsubmx (dsubmx M).
	Definition drsubmx m1 m2 n1 n2 (M : 'M_(m1+m2, n1+n2)) : 'M_(m2, n2) :=
		rsubmx (dsubmx M).

	Lemma usub_col_mx m1 m2 n (A : 'M_(m1, n)) (B : 'M_(m2, n)) :
		usubmx (col_mx A B) = A.
	Proof. by apply vector_eq; simplify_list_eq. Qed.

	Lemma lsub_row_mx m n1 n2 (A : 'M_(m, n1)) (B : 'M_(m, n2)) :
		lsubmx (row_mx A B) = A.
	Proof.
		apply vector_eq; simpl.
		rewrite fmap_zip_with_l;
			[done| |by rewrite !vector_length].
		intros; by apply vector_eq; simplify_list_eq.
	Qed.

	Lemma ulsub_block_mx m1 m2 n1 n2
		(A : 'M_(m1, n1)) (B : 'M_(m1, n2))
		(C : 'M_(m2, n1)) (D : 'M_(m2, n2)) :
		ulsubmx (block_mx A B C D) = A.
	Proof. unfold block_mx, ulsubmx. by rewrite usub_col_mx, lsub_row_mx. Qed.
	(* todo... dsub_col_mx, rsub_row_mx, ul/dl/ur/drsub_block_mx *)

	Definition mx_transpose (m n : nat) (M : 'M[Z]_(m,n)) : 'M[Z]_(n,m).
	(* induction M as [|r m' _ M'] using vector_induction.
	- exact [vec of replicate n nil_vector].
	- exact [vec of zip_with (@cons_vector m' _) r (list_of_vector M')].
	Qed. *)
	
	refine (vector_recursion (B:= fun m=> 'M[Z]_(n,m)) _ _ M); clear M.
	- exact [vec of replicate n nil_vector].
	- intros r m' _ M'.
		exact [vec of zip_with (@cons_vector m' _) r (list_of_vector M')].
	Defined.

	Local Notation "m ^T" := (mx_transpose m) (at level 0, format "m '^T'") : matrix_scope.

	Lemma mx_transpose_lookup m n (M : 'M_(m,n)) i j : mx_transpose M i j = M j i.
	Proof.
		unfold mx_val.
		induction M using vector_induction.
		- by destruct index_0_false.
		- unfold mx_transpose. rewrite vector_recursion_cons.
			cbn. fold M^T.
			erewrite vector_zip_with_lookup.
			destruct (destruct_index j) as [-> | [j' ->]].
			+ unfold vector_lookup; simpl. done. (* Make it so we dont get a giant term *)
			+ unfold vector_lookup; cbn. rewrite 2 vector_tail_cons. auto.
	Qed.

	Lemma mx_transpose_eq m n (M : 'M_(m,n)) : M^T = mx_of_finfun (λ i j, M j i).
	Proof.
		unfold mx_of_finfun.
		do 2 (eapply vector_lookup_eq; intros; rewrite lookup_vec_of_finfun).
		apply mx_transpose_lookup.
	Qed.

	Lemma mx_transpose_involution m n (M : 'M_(m,n)) : M^T^T = M.
	Proof. by eapply mx_lookup_eq; intros i j; rewrite 2 mx_transpose_lookup. Qed.

	Lemma mx_transpose_get_col m n (M : 'M_(m,n)) i : get_row M^T i = get_col M i.
	Proof.
		unfold get_row, get_col.
		eapply vector_lookup_eq; intros j.
		fold (M^T i j); rewrite mx_transpose_lookup.
		by erewrite vector_fmap_lookup.
	Qed.

	Lemma mx_transpose_get_row m n (M : 'M_(m,n)) i : get_col M^T i = get_row M i.
	Proof.
		unfold get_col, get_row.
		eapply vector_lookup_eq; intros j.
		erewrite vector_fmap_lookup.
		apply mx_transpose_lookup.
	Qed.

	(* Lemma lookup_total_mx_of_fun m n f i :
		i < m →
		(mx_of_fun m n f) !!! i = f i <$> (seq 0 n).
	Proof.
		intros H. unfold mx_of_fun.
		rewrite (list_lookup_total_fmap (λ i0, (λ j, f i0 j) <$> seq 0 n) (seq 0 m) i).
		2:{ by rewrite seq_length. }
		rewrite lookup_total_seq_lt; [|done].
		done.
	Qed. *)

	Definition mx1x1 (a : Z) : 'M_1 := [vec [vec a]].

	Lemma mx1x1_lookup a i j : mx1x1 a i j = a.
	Proof. by rewrite (index_1_ind0 i), (index_1_ind0 j). Qed.

	Lemma mx1x1_eq a b : mx1x1 a = mx1x1 b → a = b.
	Proof. by intros [=]. Qed.

	Definition row_from_vector n (v : vector Z n) : 'M_(1,n) := [vec v].

	Definition col_from_vector n (v : vector Z n) : 'M_(n,1) :=
		vmap (λ x : Z, [vec x]) v.

	Definition vector_from_row n (M : 'M_(1,n)) : vector Z n := hd_vector M.

	Definition vector_from_col n (M : 'M_(n,1)) : vector Z n :=
		vmap hd_vector M.

	Lemma vector_from_row_from_vector n (v : vector Z n) :
		vector_from_row (row_from_vector v) = v.
	Proof. done. Qed.

	Lemma vector_from_col_from_vector n (v : vector Z n) :
		vector_from_col (col_from_vector v) = v.
	Proof.
		apply vector_eq; simpl.
		rewrite <- list_fmap_compose; unfold "∘"; cbn.
		by rewrite list_fmap_id.
	Qed.

	Lemma col_from_vector_from_col n (M : 'M_(n,1)) :
		col_from_vector (vector_from_col M) = M.
	Proof.
		eapply vector_lookup_eq; intros i.
		unfold col_from_vector, vector_from_col.
		erewrite 2 vector_fmap_lookup.
		by rewrite vector_S, vector_0 with (v := tail_vector _).
	Qed.

	Lemma cons_row_col_mx m n (M : 'M_(m,n)) r :
		cons_vector r M = col_mx (row_from_vector r) M.
	Proof. by apply vector_eq. Qed.

	Lemma col_mx_transpose m1 m2 n (A : 'M_(m1, n)) (B : 'M_(m2, n)) :
		(col_mx A B)^T = row_mx A^T B^T.
	Proof.
		apply mx_lookup_eq; intros i j.
		rewrite mx_transpose_lookup.
		unfold col_mx, mx_val; fold [vec of A ++ B].
		edestruct (app_vector_lookup) as [(j' & -> & ->) | (j' & -> & ->)].
		- unfold row_mx. erewrite vector_zip_with_lookup.
			fold [vec of (A^T !# i) ++ (B^T !# i)]; rewrite app_vector_lookup_plus_bump.
			by fold (A^T i j'); rewrite mx_transpose_lookup.
		- unfold row_mx. erewrite vector_zip_with_lookup.
			fold [vec of (A^T !# i) ++ (B^T !# i)]; rewrite app_vector_lookup_plus.
			by fold (B^T i j'); rewrite mx_transpose_lookup.
	Qed.

	Lemma get_col_from_vector n (v : vector Z n) i :
		get_col (col_from_vector v) i = v.
	Proof.
		rewrite (index_1_ind0 i).
		apply vector_eq; simpl.
		rewrite <- list_fmap_compose.
		apply list_fmap_id.
	Qed.

	Lemma col_from_vector_lookup n (v : vector Z n) i :
		(col_from_vector v) !# i = [vec v !# i].
	Proof. unfold col_from_vector. by erewrite vector_fmap_lookup. Qed.

	Lemma get_row_from_vector n (v : vector Z n) i :
		get_row (row_from_vector v) i = v.
	Proof. by rewrite (index_1_ind0 i). Qed.

	Lemma col_from_vector_transpose n (v : vector Z n) :
		(col_from_vector v)^T = row_from_vector v.
	Proof.
		apply mx_lookup_eq; intros i j.
		rewrite mx_transpose_lookup.
		unfold mx_val.
		rewrite (index_1_ind0 i).
		by rewrite col_from_vector_lookup.
	Qed.

	Lemma row_from_vector_transpose n (v : vector Z n) :
		(row_from_vector v)^T = col_from_vector v.
	Proof.
		apply mx_lookup_eq; intros i j.
		rewrite mx_transpose_lookup.
		unfold mx_val.
		rewrite (index_1_ind0 j).
		by rewrite col_from_vector_lookup.
	Qed.

	Definition matrix_find m n (P : Z → Prop) `{∀ x, Decision (P x)}
		(M : 'M[Z]_(m,n)) : option ('I_m * 'I_n). (* return value? *)
	Proof.
		unfold matrix in M.
		destruct (vector_find (λ r, is_Some $ vector_find P r) M) as [[i v]|]; [|apply None].
			(* TODO: dont want to find twice... *)
		destruct (vector_find P v) as [[j _]|]; [|apply None (*impossible*)].
		exact (Some (i,j)).
	Defined.

	Global Arguments matrix_find {m n} P {_} M.

	Definition xrow m n (i1 i2 : 'I_m) (M : 'M[Z]_(m,n)) : 'M[Z]_(m,n) :=
		vector_x i1 i2 M.

	Definition xcol m n (j1 j2 : 'I_n) (M : 'M_(m,n)) : 'M[Z]_(m,n) :=
		vmap (vector_x j1 j2) M. (* explicit M *)

	Definition delete_row m n (i : 'I_(S m)) (M : 'M_(S m, n)) : 'M[Z]_(m, n) :=
		delete_vector i M.

	Definition delete_col m n (i : 'I_(S n)) (M : 'M_(m, S n)) : 'M[Z]_(m, n) :=
		vmap (delete_vector i) M.

	Definition matrix_minor m n (M : 'M_(S m, S n)) i j :=
		delete_row i (delete_col j M).

	Lemma get_col_lookup m n (M : 'M[Z]_(m,n)) i j : (get_col M j) !# i = M i j.
	Proof. unfold get_col. by erewrite vector_fmap_lookup. Qed.

	Definition scalar_mx {n} (z : Z) (a : Z) : 'M_n :=
		mx_of_finfun (λ i j, if decide (i = j) then a else z).

	Definition diagonal_mx (z : Z) (l : list Z) : 'M[Z]_(length l) := (* dont use normal lookup? *)
		mx_of_finfun (λ i j, if decide (i = j) then l !!! (nat_of_index i) else z).

	(* Fail Lemma mx_row_ind m (P : ∀ n, 'M_(m,n) → Type) :
		(∀ M, P 0 M) →
		(∀ m r M, P m M → P (S m)). *)
End MatrixOps.

Notation "m ^T" := (mx_transpose m) (at level 0, format "m '^T'") : matrix_scope.

	(*** Matrices over Z ***)
From CoqEAL Require Import hrel param refinements.

Import Refinements.Op.

Section Z_matrix.

Variable Z : Type.

Context `{zero_of Z, add_of Z, mul_of Z, opp_of Z, one_of Z}.
Context `{Inhabited Z}.
Local Open Scope computable_scope.

Definition mx_zero {m n} : 'M[Z]_(m,n) := mx_const 0%C.
Notation "0" := mx_zero : matrix_scope.

Definition mx_add {m n} := @mx_zipwith Z m n add_op.
Notation "x + y" := (mx_add x y) (at level 50, left associativity) : matrix_scope.

Definition mx_neg {m n} (M : 'M[Z]_(m,n)) := map_mx opp_op M.

Definition mx_id {n} : 'M[Z]_n := scalar_mx 0%C 1%C.
Notation "1" := mx_id : matrix_scope.

Definition mx_apply n m (M : 'M_(n,m)) (v : vector Z m) : vector Z n :=
	vmap (λ Mrow, inner_prod Mrow v) M.

Definition mx_mult p q r (M1 : 'M_(p,q)) (M2 : 'M_(q,r)) : 'M_(p,r) :=
	mx_transpose (vmap (mx_apply M1) (mx_transpose M2)).

Notation "x *m y" := (mx_mult x y) (at level 40, left associativity) : matrix_scope.

Definition lift0_mx n (M : 'M_n) : 'M_(1+n) :=
	block_mx (mx1x1 1%C) 0 0 M.

Lemma mx_add_lookup m n (M1 M2 : 'M[Z]_(m,n)) i j :
	(M1 + M2)%M i j = (M1 i j + M2 i j)%C.
Proof. apply mx_zipwith_lookup. Qed.

Lemma mx_add_eq m n (M1 M2 : 'M[Z]_(m,n)) :
	(M1 + M2)%M = mx_of_finfun (λ i j, (M1 i j) + (M2 i j))%C.
Proof. apply mx_zipwith_eq. Qed.

Lemma mx_neg_lookup m n (M : 'M_(m,n)) i j : (mx_neg M) i j = - (M i j).
Proof. unfold mx_neg, map_mx, mx_val. by erewrite 2 vector_fmap_lookup. Qed.

Lemma mx_apply_lookup n m (M : 'M_(n,m)) v i : (mx_apply M v) !# i = inner_prod (M !# i) v.
Proof. apply vector_fmap_lookup. Qed.

Lemma inner_prod_finfun n (v w : vector Z n) :
	inner_prod v w = foldr add_op 0%C (vec_of_finfun (λ i, v !# i * w !# i)%C).
Proof.
	unfold inner_prod, vector_total.
	repeat f_equal.
	eapply vector_lookup_eq; intros i.
	by erewrite lookup_vec_of_finfun, vector_zip_with_lookup.
Qed.

Lemma mx_apply_col_mx m1 m2 n (A : 'M_(m1,n)) (B : 'M_(m2,n)) (v : vector Z n) :
	mx_apply (col_mx A B) v = [vec of (mx_apply A v) ++ (mx_apply B v)].
Proof. apply vector_eq; simpl; by rewrite fmap_app. Qed.

Lemma mx_mult_lookup p q r (M1 : 'M_(p,q)) (M2 : 'M_(q,r)) i j :
	(M1 *m M2) i j = inner_prod (get_row M1 i) (get_col M2 j).
Proof.
	unfold mx_mult.
	rewrite mx_transpose_lookup. unfold mx_val.
	erewrite vector_fmap_lookup.
	by rewrite mx_apply_lookup, <- mx_transpose_get_col.
Qed.

Lemma mx_mult_lookup_2 p q r (A : 'M_(p,q)) (B : 'M_(q,r)) i :
	get_col (A *m B) i = mx_apply A (get_col B i).
Proof.
	eapply vector_lookup_eq; intros j.
	unfold get_col. erewrite vector_fmap_lookup.
	rewrite mx_apply_lookup.
	fold (mx_val (A *m B) j i); rewrite mx_mult_lookup.
	done.
Qed.

Lemma inner_prod_mx_apply_row n (v w : vector Z n) :
	[vec inner_prod v w] = mx_apply (row_from_vector v) w.
Proof.
	eapply vector_lookup_eq; intros i.
	by rewrite (index_1_ind0 i).
Qed.

Lemma inner_prod_mx_prod n (v w : vector Z n) :
	mx1x1 $ inner_prod v w = (row_from_vector v) *m (col_from_vector w).
Proof.
	apply mx_lookup_eq; intros i j.
	rewrite mx_mult_lookup.
	rewrite get_col_from_vector, get_row_from_vector.
	apply mx1x1_lookup.
Qed.

Lemma inner_prod_cons (a b : Z) n (v w : vector Z n) :
	inner_prod (cons_vector a v) (cons_vector b w) = (a*b + inner_prod v w)%C.
Proof. done. Qed.

End Z_matrix.

Notation "0" := (mx_zero (Z:=_)) : matrix_scope.
Notation "x + y" := (mx_add x y) (at level 50, left associativity) : matrix_scope.
Notation "1" := (mx_id (Z:=_)) : matrix_scope.
(* Notation "1[ Z ]" := (mx_id (Z:=Z)) : matrix_scope.
Notation "1_ n" := (mx_id (Z:=_) (n:=n)) : matrix_scope. *)
Notation "x *m y" := (mx_mult x y) (at level 40, left associativity) : matrix_scope.

Section Z_matrix_binint.

Lemma mx_neg_neg m n (M : 'M[Z]_(m,n)) : mx_neg (mx_neg M) = M.
Proof. apply mx_lookup_eq; intros; rewrite 2 mx_neg_lookup; simpC_Z. lia. Qed.

Lemma mx_apply_add n m (M : 'M_(n,m)) (v w : vector Z m) :
	mx_apply M (vector_add v w) = vector_add (mx_apply M v) (mx_apply M w).
Proof.
	eapply vector_lookup_eq; intros i.
	rewrite vector_add_lookup, 3 mx_apply_lookup.
	by rewrite inner_prod_add_r.
Qed.

Lemma mx_add_apply n m (M1 M2 : 'M[Z]_(n,m)) v :
	mx_apply (M1 + M2) v = vector_add (mx_apply M1 v) (mx_apply M2 v).
Proof.
	eapply vector_lookup_eq; intros i.
	rewrite vector_add_lookup, !mx_apply_lookup.
	rewrite <- inner_prod_add_l. f_equal.
	eapply vector_lookup_eq; intros j.
	fold (mx_val (M1 + M2) i j). (* meh *)
	by rewrite mx_add_lookup, vector_add_lookup.
Qed.

Lemma mx_apply_id n (v : vector Z n) : mx_apply 1 v = v.
Proof.
	eapply vector_lookup_eq; intros i.
	rewrite mx_apply_lookup.
	cbn; unfold mx_id, scalar_mx, mx_of_finfun; simpC_Z.
	rewrite lookup_vec_of_finfun.
	set (x := v !# i); rewrite <- (vec_of_lookup v); subst x; simpl.
	rewrite zip_with_fmap_same.
	rewrite <- (take_drop_middle (index_enum n) i i)
		by apply index_enum_lookup_nat.
	simplify_list_eq.
	erewrite decide_True, Z.mul_1_l, Zsum_list_app by done; simpl.
	do 2 try (rewrite Zsum_list_filter_0, filter_Forall_not; simpl).
	1: apply Z.add_0_r.
	all: apply Forall_fmap, Forall_forall; intros x H [].
	all: rewrite decide_False; [apply Z.mul_0_l|].
	1: apply elem_of_drop in H as (j' & Hjj' & lej').
	2: apply elem_of_take in H as (j' & Hjj' & ltj').
	all: rewrite index_enum_lookup in Hjj'; apply index_option_Some in Hjj'; intro; subst; lia.
Qed.

Lemma mx_apply_0 n m (v : vector Z n) : mx_apply (0 : 'M_(m,n)) v = replicate_vector 0%C _.
Proof.
	eapply vector_lookup_eq; intros i.
	rewrite mx_apply_lookup.
	erewrite 2 vector_lookup_list_lookup_2; simpl; [|by apply lookup_replicate..].
	cbn; simpC_Z.
	erewrite zip_with_replicate_l by by rewrite vector_length.
	erewrite const_fmap by apply Z.mul_0_l.
	rewrite Zsum_list_replicate.
	apply Z.mul_0_r.
Qed.

Lemma mx_apply_row_mx m n1 n2 (A : 'M_(m,n1)) (B : 'M_(m,n2)) (v : vector Z (n1 + n2)) :
	mx_apply (row_mx A B) v = vector_add (mx_apply A (take_vector v)) (mx_apply B (drop_vector v)).
Proof.
	eapply vector_lookup_eq; intros i.
	rewrite vector_add_lookup, !mx_apply_lookup; simpC_Z.
	unfold row_mx. erewrite vector_zip_with_lookup.
	rewrite (vector_take_drop v) at 1.
	cbn; simpC_Z.
	rewrite zip_with_app by by rewrite !vector_length.
	apply Zsum_list_app.
Qed.

Lemma mx_mult_lookup_1 p q r (M1 : 'M[Z]_(p,q)) (M2 : 'M[Z]_(q,r)) i :
	(M1 *m M2) !# i = mx_apply M2^T (M1 !# i).
Proof.
	eapply vector_lookup_eq; intros j.
	fold (mx_val (M1 *m M2) i j).
	unfold mx_mult. rewrite mx_transpose_lookup. unfold mx_val.
	erewrite vector_fmap_lookup.
	rewrite 2 mx_apply_lookup.
	pose proof (mx_transpose_get_col M2 j); unfold get_row in H.
	rewrite H; clear H.
	apply inner_prod_comm.
Qed. (* todo: this proof is mostly bureaucracy *)

Lemma mx_apply_mult_col m n (M : 'M[Z]_(m,n)) v :
	mx_apply M v = vector_from_col (M *m col_from_vector v).
Proof.
	unfold vector_from_col.
	eapply vector_lookup_eq; intros i.
	by erewrite mx_apply_lookup, vector_fmap_lookup,
		mx_mult_lookup_1, col_from_vector_transpose,
		<- inner_prod_mx_apply_row, inner_prod_comm. (* funny *)
Qed.

Lemma mx_mult_transpose p q r (A : 'M[Z]_(p,q)) (B : 'M_(q,r)) :
	(A *m B)^T = B^T *m A^T.
Proof.
	apply mx_lookup_eq; intros i j.
	rewrite mx_transpose_lookup.
	rewrite 2 mx_mult_lookup.
	rewrite mx_transpose_get_col, mx_transpose_get_row.
	apply inner_prod_comm.
Qed.

Lemma inner_prod_adjoint m n (M : 'M[Z]_(m,n)) v w :
	inner_prod (mx_apply M v) w = inner_prod v (mx_apply M^T w).
Proof.
	induction M using vector_induction.
	- 	rewrite (vector_0 w); cbn; simpC_Z.
		rewrite fmap_replicate; cbn.
		rewrite zip_with_replicate_r_eq by done.
		erewrite const_fmap by (intros; apply Z.mul_0_r).
		rewrite Zsum_list_replicate; lia.
	- rewrite (vector_S w); destruct (destruct_vector w); simpl.
		rewrite cons_row_col_mx.
		rewrite (mx_apply_col_mx (m1:=1)).
		rewrite (col_mx_transpose (m1:=1)).
		rewrite (mx_apply_row_mx (n1:=1)).
		rewrite take_vector_cons_vector, drop_vector_cons_vector.
		rewrite row_from_vector_transpose.
		rewrite inner_prod_add_r.
		rewrite <- inner_prod_mx_apply_row.
		replace [vec of [vec inner_prod a v] ++ mx_apply M v] with
			(cons_vector (inner_prod a v) (mx_apply M v)) by
			by apply vector_eq.
		rewrite inner_prod_cons.
		rewrite IHM; f_equal.
		(* boring: *)
		rewrite Z.mul_comm, vector_scale_inner_prod_l, inner_prod_comm; f_equal.
		unfold mx_apply, col_from_vector; cbn; simpC_Z.
		rewrite <-vector_fmap_compose; unfold "∘"; cbn.
		unfold vector_scale; apply vector_eq; rewrite !list_of_vector_fmap; simpC_Z.
		apply list_fmap_ext; lia.
Qed.

Lemma mx_mult_apply p q r (M1 : 'M[Z]_(p,q)) (M2 : 'M_(q,r)) v :
	mx_apply (M1 *m M2) v = mx_apply M1 (mx_apply M2 v).
Proof.
	eapply vector_lookup_eq; intros i.
	rewrite !mx_apply_lookup, mx_mult_lookup_1.
	generalize (M1 !# i); intros w.
	by rewrite inner_prod_adjoint, mx_transpose_involution.
Qed.

Lemma mx_mult_assoc p q r s (A : 'M[Z]_(p,q)) (B : 'M_(q,r)) (C : 'M_(r,s)) :
	(A *m B) *m C = A *m (B *m C).
Proof.
	apply mx_lookup_eq; intros i j.
	rewrite 2 mx_mult_lookup.
	unfold get_row; rewrite mx_mult_lookup_1, mx_mult_lookup_2.
	generalize (A !# i), (get_col C j); intros v w.
	by rewrite inner_prod_adjoint, mx_transpose_involution.
Qed.

End Z_matrix_binint.
