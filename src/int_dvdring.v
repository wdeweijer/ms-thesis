(* In this file we prove that mathcomp int is a euclidean domain *)

From HB Require Import structures.
From Coq Require Import ssreflect ssrfun ssrbool.
Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import ssrnat div seq eqtype choice order.
From mathcomp Require Import ssralg ssrint ssrnum intdiv prime.
From CoqEAL Require Import stronglydiscrete dvdring polydvd.
Set Warnings "notation-overridden, ambiguous-paths".

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition prime_decomp2 (n : nat) :=
  tally_seq (prime_decomp n).

Lemma prime_decomp_wf_tally n : prime_decomp n \is a wf_tally _.
Proof.
  rewrite qualifE; apply/andP; split.
  apply primes_uniq.
  rewrite prime_decompE /unzip2 -map_comp /comp /=.
  apply/negP => /mapP [x].
  by rewrite -logn_gt0 => /[swap] ->; rewrite ltnn.
Qed.

Lemma mem_tally_seq (T : eqType) (l : seq (T * nat)) :
  l \is a wf_tally _ -> tally_seq l =i unzip1 l.
Proof.
  rewrite /eq_mem=> H x.
  by rewrite -{2}(tally_seqK H) (perm_mem (tallyEl _)) mem_undup.
Qed.

Lemma prime_decomp2_i (n : nat) :
  prime_decomp2 n =i primes n.
Proof.
  rewrite /prime_decomp2 /primes /eq_mem=> x.
  by rewrite mem_tally_seq; last by apply prime_decomp_wf_tally.
Qed.

From mathcomp Require Import bigop.

Lemma prod_prime_decomp2 n :
  n > 0 -> n = \prod_(p <- prime_decomp2 n) p.
Proof.
  move=>ngt0; rewrite /prime_decomp2 /tally_seq.
  rewrite big_flatten /= big_map.
  erewrite eq_bigr; first by apply prod_prime_decomp.
  move=>[p e] _ /=.
  by rewrite big_nseq iter_muln_1.
Qed.

Lemma mem_prime_decomp2 n p :
  p \in prime_decomp2 n -> prime p.
Proof. by rewrite prime_decomp2_i mem_primes => /andP []. Qed.

(* Lemma muln_ind (P : nat -> Type) :
  P 0%N -> P 1%N ->
  (forall n p, prime p -> P n -> P (p * n)%N) ->
    forall n, P n.
Proof.
  move=> P0 P1 Pind n.
  case Hngt0 : (n > 0);
    last by move: Hngt0 => /negbT; rewrite -eqn0Ngt; move=> /eqP ->; exact: P0.
  move Hd : (prime_decomp2 n) => d; move: n Hngt0 Hd.
  induction d => n Hngt0 Hn; have := (prod_prime_decomp2 Hngt0);
    rewrite Hn (big_nil, big_cons); first by move=>->.
  set (k := \prod_(j <- d) j).
  have Hkgt0 : (0 < k).
    apply prodn_cond_gt0. admit.
  specialize (IHd k Hkgt0).
  move=> ->; apply Pind.
  apply: (@mem_prime_decomp2 n); rewrite Hn; apply: mem_head.
  apply IHd.
  subst k.
  admit.
Admitted. *)



Local Open Scope ring_scope.

Import GRing.Theory Num.Theory.
(* Import Refinements.Op. *)

Definition edivz (m d : int) := (m %/ d, m %% d)%Z. (* todo use edivn? *)

Definition pdivz (m d : int) := (* todo use edivn? *)
  if (d %| m)%Z then Some (m %/ d)%Z else None.

Lemma pdivzP : forall a b, div_spec a b (pdivz a b).
Proof.
  intros; unfold pdivz; case /boolP : (b %| a)%Z => H; constructor.
  - by rewrite divzK.
  - move=> x; move: H; apply: contra=> H.
    apply /dvdzP. eexists. apply/eqP. eassumption. (* idiomatize *)
Qed.

HB.instance Definition _ := Ring_hasDiv.Build int (div := pdivz) pdivzP.

Lemma pdiv_gcd (d a b : int) : (d %| gcdz a b) = (d %| a) && (d %| b).
Proof.
  unfold dvdr; cbn; unfold pdivz.
  case /boolP : (d %| gcdz a b)%Z.
  - rewrite dvdz_gcd; by move=> /andP [-> ->].
  - case Ha : (d %| a)%Z; case Hb : (d %| b)%Z => //= /negP [].
    by rewrite dvdz_gcd Ha Hb.
Qed.

HB.instance Definition _ := DvdRing_hasGcd.Build int pdiv_gcd.

Lemma int_egcdzP a b : bezout_spec a b (egcdz a b).
Proof.
  case: (egcdzP a b)=> x y H _; constructor.
  by apply eq_eqd.
Qed.

HB.instance Definition _ := GcdDomain_hasBezout.Build int int_egcdzP.

(* Lemma mul1_unique : unique (fun x => forall a, x * a = a) (1 : int).
constructor. (* ought to exist already *)
- apply mul1r.
- intros x H. specialize (H 1). revert H. (* ? *)
  move=> /eqP. rewrite intUnitRing.mulzn_eq1. by move=> /andP [/eqP H _].
Qed. *)

Lemma mulfI' (R : idomainType) (a b x : R) :
  (x != 0) -> (a * x == b * x) = (a == b).
Proof.
  intros.
  apply: (sameP (iffP idP _ _) eqP); last by move=> ->.
  move=>/eqP H'. apply: (mulIf H). done.
Qed.

Lemma mulf_id_iff_1 (R : idomainType) (a b : R) : a != 0 -> (a == b * a) = (1 == b).
Proof.
  move=> H.
  rewrite -{1}[a]mul1r.
  by rewrite mulfI'.
Qed.

Lemma dvdrz (a b : int) : a %| b = (a %| b)%Z.
Proof.
  rewrite /dvdr /odivr /div /= /pdivz.
  by case (a %| b)%Z.
Qed.

Lemma dvdrzn (a b : nat) : (Posz a %| Posz b) = (a %| b)%N.
Proof. by rewrite dvdrz. Qed.

Lemma mulzz_eq1 (m n : int) : (m * n == 1) =
  (m == 1) && (n == 1) || (m == -1) && (n == -1).
Proof.
  case: m; case: n => m n /=;
    rewrite ?orbF ?NegzE ?mulrNN ?mulrN ?mulNr ?eqr_opp ?eqr_oppLR -PoszM.
  all: try by rewrite ?[_ == _]muln_eq1.
  all: try by rewrite ?andbF.
Qed.

Lemma int_eqd_eq (a b : int) : (a %= b) = (`|a| == `|b|).
Proof.
  case: (a =P 0)=> [-> | /(negPP eqP) a_neq0].
    by rewrite normr0 [0 == _]eq_sym normr_eq0 eqd0r.
  apply: (sameP (iffP idP _ _) eqP).
  2:{ rewrite /eqd; cbn. rewrite !dvdrz !dvdzE => [=->]. by rewrite dvdnn. }
  move=> /andP [/dvdrP [x Hx] /dvdrP [y Hy]]; move: Hy.
  rewrite {1}Hx mulrA.
  move=> /eqP; rewrite mulf_id_iff_1 //{a_neq0} eq_sym mulzz_eq1.
  by move=> /orP [/andP [_ /eqP ?] | /andP [_ /eqP ?]]; subst;
    rewrite ?mul1r ?mulN1r ?normrN.
Qed.

Lemma int_eqd_eq' (a b : int) : a %= b -> (`|a| = `|b|)%N.
Proof.
  move=> /andP [].
  rewrite /dvdr /odivr /div /= /pdivz;
    case E: (a %| b)%Z=> // _; case E': (b %| a)%Z=> // _;
    move: E E'; rewrite /dvdz /= !unfold_in.
  move: `|a|%N `|b|%N=> /= {}a {}b.
  move=> E E'. apply/eqP. by rewrite eqn_dvd E E'.
Qed.

Lemma sdvdr1 (R : dvdRingType) (a : R) : ~~ (a %<| 1).
Proof. by rewrite /sdvdr dvd1r /= andbF. Qed.

Lemma sdvdzN (a b : int) : (a %<| - b) = (a %<| b).
Proof. by rewrite /sdvdr !dvdrz !dvdzE abszN. Qed.

Lemma sdvdNz (a b : int) : (- a %<| b) = (a %<| b).
Proof. by rewrite /sdvdr !dvdrz !dvdzE abszN. Qed.

Lemma sdvdz_acc_1 : Acc (sdvdr (R:=int)) 1.
Proof. constructor=> x H. by have := sdvdr1 x => /negP []. Qed.

Lemma sdvdz_acc_n1 : Acc (sdvdr (R:=int)) (-1).
Proof. constructor=> x. rewrite sdvdzN=> H. by have := sdvdr1 x => /negP []. Qed.

(* Lemma ding4 a : Acc (sdvdr (R:=int)) a -> Acc (sdvdr (R:=int)) (- a).
Proof. move=> [H]; constructor=> y; rewrite sdvdzN; apply H. Qed. *)

Lemma sdvdz_acc_prime p : prime p -> Acc (sdvdr (R:=int)) p.
Proof.
  move=> /primeP [_ H].
  constructor=> x /andP [Hx /negP Hp].
  case: x Hx Hp H=> n;
    rewrite ?NegzE ?dvdNr ?dvdrN dvdrzn => Hx Hp /(_ _ Hx) /orP [/eqP -> | /eqP H].
  by apply sdvdz_acc_1.
  by subst; case: Hp.
  by apply sdvdz_acc_n1.
  by subst; case: Hp.
Qed.

Definition sdvdn (x y : nat) := (x %| y)%N && ~~ (y %| x)%N.
Local Infix "%<|" := sdvdn : nat_scope.

Lemma sdvdnP' (d n : nat) : (0 < n)%N -> (d %<| n)%N = ((d \in divisors n) && (d < n))%N.
Proof.
  move=> ngt0; rewrite -dvdn_divisors; last by done.
  apply: andb_id2l => Hd; have Hle: (d <= n)%N; first by apply dvdn_leq.
  by rewrite ltn_neqAle Hle andbT eqn_dvd Hd.
Qed.

Lemma sdvdz_n (d n : int) : (`|d| %<| `|n|)%N = (d %<| n).
Proof. by rewrite /sdvdn /sdvdr !dvdrz !dvdzE. Qed.

Lemma sdvdnP (d n : nat) : reflect
  ((d != 0%N) /\ (n == 0%N) \/ (d \in divisors n) /\ (d < n)%N)
  (d %<| n)%N.
Proof.
  case: n => [|n] /=. rewrite /sdvdn eqxx dvd0n dvdn0 => /=.
    case: d => /=; constructor; [rewrite ltnn; by move=> [[]|[]] | eauto]. (* so much work *)
  rewrite sdvdnP'; last by done.
  apply/(iffP idP). by move=>/andP; right. (* ugly *)
  move=> [[_ /notF []]|H]. by apply/andP.
Qed.

Lemma sdvdzP (d n : int) : reflect
  (d != 0 /\ n == 0 \/ `|d|%N \in divisors `|n| /\ (`|d| < `|n|)%N)
  (d %<| n).
Proof. rewrite -sdvdz_n -[d==0]normr_eq0 -[n==0]normr_eq0; apply sdvdnP. Qed.  

(* Lemma sdvdn_0 (b : nat) : ~~ (0 %<| b)%N.
Proof. by case: b. Qed. *)

Lemma divisors_notin0 n : 0%N \notin divisors n.
Proof. case: n=> [|n] //; by rewrite -dvdn_divisors. Qed.

(* Lemma sdvdn_ne0_wf i : i != 0%N -> Acc sdvdn i.
Proof.
  have H: forall (m n : nat), n != 0%N -> (n < m)%N -> Acc sdvdn n.
  {
    induction m=> n Hne0 Hlt; first by done.
    constructor=> k /sdvdnP [[Hne0' /eqP He0]|[Hd Hlt']]. by subst.
    apply IHm.
      by have := (divisors_notin0 n)=> /memPn /(_ k Hd).
      apply: leq_trans. apply: Hlt'. apply: Hlt.
  }
  move=> Hi; by apply: (H i.+1).
Qed.

Lemma sdvdn_0_wf : Acc sdvdn 0%N.
Proof.
  constructor=> x /sdvdnP [[H _]|[_ H]]; last by [].
  by apply sdvdn_ne0_wf.
Qed.

Lemma sdvdn_wf : well_founded sdvdn.
Proof. by case=>[|n]; [apply: sdvdn_0_wf | apply: sdvdn_ne0_wf]. Qed. *)

Lemma sdvdz_acc_ne0 (i : int) : i != 0 -> Acc (sdvdr (R:=int)) i.
Proof.
  have H: (forall (m : nat) (a : int), a != 0 -> `|a| < m -> Acc (sdvdr (R:=int)) a).
  {
    induction m=> n Hne0 Hlt; first by done.
    constructor=> k /sdvdzP [[Hne0' /eqP He0]|[Hd Hlt']]. by subst.
    apply IHm.
      by have := (divisors_notin0 `|n|)=> /memPn /(_ _ Hd); rewrite absz_eq0.
      apply: leq_trans. apply: Hlt'. apply: Hlt.
  }
  move=> Hn. case: i Hn=> n Hn;
    (apply: (H n.+2); first by done);
    rewrite /Num.Def.normr /Order.lt //=. (* ??? *)
Qed.

Lemma sdvdz_acc_0 : Acc (sdvdr (R:=int)) 0.
Proof.
  constructor=> x /sdvdzP [[H _]|[_ H]]; last by [].
  by apply sdvdz_acc_ne0.
Qed.

Lemma sdvdz_wf : well_founded (sdvdr (R:=int)).
Proof.
  move=> n. case: (n == 0) /eqP => [->|/eqP H];
    [apply: sdvdz_acc_0 | by apply: sdvdz_acc_ne0].
Qed.

HB.instance Definition _ := DvdRing_isWellFounded.Build int sdvdz_wf.

Lemma le_abs_pemull a b : a != 0 -> (`|b| <= `|(a * b)%R|)%N. (* todo: name *)
Proof.
  intros. rewrite -lez_nat abszM; apply leq_pmull.
  by rewrite absz_gt0.
Qed.

Lemma edivzP a b : edivr_spec absz a b (edivz a b).
Proof.
  constructor.
  - apply divz_eq.
  - apply/implyP => H.
    rewrite -lez_nat intS lez1D gez0_abs; last by apply modz_ge0.
    by apply ltz_mod.
Qed.

HB.instance Definition _ := Ring_isEuclidean.Build int le_abs_pemull edivzP.


(*** Additional facts ***)
Lemma divz_sgz d : (d %/ `|d|%N)%Z = sgz d.
Proof.
  case: (intP d)=> //= n; rewrite /sgz /=.
  - by rewrite divzz.
  - by rewrite divNz_nat // divn_small.
Qed.

Lemma egcdrz (a b : int) :
  a != 0 ->
  let g := gcdz a b in
  let (u, v) := egcdz a b in
  let a1 := (a %/ g)%Z in
  let b1 := (b %/ g)%Z in
  (g, u, v, a1, b1) = egcdr a b.
Proof.
  move=> a_neq0.
  case E: (egcdz a b)=> [u v] /=.
  rewrite /egcdr /bezout /= E.
  have ->: u * a + v * b = gcdz a b.
    move: E. by case: egcdzP=> _ _ <- _ [-> ->].
  rewrite gcdz_eq0 (negbTE a_neq0) /=.
  repeat f_equal.
  all: by rewrite /odivr /div /= /pdivz (dvdz_gcdl, dvdz_gcdr).
Qed.
