
(* let rec intersperse sep ls =
  match ls with
  | [] | [_] -> ls
  | hd::tl -> hd :: sep :: intersperse sep tl *)

let list_show show xs = match xs with | [] -> "[]" | _ ->
  "[ " ^ (String.concat "; " (List.map show xs)) ^ " ]"

let hmlgy_to_string hmlgy = list_show (fun p -> "(free: " ^ (string_of_int (fst p)) ^ ", torsion: " ^ (list_show Big_int_Z.string_of_big_int (snd p)) ^ ")") hmlgy

(* let () = print_endline @@ list_show (list_show Big_int_Z.string_of_big_int) (Extract.Torus_homology.torus_boundary0 ())

let () = print_endline @@ list_show (list_show Big_int_Z.string_of_big_int) (Extract.Torus_homology.torus_boundary1 ())

let () = print_endline @@ list_show (list_show Big_int_Z.string_of_big_int) (Extract.Torus_homology.torus_boundary2 ()) *)

let () = print_endline (hmlgy_to_string (Extract.Torus_homology.torus_homology ()))
