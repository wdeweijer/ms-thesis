From stdpp Require Import prelude options sorting.

(*** Notes ***)

(* Are these two the same? *)
(* Check fin_S_inv.
Check Fin.caseS'. *)

(* Why is fin_maps.map_included only for (R : relation _) and not heterogeneous relations? *)

(* TODO: Check whether stdpp 1.10.0 and 1.11.0 add any lemmas that we proved here *)

#[export] Set Implicit Arguments.
#[export] Unset Strict Implicit.
#[export] Unset Printing Implicit Defensive.

(* TODO: Make sure results are not dependent on value of inhabitant *)
(* Old comment: for us, 0 and not 1 is the inhabitant of Z *)
#[export] Instance Z_inhabited : Inhabited Z := populate 0%Z.

(* Phantom type used for typeclass resolution *)
(* TODO: Coq puts this in Prop, is that a problem? *)
Inductive phantom (T : Type) (t : T) : Type :=
	Phantom.

(*** Various lemmas. ***)
(* ziplist applicative *)
Definition zip_apply A B (fs : list (A → B)) (xs : list A) : list B :=
	zip_with ($) fs xs.

Lemma tail_length A (l : list A) : length (tail l) = pred (length l).
Proof. by destruct l. Qed.

Lemma nth_lookup_total A `{Inhabited A} (l : list A) i :
	nth i l inhabitant = l !!! i.
Proof. induction l in i |- *; destruct i; simpl; done. Qed.

Lemma list_omap_filter A B (f : A → option B) (g : B → A) :
	(∀ a, from_option g a (f a) = a) →
	∀ (l : list A), g <$> omap f l = filter (is_Some ∘ f) l.
Proof.
	intros Cancel l.
	induction l as [| a l IHl]; [done|].
	specialize (Cancel a).
	unfold filter, list_filter.
	remember (filter (is_Some ∘ f) l) as l' eqn:Hl'; rewrite <- IHl; clear IHl Hl' l'.
	simpl. destruct (f a) eqn:H; simpl; [|done].
	eauto with f_equal.
Qed.

Lemma list_total_eq A `{Inhabited A} (l1 l2 : list A) :
	length l1 = length l2 →
	(∀ i, l1 !!! i = l2 !!! i) →
	l1 = l2.
Proof.
	intros Hl Hi. eapply list_eq_same_length; [done..|].
	intros. ospecialize* Hi.
	by erewrite 2 list_lookup_total_correct in Hi.
Qed.

Lemma list_lookup_total_ge {A} `{Inhabited A} (l : list A) i :
	length l ≤ i →
	l !!! i = inhabitant.
Proof. intros Hl. by rewrite list_lookup_total_alt, lookup_ge_None_2. Qed.

Lemma filter_Forall A (P : A → Prop) l {PDec : ∀ x, Decision (P x)} :
	Forall P l → filter P l = l.
Proof.
	intros H.
	induction l; [done|]; inv H.
	rewrite filter_cons_True; eauto using f_equal.
Qed.

(* todo: name? maybe omap_lookup *)
Lemma omap_Forall A B `{Inhabited A} (f : A → option B) (l : list A) :
	Forall is_Some (f <$> l) → ∀ i, omap f l !! i = (l !! i) ≫= f.
Proof.
	induction l as [|?? IHl]; [done|]; simpl.
	intros [[??]?]%Forall_cons_1 i.
	case_match; [|done]; simplify_eq.
	destruct i; [done|]; simpl.
	by apply IHl.
Qed.

(* LeftID is not heterogenous, so we cant use it *)
Lemma foldr_left_unit_filter A B `{EqDecision A}
	(l : list A) (x : B) (f : A → B → B) (unit : A) :
	(∀ b, f unit b = b) → foldr f x l = foldr f x (filter (λ a, a <> unit) l).
Proof.
	intros H.
	induction l; [done|]; cbn.
	case_decide; simpl.
	- by f_equal.
	- subst; by rewrite H.
Qed.

Lemma filter_Forall_not A (P : A → Prop) `{PDec : ∀ a, Decision (P a)} (l : list A) :
	(Forall (λ a, ¬ (P a)) l) → filter P l = [].
Proof.
	intros H.
	induction l; [apply filter_nil|].
	apply Forall_cons in H as [nPa H].
	rewrite filter_cons_False; auto.
Qed.

Lemma elem_of_drop A (x : A) n (l : list A) :
	x ∈ drop n l ↔ (∃ i, l !! i = Some x ∧ n ≤ i).
Proof.
	rewrite elem_of_list_lookup.
	split.
	- intros [i H]. exists (n + i). rewrite <- lookup_drop. auto with arith.
	- intros [i [H le]]. exists (i - n).
		rewrite lookup_drop, <- H. f_equal; auto with arith.
Qed.

Lemma zip_with_fmap_same A B C D (l : list A) (g : A → B) (h : A → C) (f : B → C → D) :
	zip_with f (g <$> l) (h <$> l) = (λ x, f (g x) (h x)) <$> l.
Proof. by rewrite zip_with_fmap_r, zip_with_fmap_l, zip_with_diag. Qed.

Lemma filter_fmap A B (l : list A) (f : A → B) (P : B → Prop) `{∀ x : B, Decision (P x)} :
	filter P (f <$> l) = f <$> filter (P ∘ f) l.
Proof.
	induction l as [|?? IHl]; simpl.
	- apply filter_nil.
	- rewrite 2 filter_cons.
		unfold "∘" in *. destruct (decide _).
		+ by rewrite fmap_cons, <- IHl.
		+ apply IHl.
Qed.

Lemma NoDup_StronglySorted A (l : list A) : NoDup l ↔ StronglySorted (≠) l.
Proof.
	split; intros H;
		induction H; constructor; try done;
		rewrite Forall_Exists_neg, Exists_exists in *; naive_solver.
Qed.

Lemma list_subseteq_Forall A (P : A → Prop) (l l' : list A) :
	 l ⊆ l' → Forall P l' → Forall P l.
Proof. rewrite 2 Forall_forall. intros H H' ??. by apply H', H. Qed.

Lemma option_guard_False_2 P `{Decision P} : guard P = None → ¬ P.
Proof. by case_guard. Qed.

Lemma option_guard_False' P `{Decision P} : guard P = None ↔ ¬ P.
Proof. split; [apply option_guard_False_2 |apply option_guard_False]. Qed.

Lemma rev_seq j n : rev (seq j n) = (λ i, j + (n - i)) <$> seq 1 n.
	induction n in j |- *; [done|].
	simpl rev.
	rewrite IHn.
	rewrite seq_S.
	rewrite fmap_app.
	f_equal.
	2:{ simpl; f_equal; lia. }
	apply list_fmap_ext; intros ?? []%lookup_seq; lia.
Qed.

Lemma zip_fmap A B C D (f : A → B) (g : C → D) l k :
	zip (f <$> l) (g <$> k) = (prod_map f g) <$> zip l k.
Proof.
	induction l in k |- *; [done|]; destruct k; [done|].
	csimpl. by f_equal.
Qed.

Lemma zip_fmap_l A B C (f : A → B) l (k : list C) :
	zip (f <$> l) k = (prod_map f id) <$> zip l k.
Proof.
	replace k with (id <$> k) at 1 by apply list_fmap_id.
	apply zip_fmap.
Qed.

Lemma zip_fmap_r A B C (f : B → C) (l : list A) k :
	zip l (f <$> k) = (prod_map id f) <$> zip l k.
Proof.
	replace l with (id <$> l) at 1 by apply list_fmap_id.
	apply zip_fmap.
Qed.

(* Better version of Forall_impl *)
Lemma Forall_weaken A (P : A → Prop) (l : list A) (Q : A → Prop) :
	Forall P l → (∀ x, x ∈ l → P x → Q x) → Forall Q l.
Proof. rewrite 2 Forall_forall. auto. Qed.

Lemma Forall_weaken_Forall A (P : A → Prop) (l : list A) (Q : A → Prop) :
	Forall P l → Forall (λ x, P x → Q x) l → Forall Q l.
Proof. rewrite !Forall_forall. auto. Qed.

Lemma imap_zip_with_seq A B (l : list A) (f : nat → A → B) :
	imap f l = zip_with f (seq 0 (length l)) l.
Proof.
	induction l in f |- *; cbn; [done|]; f_equal.
	rewrite <- fmap_S_seq, zip_with_fmap_l.
	by rewrite <- IHl.
Qed.

Lemma reverse_nil_inv A (l : list A) : reverse l = [] → l = [].
Proof.
	destruct l; [done|]; simpl; rewrite reverse_cons; intros H; exfalso.
	by eapply (app_cons_not_nil _ _ a).
Qed.

Lemma last_seq j n : last (seq j (S n)) = Some (j + n).
Proof.
	rewrite last_lookup, length_seq; simpl pred.
	rewrite lookup_seq. eauto.
Qed.

(* Check fold_left_rev_right. *)
Lemma reverse_foldr_foldl A B l x (f : B → A → A) :
	foldr f x (reverse l) = foldl (flip f) x l.
Proof.
	induction l in x |- *; simpl; [done|].
	rewrite reverse_cons, foldr_app. by rewrite IHl.
Qed.

(*** Instances and Lemmas for Z and sums of list Z ***)
Global Instance Zadd_assoc : Assoc eq Z.add.
Proof. intros ?*; eapply Z.add_assoc. Qed.
Global Instance Zadd_comm : Comm eq Z.add.
Proof. intros ?*; eapply Z.add_comm. Qed.
Global Instance Zmul_assoc : Assoc eq Z.mul.
Proof. intros ?*; eapply Z.mul_assoc. Qed.
Global Instance Zmul_comm : Comm eq Z.mul.
Proof. intros ?*; eapply Z.mul_comm. Qed.

Notation Zsum_list := (foldr Z.add 0%Z).

Section Zsum_list_lemmas.
	Open Scope Z_scope.
	Local Coercion Z.of_nat : nat >-> Z.
	Implicit Types x y z : Z.
	Implicit Types l k : list Z.
	Lemma Zsum_list_app l k : Zsum_list (l ++ k) = Zsum_list l + Zsum_list k.
	Proof. induction l; simpl; lia. Qed.
	Lemma Zsum_list_reverse l : Zsum_list (reverse l) = Zsum_list l.
	Proof. induction l; simpl; rewrite ?reverse_cons, ?Zsum_list_app; simpl; lia. Qed.
	Lemma Zsum_list_replicate n m : Zsum_list (replicate m n) = m * n.
	Proof. induction m; simpl; lia. Qed.
	Lemma Zsum_list_fmap_same n l f :
		Forall (λ x, f x = n) l →
		Zsum_list (f <$> l) = length l * n.
	Proof. induction 1; csimpl; lia. Qed.
	Lemma Zsum_list_fmap_const l n : Zsum_list ((λ _, n) <$> l) = length l * n.
	Proof. erewrite const_fmap by done. apply Zsum_list_replicate. Qed.
	Lemma Zsum_list_filter_0 l : Zsum_list l = Zsum_list (filter (λ a, a <> 0%Z) l).
	Proof. by apply foldr_left_unit_filter. Qed.
	Lemma Zsum_list_permute l1 l2 : l1 ≡ₚ l2 → Zsum_list l1 = Zsum_list l2.
	Proof. apply foldr_permutation_proper'; tc_solve. Qed.
	Lemma Zsum_list_scale x l : Zsum_list (Z.mul x <$> l) = x * Zsum_list l.
	Proof. induction l as [|?? IHl]; cbn; [|rewrite IHl]; lia. Qed.
End Zsum_list_lemmas.

Definition divides (a b : Z) : bool := (b mod a =? 0)%Z.
Infix "%|" := divides (at level 30): Z_scope.


(*** Extended GCD on Z ***)

(* TODO: Test speedup compared to our implementation *)
Definition Zgcd_ex (a b : Z) : Z * (Z * Z) :=
	let '(u, v, d) := Znumtheory.extgcd a b in (d, (u, v)).

Lemma app_head A (l k : list A) : head (l ++ k) =
	match head l with
	| Some x => Some x
	| None => head k
	end.
Proof. by destruct l. Qed.

Lemma fmap_head A B (f : A → B) (l : list A) : head (f <$> l) = f <$> (head l).
Proof. by destruct l. Qed.

Lemma Sorted_snoc2 A (R : relation A) l x : Sorted R (l ++ [x]) → Sorted R l ∧ TlRel R x l.
	induction l; cbn.
	1: intros _; repeat constructor.
	inv 1. specialize (IHl H2) as [].
	split. { constructor. 1:done. destruct l; constructor. by inv H3. }
	inv H0; cbn in *. { inv H3. apply (TlRel_cons _ _ _ [] H1). }
	rewrite app_comm_cons. by constructor.
Qed.

Lemma Sorted_snoc' A (R : relation A) l x : Sorted R (l ++ [x]) ↔ Sorted R l ∧ TlRel R x l.
Proof. split. 1:apply Sorted_snoc2. intros []; by apply Sorted_snoc. Qed.

Lemma Sorted_middle A (R : relation A) l x k : Sorted R (l ++ [x]) → Sorted R (x :: k) →
	Sorted R ((l ++ [x]) ++ k).
Proof.
	intros []%Sorted_snoc2 ?.
	induction l.
	{ done. }
	inv H.
	cbn. constructor.
	- apply IHl; [done|].
		destruct l using rev_ind; constructor.
		inv H0; rewrite app_comm_cons in H; apply app_inj_tail in H as []; simplify_eq; done.
	- destruct l; simpl.
		2:{ constructor. by inv H5. }
		constructor.
		inv H0. rewrite <- app_nil_l in H. apply app_inj_tail in H as []; simplify_eq; done.
Qed.

Lemma Sorted_middle' A (R : relation A) l k x : Sorted R l → TlRel R x l → HdRel R x k → Sorted R k →
	Sorted R (l ++ [x] ++ k).
Proof.
	intros ????.
	rewrite app_assoc. apply Sorted_middle.
	- by apply Sorted_snoc.
	- by apply Sorted_cons.
Qed.

Lemma Sorted_app A (R : relation A) l k : Sorted R l → Sorted R k →
	match last l with
	| None => True
	| Some x => HdRel R x k
	end →
		Sorted R (l ++ k).
Proof.
	case_match; simplify_eq.
	2:{ apply last_None in H as ->. done. }
	apply last_Some in H as [l' ->].
	intros ???. apply Sorted_middle. 1:done.
	by constructor.
Qed.


(*** Sublists ***)
(* A list of all sublists of a given list.
	Should probably really be some sort of set. *)
Fixpoint sublists {A} (l : list A) : list (list A) :=
	match l with
	| [] => [[]]
	| x :: ls => (cons x <$> sublists ls) ++ sublists ls
	end.

(* Does this order have a name? *)
Definition my_list_lexico `{Lexico A} : Lexico (list A) :=
	fix go l1 l2 :=
	let _ : Lexico (list A) := @go in
	match l1, l2 with
	| _ :: _, [] => True (* cf. list_lexico *)
	| x1 :: l1, x2 :: l2 => lexico (x1,l1) (x2,l2)
	| _, _ => False
	end.

Lemma sublists_last A (l : list A) : last (sublists l) = Some [].
Proof.
	induction l; cbn; [done|].
	by rewrite last_app, IHl.
Qed.

Lemma sublists_head A (l : list A) : head (sublists l) = Some l.
Proof.
	induction l; cbn; [done|].
	rewrite app_head; case_match.
	{ rewrite fmap_head, IHl in H. by simplify_eq. }
	exfalso. apply head_None in H. apply head_Some in IHl as []; by rewrite H0 in H.
Qed.

Lemma sublists_Sorted_lexico A `{Lexico A} (l : list A) : Sorted lexico l → (Sorted (lexico (Lexico:=my_list_lexico)) (sublists l)).
Proof.
	induction l.
	{ intros _. eauto using Sorted, HdRel. }
	inv 1. specialize (IHl H3). cbn.
	apply Sorted_app.
	- eapply Sorted_fmap; [|done].
		cbn; unfold lexico, prod_lexico; eauto.
	- done.
	- rewrite fmap_last, sublists_last; cbn.
		opose proof (sublists_head _); apply head_Some in H0 as [sl' ->].
		constructor.
		inv H4. 1:done.
		cbv in H0; cbv. by left.
Qed.

Lemma sublist_sublist_1 A (l : list A) :
	Forall (λ sl, sl `sublist_of` l) (sublists l).
Proof.
	induction l as [|?? IHl]; simpl; eauto.
	apply Forall_app; split; [apply Forall_fmap; unfold "∘" |];
		apply Forall_forall; intros x H;
		rewrite Forall_forall in IHl;
		constructor; eauto.
Qed.

Lemma sublist_sublists_2 A (sl l : list A) :
	sl `sublist_of` l → sl ∈ sublists l.
Proof.
	induction 1; simpl.
	- constructor.
	- apply elem_of_app; left; eapply elem_of_list_fmap; eauto.
	- apply elem_of_app; right; done.
Qed.

Lemma sublist_sublists A (sl l : list A) :
	sl `sublist_of` l ↔ sl ∈ sublists l.
Proof.
	split.
	- apply sublist_sublists_2.
	- revert sl; apply Forall_forall, sublist_sublist_1.
Qed.

Lemma sublists_length A (l : list A) : length (sublists l) = 2^(length l).
Proof.
	induction l as [|?? IHl]; simpl; [done|].
	by rewrite length_app, length_fmap, Nat.add_0_r, IHl.
Qed.

Lemma elem_of_sublist A (l l' : list A) (a : A) : l `sublist_of` l' → a ∈ l → a ∈ l'.
Proof.
	intros Hsub Helem.
	induction Hsub.
	- done.
	- apply elem_of_cons in Helem as [].
		+ subst. constructor.
		+ constructor. tauto.
	- constructor. tauto.
Qed.

Lemma sublist_subseteq A (l l' : list A) : l `sublist_of` l' → l ⊆ l'.
Proof. intros Hsub a. by apply elem_of_sublist. Qed.

Lemma sublist_Forall A (P : A → Prop) (l l' : list A) :
	l `sublist_of` l' → Forall P l' → Forall P l.
Proof. intros ?%sublist_subseteq. by apply list_subseteq_Forall. Qed.

Lemma sublist_sorted A (R : relation A) (l l' : list A) :
	l `sublist_of` l' → StronglySorted R l' → StronglySorted R l.
Proof.
	intros Hsub Hsort.
	induction Hsub; [done|..];
		apply StronglySorted_inv in Hsort as [];
		eauto using sublist_Forall, StronglySorted.
Qed.

Lemma sublists_NoDup A (l : list A) : NoDup l → NoDup (sublists l).
Proof.
	induction 1; simpl.
	- apply NoDup_singleton.
	- apply NoDup_app; repeat split.
		+ apply NoDup_fmap; [tc_solve | done].
		+ intros l' Hlx' Hl'.
			eapply sublist_sublists, elem_of_sublist in Hl'; [done|].
			refine (proj1 (Forall_forall _ _) _ _ Hlx').
			apply Forall_fmap, Forall_forall. constructor.
		+ done.
Qed.

Lemma dec_iff {P Q} : (P ↔ Q) → Decision P → Decision Q.
Proof. intros ? []; left + right; tauto. Defined.

Instance : CMorphisms.Proper
	(CMorphisms.respectful iff CRelationClasses.iffT)
	Decision.
Proof. split; by eapply dec_iff. Defined.

Instance : CMorphisms.Proper
	(CMorphisms.respectful iff (flip CRelationClasses.arrow))
	Decision.
Proof. intros ????. by eapply dec_iff. Defined.

Lemma sublist_dec_slow A `{dec : EqDecision A} : RelDecision (@sublist A).
Proof. intros ??. rewrite sublist_sublists. apply elem_of_list_dec. Defined.

Lemma sublist_cons_l_weak A (a : A) (l k : list A) : a :: l `sublist_of` k → l `sublist_of` k.
Proof. (* easier way? *)
	intros H. induction k; inv H.
	{ by constructor. }
	constructor; tauto.
Qed.

Section sublistb.
Context A `{dec : EqDecision A}.
Implicit Types l : list A.

Fixpoint sublistb l' l : bool :=
	match l', l with
	| [], _ => true
	| _, [] => false
	| a :: ls', b :: ls => if decide (a = b) then sublistb ls' ls else sublistb l' ls
	end.

Lemma sublistb_complete l l' : sublistb l' l → l' `sublist_of` l.
Proof.
	induction l in l' |- *; destruct l'; simpl; try done; intros.
	{ apply sublist_nil_l. }
	case_match; subst; constructor; eauto.
Qed.

Lemma sublistb_correct l l' : l' `sublist_of` l → sublistb l' l.
Proof.
	induction l as [|?? IHl] in l' |- *; destruct l'; simpl; try done.
	{ inv 1. }
	inv 1.
	{ rewrite decide_True by done; naive_solver. }
	case_match; apply IHl; [|done].
	by eapply sublist_cons_l_weak.
Qed.

Lemma sublistb_spec l l' : (sublist l l') ↔ (sublistb l l').
Proof. split; [apply sublistb_correct | apply sublistb_complete]. Qed.

Global Instance sublist_dec : RelDecision (@sublist A).
Proof using dec. intros ??. rewrite sublistb_spec. tc_solve. Defined.

End sublistb.

Lemma pred_finite_sublist A (l : list A) : pred_finite (flip sublist l).
Proof. eexists; intros; by apply sublist_sublists_2. Qed.

(* The list of sublists of length n *)
Fixpoint sublists_of_len {A} (n : nat) (l : list A) : list (list A) :=
	match n, l with
	| 0, _ => [[]]
	| S n, [] => []
	| S n, x :: l' => (cons x <$> sublists_of_len n l') ++ sublists_of_len (S n) l'
	end.

Lemma sublists_of_len_0 A (l : list A) : sublists_of_len 0 l = [[]].
Proof. by destruct l. Qed.

Lemma sublists_of_len_elem_length n A (l : list A) :
	Forall (λ sl, length sl = n) (sublists_of_len n l).
Proof.
	induction l as [|?? IHl] in n |- *; destruct n; simpl; eauto.
	setoid_rewrite Forall_forall in IHl.
	apply Forall_app; split; apply Forall_forall; intros x H.
	-	apply elem_of_list_fmap in H as (y & Hxy & Hy).
		simplify_list_eq; eauto.
	- eauto.
Qed.

Lemma sublist_sublists_of_len A (l : list A) n :
	Forall (λ sl, sl `sublist_of` l) (sublists_of_len n l).
Proof.
	induction l as [|?? IHl] in n |- *; destruct n; simpl; eauto.
	- constructor. { apply sublist_nil_l. }	constructor.
	- apply Forall_app; split; [apply Forall_fmap; unfold "∘" |];
		apply Forall_forall; intros x H;
		constructor;
		setoid_rewrite Forall_forall in IHl; eauto.
Qed.

Lemma sublists_of_len_sublists A (l : list A) n : sublists_of_len n l ⊆ sublists l.
Proof.
	intros l' H; apply sublist_sublists.
	pose proof (sublist_sublists_of_len l n) as L; rewrite Forall_forall in L; eauto.
Qed.

(* Lemma nat_nat_rect2_dyn (P : nat → nat → Type) :
	P 0 0 →
	(∀ m, P m 0 → P (S m) 0) →
	(∀ n, P 0 n → P 0 (S n)) →
	(∀ {m n}, P (S m) n → P m (S n) → P (S m) (S n)) →
	∀ m n, P m n.
Proof.
	intros H00 HS0 H0S HSS m n.
	assert (Hm0 : ∀ m, P m 0) by (intros k; induction k; eauto); clear HS0.
	assert (H0n : ∀ n, P 0 n) by (intros k; induction k; eauto); clear H0S.
	destruct m,n; [apply H00 | apply H0n | apply Hm0 |].
	clear H00.
	(* We want quadratic and not exponential behaviour *)
Abort.

Lemma list_nat_rect2 {A} (P : nat → list A → Type) :
	P 0 nil →
	(∀ n, P n nil → P (S n) nil) →
	(∀ a l, P 0 l → P 0 (a :: l)) →
	(∀ n a l, P (S n) l → P n (a :: l) → P (S n) (a :: l)) →
	∀ n l, P n l.
Abort. *)

(* Nest a function n times within itself *)
(* Definition nest {A} (f : A → A) (n : nat) (a : A) : A := Nat.iter n f a.
	(* match n with
	| 0 => a
	| S n => nest f n (f a)
	end. *)

Lemma nest_0 {A} f (a : A) : nest f 0 a = a.   Proof. done. Qed.
Lemma nest_1 {A} f (a : A) : nest f 1 a = f a. Proof. done. Qed.
Lemma nest_S_aux (n b : nat) : nest S n b = b + n.
Proof.
	induction n in b |- *; simpl; eauto.
	rewrite <- Nat.add_succ_comm; simpl; eauto.
Qed.
Lemma nest_S (n : nat) : nest S n 0 = n. Proof. apply nest_S_aux. Qed.

Lemma nest_assoc A f n (a : A) : f (nest f n a) = nest f n (f a).
Proof. unfold nest. symmetry. apply Nat.iter_swap. Qed. *)

(* [ f (f (f a)); f (f a); f a; a ] *)
Definition reverse_nest_list {A} (f : A → A) (n : nat) (a : A) : list A.
	induction n as [|? IHn].
	- exact [a].
	- destruct IHn as [|fa ls].
		+ exact []. (* impossible *)
		+ exact (f fa :: fa :: ls).
Defined.

(* [ a; f a; f (f a); ... ] *)
Definition nest_list {A} (f : A → A) (n : nat) (a : A) : list A :=
	reverse (reverse_nest_list f n a).

Lemma reverse_nest_list_length A f n (a : A) : length $ reverse_nest_list f n a = S n.
Proof.
	unfold reverse_nest_list.
	induction n; simpl; [done|].
	case_match; [done|].
	naive_solver.
Qed.

Lemma nest_list_length A f n (a : A) : length $ nest_list f n a = S n.
Proof.
	unfold nest_list; rewrite length_reverse; apply reverse_nest_list_length.
Qed.

Lemma reverse_nest_list_last A f n (a : A) : last $ reverse_nest_list f n a = Some a.
Proof. induction n; simpl; [done|]. case_match; simpl; eauto. Qed.

Lemma reverse_nest_list_lookup_last A f n (a : A) : (reverse_nest_list f n a) !! n = Some a.
Proof. induction n; simpl; [done|].	case_match; simpl; eauto. Qed.

Lemma reverse_nest_list_head A f n (a : A) : head (reverse_nest_list f n a) = Some $ Nat.iter n f a.
Proof.
	induction n; simpl; [done|].
	case_match; simpl in *; [done|].
	f_equal; simplify_eq.
	by rewrite <- Nat.iter_swap.
Qed.

Lemma reverse_nest_list_ne_nil A f n (a : A) : reverse_nest_list f n a ≠ [].
Proof. induction n; simpl; [done|]; by case_match. Qed.

Lemma reverse_nest_list_S A f n (a : A) :
	reverse_nest_list f (S n) a = (Nat.iter (S n) f a) :: (reverse_nest_list f n a).
Proof.
	simpl. case_match eqn:E; simpl. { by exfalso; eapply reverse_nest_list_ne_nil. }
	f_equal.
	apply (f_equal head) in E; simpl in E; rewrite reverse_nest_list_head in E; by injection E as <-.
Qed.

Lemma reverse_nest_list_lookup A f n (a : A) i : i ≤ n →
	reverse_nest_list f n a !! (n - i) = Some (Nat.iter i f a).
Proof.
	intros Hle. induction n as [|n IHn].
	{ apply Nat.le_0_r in Hle; by subst. }
	rewrite reverse_nest_list_S. assert (H : i ≤ n \/ i = S n) by lia; clear Hle; destruct H.
	- rewrite Nat.sub_succ_l by done; simpl; eauto.
	- subst. by rewrite Nat.sub_diag; simpl.
Qed.

Lemma nest_list_lookup {A} f n (a : A) i : i ≤ n →
	(nest_list f n a) !! i = Some $ Nat.iter i f a.
Proof.
	intros Hle.
	unfold nest_list;
		rewrite reverse_lookup by (rewrite reverse_nest_list_length; eauto with arith);
		rewrite reverse_nest_list_length; simpl.
	by apply reverse_nest_list_lookup.
Qed.

Definition inest_list {A} (f : nat → A → A) (n : nat) (a : A) : list A.
	apply reverse.
	induction n as [|n IHn].
	- exact [a].
	- destruct IHn as [|fa ls].
		+ exact []. (* impossible *)
		+ exact (f n fa :: fa :: ls).
Defined.

(* Compute inest_list (λ k _, k) 10 123. *)
(* = [123; 0; 1; 2; 3; 4; 5; 6; 7; 8; 9] *)

Lemma inest_list_length A f n (a : A) : length $ inest_list f n a = S n.
Proof.
	unfold inest_list; rewrite length_reverse.
	induction n; simpl; [done|].
	case_match; [done|].
	naive_solver.
Qed.

(* Duplicates the i'th element of l *)
Fixpoint duplicate {A} (i : nat) (l : list A) : list A :=
	match l with
	| []      => []
	| x :: l' => match i with
	             | 0    => x :: x :: l'
	             | S i' => x :: (duplicate i' l')
	             end
	end.

Section duplicate_properties.
Context {A : Type}.
Implicit Types a : A.
Implicit Types l : list A.

Lemma duplicate_cons a l i : duplicate (S i) (a :: l) = a :: duplicate i l.
Proof. reflexivity. Qed.

Lemma duplicate_length i l : i < length l → length (duplicate i l) = S (length l).
Proof.
	induction l in i |- *; simpl; intro Hlt; [lia|].
	destruct i; simpl; [done|].
	eauto with arith.
Qed.

Lemma duplicate_gt i l : length l ≤ i → duplicate i l = l.
Proof.
	induction l in i |- *; simpl; intro Hle; [by destruct i|].
	destruct i; simpl; [lia|].
	eauto with arith f_equal.
Qed.

Lemma sublist_duplicate i l : l `sublist_of` duplicate i l.
Proof.
	induction l in i |- *; simpl. { apply sublist_nil_l. }
	destruct i; simpl; by repeat constructor.
Qed.

Lemma duplicate_take_drop i l : duplicate i l = take (S i) l ++ drop i l.
Proof. induction l in i |- *; destruct i; try done; f_equal/=; auto. Qed.

Lemma duplicate_middle l1 l2 a : duplicate (length l1) (l1 ++ a :: l2) = l1 ++ a :: a :: l2.
Proof. induction l1; f_equal/=; auto. Qed.

Lemma lookup_duplicate_le l i j : j ≤ i → duplicate i l !! j = l !! j.
Proof.
	(* Time induction l in i, j |- *; destruct i, j; naive_solver eauto with lia. *)
	induction l in i, j |- *; destruct i; simpl; try done.
	{ by intros ->%Nat.le_0_r. }
	destruct j; simpl; [done|].
	intros; apply IHl; eauto with arith.
Qed.

Lemma lookup_duplicate_gt l i j : i < j → duplicate i l !! j = l !! (j - 1).
Proof.
	revert i j; induction l as [|?? IHl]; intros [|i] [|[|j]]; [(done || lia).. | simpl].
	intros Hlt. rewrite IHl by eauto with arith; simpl. by rewrite Nat.sub_0_r.
Qed.

Lemma duplicate_replicate a i n :
	i ≤ n → duplicate i (replicate (S n) a) = replicate (S (S n)) a.
Proof.
	revert i n. apply Nat.le_ind_rel; try tc_solve.
	1: done.
	intros ????; simpl; by f_equal.
Qed.

(* Wrong place, but cf duplicate_replicate *)
Lemma delete_replicate a i n :
	i ≤ n → delete i (replicate (S n) a) = replicate n a.
Proof.
	revert i n. apply Nat.le_ind_rel; try tc_solve.
	1: done.
	intros ????; simpl; by f_equal.
Qed.

End duplicate_properties.

Instance duplicate_inj A i : Inj (=) (=) (@duplicate A i).
Proof.
	intros l k H.
	apply list_eq; intros j.
	destruct (decide (i < j)).
	- apply (f_equal (.!! (S j))) in H.
		rewrite 2 lookup_duplicate_gt in H by lia.
		simpl in H; by rewrite Nat.sub_0_r in H.
	- apply (f_equal (.!! j)) in H.
		rewrite 2 lookup_duplicate_le in H by lia.
		done.
Qed.

Lemma delete_le A (l : list A) i : length l ≤ i → delete i l = l.
Proof.
	induction l in i |- *; simpl; [done|].
	intros H.
	destruct i; simpl; [lia|].
	f_equal. apply IHl. lia.
Qed.

Instance list_delete_surj A `{Inhabited A} i : Surj (=) (delete (Delete:=@list_delete A) i).
	intros l.
	destruct (decide (i < length l)).
	2:{ exists l. apply delete_le. lia. }
	exists (take i l ++ inhabitant :: drop i l).
	replace i with (length (take i l)) at 1.
	2:{ f_equal. rewrite length_take; lia. }
	by rewrite delete_middle, take_drop.
Qed.

(*** Simplicial equations ***)
Lemma delete_delete_le i j (leij : i ≤ j) A (l : list A) :
	delete i (delete (S j) l) = delete j (delete i l).
Proof.
	induction l as [|?? IHl] in i,j,leij |- *; destruct i, j; cbn; try done || lia.
	by rewrite IHl by lia.
	(* revert l.
	eapply Nat.le_ind_rel with (n:=i) (m:=j); [apply _| | |done]; intros; cbn.
	all: destruct l; try done.
	cbn. by rewrite H0. *)
Qed.

Lemma delete_delete_lt A (l : list A) i j : i < j →
	delete i (delete j l) = delete (j-1) (delete i l).
Proof.
	destruct j; [lia|]; cbn; rewrite Nat.sub_0_r.
	intros leij%le_S_n.
	by apply delete_delete_le.
Qed.

Lemma duplicate_duplicate_le A (l :list A) i j (leij : i ≤ j) :
	duplicate i (duplicate j l) = duplicate (S j) (duplicate i l).
Proof.
	induction l as [|?? IHl] in i,j,leij |- *; destruct i, j; cbn; try done || lia.
	by rewrite IHl by lia.
Qed.

Lemma delete_duplicate_id_1 A (l : list A) i : delete i (duplicate i l) = l.
Proof.
	induction i as [|? IHi] in l |- *; destruct l; cbn; try done.
	f_equal. apply IHi.
Qed.

Lemma delete_duplicate_id_2 A (l : list A) i : delete (S i) (duplicate i l) = l.
Proof.
	induction i as [|? IHi] in l |- *; destruct l; cbn; try done.
	f_equal. apply IHi.
Qed.

Lemma delete_duplicate_le A (l : list A) i j (leij : i ≤ j) :
	delete i (duplicate (S j) l) = duplicate j (delete i l).
Proof.
	induction l as [|?? IHl] in i,j,leij |- *;
		destruct i,j; cbn; try done || lia.
	f_equal. by rewrite <- IHl by lia.
Qed.

Lemma delete_duplicate_gt A (l : list A) i j (gtij : i > j) :
	delete (S i) (duplicate j l) = duplicate j (delete i l).
Proof.
	induction l as [|?? IHl] in i,j,gtij |- *;
		destruct i,j; cbn; try done || lia.
	f_equal. by rewrite <- IHl by lia.
Qed.


Definition sublists_degen_of_len {A} (n : nat) (l : list A) : list (list A).
	set (init1 := replicate (S $ length l) [[]] : list (list (list A))). (* S? *)
	unshelve eset (table := nest_list _ n init1).
	{ intros prev.
		unshelve eapply (inest_list _ (length l) []).
		intros k prev'.
		destruct (l !! (length l - k - 1)) as [lk|]; [|apply inhabitant (* imp? *)].
		destruct (prev !! (S k)) as [prevk|]; [|apply inhabitant (* imp? *)].
		exact ((cons lk <$> prevk) ++ prev').
	}
	destruct (last table) as [table'|]; [|exact [[]] (* imp? *)].
	destruct (last table') as [table''|]; [|exact [] (* imp? *)].
	exact table''.
Defined.

(* Time Compute length $ remove_dups $ merge_sort lt <$> ((λ l, l.*2) <$> list_power (seq 0 6) [1;2;3;4;5]). *)

Lemma sublists_degen_of_len_0 A (l : list A) : sublists_degen_of_len 0 l = [[]].
Proof.
	cbn. case_match eqn:H.
	2:{ rewrite last_None in H; discriminate. }
	rewrite last_cons in H. case_match eqn:H0; [|congruence].
	rewrite last_lookup, lookup_replicate in H0. destruct H0; congruence.
Qed.

(* Compute sublists [0;1;2;3].
Compute duplicate 3 [0;1;2;3].
About duplicate.
Compute foldr duplicate [0;1;2;3] <$> sublists [0;1;2;3].
Compute foldr duplicate [0;1;2;3] [0;0].
Compute foldr duplicate [0;1;2;3] [1;0].
Compute foldr duplicate [0;1;2;3] [2;1;0].

Compute foldr duplicate [0;1;2] <$> (reverse <$> sublists_of_len 3 [0;1;2;3;4]).
Compute sublists_degen_of_len 6 [0;1;2]. *)

(* Definition degen {A} (n : nat) (l : list A) : list (list A) :=
	foldr duplicate l <$> (reverse <$> sublists_of_len n (seq 0 (n + length l - 1))). *)

(* Compute degen 3 [0;1].

Lemma degen_length A n (l : list A) :
	Forall (λ s, length s = n + length l) (degen n l).
Proof.
	induction l in n |- *; cbn.
	- *)

Fixpoint iter_duplicate {A} (d : list bool) (s : list A) {struct d} : list A :=
	match d, s with
	| [], _ => s
	| _, [] => [] (* forbidden? *)
	| true :: d', a :: s' => a :: iter_duplicate d' s
	| false :: d', a :: s' => a :: iter_duplicate d' s'
	end.

(* Compute iter_duplicate [true; true] [0;1;2;3].

Compute iter_duplicate [true; true; false; false; false; true] [0;1;2;3].
Compute foldr duplicate [0;1;2;3] [5;1;0]. *)

(* l and p must have equal length! Encode this somehow? *)
Definition pick {A B} (P : B → Prop) `{∀ x, Decision (P x)} : list A → list B → list A :=
	fix go l p := match l,p with
	| _, [] => [] (* If p is too short, do we drop the rest of l or keep it? *)
	| [], _ => []
	| a :: l', b :: p' => if decide (P b) then a :: go l' p' else go l' p'
	end.
Arguments pick {A B} P {_} l p.

Lemma combine_zip A B (l : list A) (k : list B) :
	combine l k = zip l k.
Proof. reflexivity. Qed.

Lemma pick_filter {A B} (P : B → Prop) `{∀ x, Decision (P x)} (l : list A) (p : list B) :
	pick P l p = (filter (λ x, P x.2) (zip l p)).*1.
Proof.
	induction l in p |- *; destruct p; cbn; try done.
	case_match; simpl; eauto with f_equal.
Qed.


(* Inversion principles, double recursion, and proof irrelevance for Forall and LocallySorted.
	These are completely formulaic similar to Fin.case0 and Vector.case0 et al., is it possible
	to automatically derive these using something like elpi.derive? Or is there any easier way? *)

Definition Forall_inv_nil {A} {P : A → Prop} (Q : Forall P [] → Prop) (H : Q (List.Forall_nil _)) v : Q v :=
	match v with
	| List.Forall_nil _ => H
	| _ => fun devil => False_ind (@IDProp) devil
	end.

Definition Forall_inv_cons_dep {A} {P : A → Prop} (Q : ∀ {a} {l}, Forall P (a :: l) → Prop)
		(H : ∀ a l p q', Q (List.Forall_cons P a l p q'))
		{a l} (v : Forall P (a :: l)) : Q v :=
	match v with
	| List.Forall_cons _ a l p q' => H a l p q'
	|_ => fun devil => False_ind (@IDProp) devil
	end.

Definition Forall_inv_cons {A} {P : A → Prop} {a} {l} (v : Forall P (a :: l)) :
		∀ (Q : Forall P (a :: l) → Prop)
		(H : ∀ p q', Q (List.Forall_cons _ _ _ p q')), Q v :=
	match v with
	| List.Forall_cons _ a l p q' => λ Q H, H p q'
	| _ => fun devil => False_rect (@IDProp) devil
	end.

Definition Forall_rect2 A (P : A → Prop) (Q : ∀ {l}, Forall P l → Forall P l → Prop)
		(bas : Q (List.Forall_nil _) (List.Forall_nil _))
		(rect : ∀ {l} {v1 v2 : Forall P l} , Q v1 v2 →
			∀ a p1 p2, Q (List.Forall_cons _ a l p1 v1) (List.Forall_cons _ a l p2 v2)) :=
	fix myrect2_fix l (v1 : Forall P l) : ∀ (v2 : Forall P l), Q v1 v2 :=
	match v1 with
	| List.Forall_nil _ => λ v2, Forall_inv_nil bas v2
	| List.Forall_cons _ a l p fp => λ v2,
		Forall_inv_cons v2 (Q := (λ v2', Q (List.Forall_cons _ a l p fp) v2')) (λ p' fp', rect (myrect2_fix l fp fp') _ _ _)
	end.

Global Instance Forall_pi A (P : A → Prop) `{∀ a, ProofIrrel (P a)} l : ProofIrrel (Forall P l).
Proof.
	intros x y; eapply Forall_rect2 with (v1 := x) (v2 := y); [done|].
	intros; f_equal; [|done].
	apply proof_irrel.
Qed.

Definition LSorted_inv_nil {A} (R : relation A) (Q : LocallySorted R [] → Prop) (H : Q (LSorted_nil _)) v : Q v :=
	match v with
	| LSorted_nil _ => H
	| _ => fun devil => False_ind (@IDProp) devil
	end.

Definition LSorted_inv_cons1 {A} {R : relation A} {a : A} (v : LocallySorted R [a]) :
	∀ (Q : LocallySorted R [a] → Prop) (H : Q (LSorted_cons1 _ _)), Q v :=
	match v with
	| LSorted_cons1 _ b => λ Q H, H
	| _ => fun devil => False_ind (@IDProp) devil
	end.

Definition LSorted_inv_consn {A} {R : relation A} {a b : A} {l : list A} (v : LocallySorted R (a :: b :: l)) :
	∀ (Q : LocallySorted R (a :: b :: l) → Prop) (H : ∀ s r, Q (LSorted_consn _ s r)), Q v :=
	match v with
	| LSorted_consn _ s r => λ Q H, H s r
	| _ => fun devil => False_rect (@IDProp) devil
	end.

Definition LSorted_rect2 A (R : relation A) (Q : ∀ {l}, LocallySorted R l → LocallySorted R l → Prop)
	(bas_nil : Q (LSorted_nil _) (LSorted_nil _))
	(bas_cons1 : ∀ a, Q (LSorted_cons1 _ a) (LSorted_cons1 _ a))
	(rect : ∀ {l} {b} {s1 s2 : LocallySorted R (b :: l)}, Q s1 s2 →
		∀ a r1 r2, Q (LSorted_consn a s1 r1) (LSorted_consn a s2 r2)) :
	∀ l (s1 s2 : LocallySorted R l), Q s1 s2 :=
	fix rect2_fix l (s1 : LocallySorted R l) : ∀ (s2 : LocallySorted R l), Q s1 s2 :=
	match s1 with
	| LSorted_nil _ => λ s2, LSorted_inv_nil bas_nil s2
	| LSorted_cons1 _ a => λ s2, LSorted_inv_cons1 s2 (bas_cons1 a)
	| @LSorted_consn _ _ a b l s' r' => λ s2,
		LSorted_inv_consn s2 (Q := λ s2', Q _ s2') (λ s'' r'', rect (rect2_fix (b :: l) s' s'') _ _ _)
	end.

Global Instance LSorted_pi A (R : A → A → Prop) `{∀ a b, ProofIrrel (R a b)} l : ProofIrrel (LocallySorted R l).
Proof.
	intros x y; eapply LSorted_rect2 with (s1 := x) (s2 := y); [done..|].
	intros; f_equal; [done|].
	apply proof_irrel.
Qed.

(* Stronger heterogenous version of Forall2_Forall *)
Lemma Forall2_Forall' A B (P : A → B → Prop) (l : list A) (k : list B) :
	Forall2 P l k → Forall (uncurry P) (zip l k).
Proof. induction 1; constructor; auto. Qed.

Lemma Forall_Forall2_2 A B (P : A → B → Prop) (l1 : list A) (l2 : list B) (E : length l1 = length l2) :
	Forall (uncurry P) (zip l1 l2) → Forall2 P l1 l2.
Proof.
	induction l1 in l2, E |- *; simpl.
	{ symmetry in E; apply nil_length_inv in E; by subst. }
	destruct l2; [done|].
	destruct 1 using Forall_inv_cons.
	naive_solver.
Qed.

Lemma Forall_Forall2' A B (P : A → B → Prop) (l1 : list A) (l2 : list B) (E : length l1 = length l2) :
	Forall2 P l1 l2 ↔ Forall (uncurry P) (zip l1 l2).
Proof. split; [ apply Forall2_Forall' | by apply Forall_Forall2_2 ]. Qed.

Lemma Forall_Forall2 A B (P : A → B → Prop) (l1 : list A) (l2 : list B) :
	Forall2 P l1 l2 ↔ Forall (uncurry P) (zip l1 l2) ∧ (length l1 = length l2).
Proof.
	split.
	{ split; [by apply Forall2_Forall' | by eapply Forall2_length]. }
	intros []; by apply Forall_Forall2_2.
Qed.

Lemma Forall2_weaken A B (P Q : A → B → Prop) l k :
	Forall2 P l k → Forall2 (λ x y, P x y → Q x y) l k → Forall2 Q l k.
Proof.
	rewrite !Forall_Forall2, !Forall_forall; firstorder.
	destruct x; firstorder.
Qed.

(* Sorted_LocallySorted_iff is opaque! *)
Lemma LocallySorted_dec_bad A (R : relation A) `{∀ a b, Decision (R a b)} : ∀ l, Decision (LocallySorted R l).
Proof. intros ?; rewrite <- Sorted_LocallySorted_iff; tc_solve. Qed.

Global Instance LocallySorted_dec A (R : relation A) `{∀ a b, Decision (R a b)} : ∀ l, Decision (LocallySorted R l).
Proof.
	intros l; induction l as [|a l IHl].
	{ left; constructor. }
	destruct l as [|b l].
	{ left; constructor. }
	destruct (decide (R a b)).
	{ refine (cast_if IHl); [by constructor | abstract by inv 1]. }
	right; abstract by inv 1.
Defined.
