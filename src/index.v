From stdpp Require Import prelude options.
From thesis Require Import lib.

Section IndexDef.
	Variable b : nat.

	Structure index := Index {
		nat_of_index :> nat ;
		#[canonical=no] index_lt : nat_of_index < b ; (* is_true here? Or maybe Is_true? *)
	}.

	Implicit Type i : index.

	Lemma nat_of_indexE k H :
		nat_of_index (@Index k H) = k.
	Proof. done. Qed.

	Lemma Index_nat_of_index i H : @Index (nat_of_index i) H = i.
	Proof. destruct i; f_equal/=. apply proof_irrel. Qed.

	Lemma index_eq {i1 i2} : i1 =@{nat} i2 → i1 = i2.
	Proof.
		destruct i1, i2; simpl; intros ->.
		f_equal; apply proof_irrel.
	Qed.

	Global Instance nat_of_index_inj : Inj (=) (=) nat_of_index.
	Proof. intros ??; apply index_eq. Qed.

	Global Instance index_eq_dec : EqDecision index.
	Proof.
		intros i1 i2. destruct (decide (i1 =@{nat} i2)); [left|right].
		- by apply (inj _).
		- intros ?; simplify_eq.
	Defined.

	Definition mk_index (n : nat) (H : n <? b = true) : index :=
		Index ltac:(by apply Nat.ltb_lt).

	Lemma mk_indexE (n : nat) H : @mk_index n H =@{nat} n.
	Proof. reflexivity. Qed.

	Lemma S_indexP i : S i < S b.
	Proof. apply ->Nat.succ_lt_mono; apply index_lt. Qed.

	Definition index_option (k : nat) : option index := (@Index _) <$> guard (k < b).

	Definition index_enum : list index :=
		omap index_option (seq 0 b).

End IndexDef.

Arguments Index [b i] : rename.
Arguments nat_of_index {b}.

Notation "''I_' n" := (index n)
	(at level 8, n at level 2, format "''I_' n"). (* todo level? *)

Global Hint Resolve index_lt | 0 : core.
Global Hint Resolve index_eq : core.

(* What is this? *)
Lemma canonical_index k n (H : k < n) : {i : 'I_n & k = nat_of_index i}.
Proof. by exists (Index H). Defined.

Canonical S_index b (i : index b) : index (S b) :=
	Index (S_indexP i).

(* Make canonical? *)
Canonical index_bump {b} (i : index b) : index (S b).
Proof. apply Index with i. by apply Nat.lt_lt_succ_r. Defined.

(* Canonical index_le {b1 b2} (H : b1 <= b2) (i : index b1) : index b2.
Proof. apply Index with i. eapply lt_le_trans; done. Defined. *)

Definition index_plus_bump {b x} (i : index b) : index (b + x).
Proof. apply Index with i. by apply Nat.lt_lt_add_r. Defined.

Definition index_plus {b x} (i : index b) : index (x + b).
Proof. apply Index with (x + i). by apply Nat.add_lt_mono_l. Defined.

Canonical index_pred b (i : index b) : index b.
	apply Index with (pred i).
	by apply Nat.lt_lt_pred.
Defined.

Example index_compose_pred b (i : index b) : pred (pred i) < b.
Proof. done. Qed.

Canonical ind0 {n} : 'I_(S n) := Index (Nat.lt_0_succ n).
Definition ind_max {n} : 'I_(S n) := Index (Nat.lt_succ_diag_r n).
Definition ind_default0 {n} k : 'I_(S n) := default ind0 (index_option _ k).

Lemma index_0_false (i : 'I_0) : False.
Proof. destruct i; lia. Qed.

(* Global Hint Resolve index_0_false | 0 : core. *)
Global Hint Extern 0 => match goal with i : 'I_0 |- _ => exfalso; exact (index_0_false i) end : core.

Lemma index_1_ind0 (i : 'I_1) : i = 0.
Proof. by apply index_eq, Nat.lt_1_r. Qed.

Lemma destruct_index n (i : 'I_(S n)) : (i = 0) + {j | i = S_index j}.
Proof.
	destruct (nat_of_index i) as [|j] eqn:H; [left | right].
	- by apply index_eq.
	- unshelve eexists (Index (i := j) _); [|by apply index_eq].
		apply Nat.succ_lt_mono. by rewrite <- H.
Defined.

Lemma index_case (P : ∀ n, 'I_n → Type) :
	(∀ n, P (S n) 0) → (∀ n i, P n i → P (S n) (S_index i)) →
		∀ n i, P n i.
Proof.
	intros P0 PS n i.
	induction n. { done. }
	destruct (destruct_index i) as [-> | [i' ->]].
	- apply P0.
	- apply PS, IHn.
Defined.

Arguments index_option _ _ /.

Lemma index_option_0 n : index_option (S n) 0 = Some ind0.
Proof. simpl. f_equal/=. by apply index_eq. Qed.

Arguments option_guard_True_pi {_ _ _} _.

Lemma index_option_lt_eq n k (H : k < n) : index_option n k = Some $ Index H.
Proof.
	simpl; unshelve erewrite option_guard_True_pi; [done|].
	by f_equal; apply index_eq.
Qed.

Lemma index_option_nat_of_index_id n (i : 'I_n) :
	index_option n (nat_of_index i) = Some i.
Proof. destruct i; apply index_option_lt_eq. Qed.

Lemma index_option_Some_1 n i k : index_option n k = Some i → k =@{nat} i ∧ k < n.
Proof. by intros [?[_ ->]]%fmap_Some_1. Qed.

Lemma index_option_Some_2 n (i : 'I_n) k :
	k =@{nat} i ∧ k < n → index_option n k = Some i.
Proof. intros [-> ?]. apply index_option_nat_of_index_id. Qed.

Lemma index_option_Some n i k : index_option n k = Some i ↔ k =@{nat} i ∧ k < n.
Proof. split; [apply index_option_Some_1 | apply index_option_Some_2]. Qed.

(* Does something like this exist? *)
(* Tactic Notation "assert_apply" constr(P) "by" tactic(tac) :=
	let e := fresh in assert (e : P) by tac; apply e; clear e.
Tactic Notation "assert_apply" constr(P) :=
	let e := fresh in assert (e : P); [|apply e; clear e]. *)

Lemma index_option_Some_lt n k : is_Some $ index_option n k ↔ k < n.
Proof. simpl; case_guard; by firstorder. Qed.
(* simpl; case_guard; simpl; [done|].
by assert_apply (∀ P Q, ¬ P → ¬ Q → P ↔ Q) by done. *)

Lemma index_option_None_1 n k : index_option n k = None → ¬ k < n. (* or n ≤ k ? *)
Proof. simpl. by intros ?%fmap_None%option_guard_False_2. Qed.

Lemma index_option_None_2 n k : ¬ k < n → index_option n k = None.
Proof. intros H; simpl. by rewrite option_guard_False. Qed.

Lemma index_option_None n k : index_option n k = None ↔ ¬ k < n.
Proof. split; [apply index_option_None_1 | apply index_option_None_2]. Qed.

(* Lemma index_option_S_Some n k i :
	index_option n k = Some i → index_option (S n) (S k) = Some (S_index i).
Proof.
	rewrite 2 index_option_Some; simpl. intros []; eauto with arith.
	(* intros [-> ?]%index_option_Some_1.
	by erewrite index_option_lt_eq. *)
Qed.

Lemma index_option_S_None n k :
	index_option n k = None → index_option (S n) (S k) = None.
Proof. rewrite 2 index_option_None. eauto with arith. Qed.

Lemma index_option_S n k :
	index_option (S n) (S k) = (@S_index _) <$> (index_option n k).
Proof.
	simpl. repeat case_decide; simpl; try done || lia.
	f_equal; by apply index_eq.
Qed.

Lemma index_option_0_inv n k : index_option (S n) k = Some ind0 → k = 0.
Proof. by intros [? _]%index_option_Some_1. Qed.

Lemma index_option_S_inv n k i :
	index_option (S n) k = Some (S_index i) → ∃ k', k = S k'.
Proof. intros [? _]%index_option_Some_1. simpl in *. by eexists. Qed.

Lemma index_option_bump n i k :
	index_option n k = Some i → index_option (S n) k = Some (index_bump i).
Proof.
	intros []%index_option_Some_1.
	apply index_option_Some_2; simpl. eauto.
Qed. *)

Arguments index_option _ _ : simpl nomatch.

Lemma nat_of_index_option_id n k :
	from_option nat_of_index k (index_option n k) = k.
Proof. unfold index_option; by case_guard. Qed.

(* needed? *)
Lemma nat_of_index_option_pid n k : k < n →
	nat_of_index <$> (index_option n k) = Some k.
Proof. intros H; by rewrite (index_option_lt_eq H). Qed.

Lemma val_index_enum b : nat_of_index <$> index_enum b = seq 0 b.
Proof.
	(* unfold index_enum. rewrite list_fmap_omap.
	erewrite (list_omap_ext _ (λ x, Some x) _ (seq 0 b)).
	{ rewrite <- list_fmap_alt. by rewrite list_fmap_id. }
	apply Forall_Forall2_diag; apply Forall_forall; intros ? [_ ?]%elem_of_seq. simpl in *.
	by apply nat_of_index_option_pid. *)

	unfold index_enum.
	rewrite list_omap_filter; [| apply nat_of_index_option_id ].
	apply filter_Forall, Forall_seq; simpl; intros j [_ H].
	by rewrite (index_option_lt_eq H).
Qed.

Lemma length_index_enum b : length (index_enum b) = b.
Proof.
	rewrite <- (length_fmap nat_of_index _), val_index_enum.
	apply length_seq.
Qed.

Lemma index_enum_lookup n i : index_enum n !! i = index_option n i.
Proof.
	unfold index_enum. rewrite omap_Forall. 2:done.
	- unfold index_option at 2; case_decide; simpl.
		+ rewrite lookup_seq_lt by done; simpl.
			unfold index_option. unshelve erewrite option_guard_True_pi; done.
			(* apply index_option_Some_lt. *)
		+ rewrite lookup_seq_ge; [done|lia].
	- rewrite Forall_fmap. apply Forall_seq.
		simpl; intros j [_ B].
		by apply index_option_Some_lt.
Qed.

Lemma index_enum_lookup_nat n i : index_enum n !! (nat_of_index i) = Some i.
Proof. rewrite index_enum_lookup. apply index_option_nat_of_index_id. Qed.

(* needed? *)
Lemma index_enum_lookup_lt n i : i < n → nat_of_index <$> index_enum n !! i = Some i.
Proof.
	intros H; rewrite <- (nat_of_indexE H).
	by rewrite index_enum_lookup_nat.
Qed.

Lemma index_enum_In b (i : 'I_b) : i ∈ (index_enum b).
Proof.
	unfold index_enum.
	apply elem_of_list_omap.
	exists i. split.
	- apply elem_of_seq. eauto using le_0_n, index_lt.
	- apply index_option_nat_of_index_id.
Qed.

Local Instance index_S_inhabited n : Inhabited 'I_(S n) := populate ind0.

Lemma index_enum_lookup_total n (i : 'I_(S n)) :
	index_enum (S n) !!! (nat_of_index i) = i.
Proof.
	rewrite list_lookup_total_alt.
	rewrite index_enum_lookup.
	by rewrite index_option_nat_of_index_id.
Qed.

Lemma index_split m n (i : 'I_(m + n)) :
	(∃ j : 'I_m, i = index_plus_bump j) ∨ (∃ j : 'I_n, i = index_plus j).
Proof.
	destruct i as [i ltmn].
	assert (i < m ∨ m <= i < m + n) as [] by lia; [left|right].
	- exists (Index H). by apply index_eq.
	- assert (i - m < n) as H' by lia.
		exists (Index H'). apply index_eq. simpl. lia.
Qed.

Lemma mk_index_0 m : @mk_index (S m) 0 eq_refl = ind0.
Proof. by apply index_eq. Qed.

Lemma nat_of_index_S n (i : 'I_n) : S i = S_index i.
Proof. done. Qed.

Lemma index_enum_NoDup n : NoDup (index_enum n).
Proof.
	unfold index_enum.
	eapply NoDup_fmap_1;
		erewrite list_omap_filter by apply nat_of_index_option_id.
	apply NoDup_filter, NoDup_seq.
Qed.

Global Instance index_finite n : finite.Finite 'I_n := {|
	finite.enum := index_enum n;
	finite.NoDup_enum := index_enum_NoDup n;
	finite.elem_of_enum := index_enum_In (b:=n)
|}.
