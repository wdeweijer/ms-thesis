From stdpp Require Import prelude options gmap sorting.
From thesis Require Import lib vector matrix smith.

Local Coercion Pos.of_succ_nat : nat >-> positive.

Local Notation sc := (list (list (list nat))).
  (* indices in the outermost list correspond with dimension,
    each dimension has a list of simplices,
    each simplex is represented by a list of nats *)

Example diabolo_nat_facet_sc : list (list nat) :=
  [
    [1;2]; [2;3]; [3;1]; [2;4];
    [4;5;6]
  ].

Example circle_nat_facet_sc : list (list nat) :=
  [ [1;2]; [2;3]; [1;3] ].

Definition sc_from_facet_sc (fc : list (list nat)) : sc :=
  let fc := (merge_sort lt <$> fc) in
  let maxdim := list_max $ length <$> fc in
    (λ n, merge_sort lexico $ remove_dups $ concat
      (sublists_of_len n <$> fc))
    <$> seq 1 maxdim.

  (* concat too slow! *)

Definition sc_degen_from_facet_sc (fc : list (list nat)) (maxdim : nat) : sc :=
  let fc := (merge_sort lt <$> fc) in
    (λ n, merge_sort lexico $ remove_dups $ concat
      (sublists_degen_of_len n <$> fc))
    <$> seq 1 (1+maxdim).

Lemma sc_from_facet_sc_face_length fc n :
  Forall (λ f, length f = 1+n) (sc_from_facet_sc fc !!! n).
Proof.
  apply Forall_forall; intros x.
  unfold sc_from_facet_sc.
  generalize (merge_sort lt <$> fc); clear fc; intros fc.
  destruct (le_lt_dec (list_max (length <$> fc)) n).
  { rewrite list_lookup_total_ge; [by rewrite elem_of_nil|].
    by rewrite length_fmap, length_seq. }
  erewrite list_lookup_total_fmap by by rewrite length_seq.
  rewrite merge_sort_Permutation, elem_of_remove_dups.
  rewrite elem_of_list_In, in_concat;
    intros (y & Hy%elem_of_list_In & Hxy%elem_of_list_In); revert Hy.
  rewrite elem_of_list_fmap; intros (z & Hy & Hzfc); subst.
  epose proof (H := sublists_of_len_elem_length _ _);
    rewrite Forall_forall in H; specialize (H _ Hxy) as ->.
  by rewrite lookup_total_seq_lt.
Qed.

Definition list_boundary {A} (l : list A) :=
  (λ n, delete n l) <$> seq 0 (length l).

(* Assumes sc is well-formed *)
Definition boundary_map_tr (S : sc) n : 'M[Z]_(length (S !!! (1+n)), length (S !!! n)).
  unfold matrix.
  set (d1 := S !!! n); set (d2 := S !!! (1+n)); set (ld1 := length d1).
  unshelve refine (Vector (l := _ <$> d2) _);
    [|by rewrite length_fmap]; intros s2; clear d2.
  set (bound_s2 := list_boundary s2).
  set (locs := fst <$> omap (λ f, list_find (λ f', f=f') d1) bound_s2).
  set (part := imap (λ n loc, <[loc := (1 - 2*(n `mod` 2))%Z]> [vec of replicate _ 0%Z] : vector Z ld1) locs).
  exact (foldr (vector_add (n:=ld1)) [vec of replicate _ 0%Z] part).
Defined.

Definition boundary_map S n := (boundary_map_tr S n)^T.

(* Assumes Mcycle . Mbound = 0 *)
Definition homology_at_dim p q r (Mbound : 'M_(p,q)) (Mcycle : 'M_(q,r)) : nat * list Z :=
  let '(_,dbound,_) := Smith Mbound in
  let '(_,dcycle,_) := Smith Mcycle in
  let free := q - length dbound - length dcycle in (* positive because of rank-nullity *)
  let dbound' := Z.abs <$> dbound in
  let torsion := filter (λ k, k <> 1)%Z dbound' in
    (free, torsion).

Definition homology (S : sc) : list (nat * list Z) :=
  (λ k, homology_at_dim (boundary_map S k) (boundary_map S (1+k))) <$> seq 0 (length S - 1).

Definition homology_degen (S : sc) : list (nat * list Z) :=
  removelast (homology S).

(* Definition nat_homology (S : sc) : list (nat * list nat) :=
  (λ p : N * list N, (N.to_nat p.1, N.to_nat <$> p.2)) <$> homology S. *)

Definition nsphere n := removelast (sc_from_facet_sc [(seq 0 (2+n))]).

(* Compute list_of_matrix (boundary_map_tr (nsphere 2) 0).
Compute list_of_matrix (boundary_map (nsphere 2) 1). *)
(* Compute list_of_matrix ((boundary_map (nsphere 4) 1) *m (boundary_map (nsphere 4) 2)). *)

Definition nsphere_degen n maxdim := (sc_degen_from_facet_sc (list_boundary $ seq 0 (2+n)) maxdim).

(* Compute sc_degen_from_facet_sc [[1;2;3]] 5. *)

Example proj_plane := sc_from_facet_sc
  [[1;2;3];[1;6;2];[2;6;4];
   [2;4;5];[2;5;3];[3;5;6];
   [3;6;4];[3;4;1];[1;4;5];
   [1;5;6]].

Definition sc_degen_product (S1 S2 : sc) : sc :=
  zip_with (λ l r, uncurry (zip_with (λ a b, Pos.to_nat $ encode (a,b))) <$>
    list_prod l r) S1 S2.

(* Compute nsphere_degen 1 1.
Compute nsphere_degen 0 1.
Compute sc_degen_product (nsphere_degen 1 1) (nsphere_degen 0 1).

Compute homology_degen $ sc_degen_product (nsphere_degen 1 2) (nsphere_degen 0 2). *)

(* Compute homology [[[0]]; []; []].
Compute homology [[[0]]; [[0;0]]; []].
Compute homology [[[0]]; [[0;0]; [0;0]]; []]. *)

(* Compute (λ n, mx_dim $ boundary_map [[[0]; [1]]; [[0;1]]; []; []] n) <$> [0;1;2;3].
Compute (λ n, mx_dim $ boundary_map [[[0]; [1]]; [[0;0]]; []; []] n) <$> [0;1;2;3].
Compute list_of_matrix $ boundary_map [[[0]]; [[0;0]]; [[0;0;0]]; [[0;0;0;0]]] 0.
Compute homology_degen [[[0]]; [[0;0]]; [[0;0;0]]; [[0;0;0;0]]]. *)

(* Compute homology [[[0]; [1]]; [[0;1]]; []; []].
Compute homology [[[0]; [1]]; []; []; []].
Compute homology [[[0]; [1]]; [[0;0]]; []; []]. *)

(* Compute (λ n, mx_dim $ boundary_map [[[0]]; []; []] n) <$> [0;1;2].
Compute (λ n, mx_dim $ boundary_map [[[0]; [1]; [2]]; []; []] n) <$> [0;1;2].

Compute list_of_matrix $ boundary_map_tr [[[0]]; [[0;0]]; []] 1.
Check ltac:(let t := type of (boundary_map_tr [[[0]]; [[0;0]]; []] 0) in let t' := eval simpl in t in idtac t').
Eval simpl in 'M_((length ([[[0]]; [[0; 0]]; []] !!! (1 + 0))), 
(length ([[[0]]; [[0; 0]]; []] !!! 0))). *)

Time Compute homology $ nsphere 2.
Time Compute homology proj_plane.

(* Structure gset_sc := {
  simplices :> gset (gset positive);
  simplices_closed : ∀ s, s ∈ simplices → ∀ f, f ⊆ s → f ∈ simplices;
}.

Structure gset_map_sc := {
  map :> nat → list (gset positive);
  map_size : ∀ n, Forall (λ s, size s = n) (map n);
  map_closed : ∀ n s, s ∈ map n → ∀ f, f ⊆ s → f ∈ map $ size f;
}. *)

Structure face_map_sc := {
  car : Type;
  car_R : relation car;
  car_R_dec : RelDecision car_R;
  fm_map :> nat → list car;
  fm_face : car → list car;
  fm_map_NoDup : ∀ n, NoDup $ fm_map n;
  (* fm_face_sorted : ∀ s, Sorted car_R (fm_face s);  *) (* No! *)
  fm_face_length : ∀ n, Forall (λ s, length $ fm_face s = 1+n) (fm_map n);
  fm_face_face : ∀ n, Forall (λ s, fm_face s ⊆ fm_map n) (fm_map $ 1+n);
}.



(*

(* transposed *)
Definition face_map_sc_d (sc : face_map_sc) (n : nat) :
  matrix (length $ sc $ 1+n) (length $ sc n).
simpl.
induction (sc (S n)); simpl.
- exact inhabitant.
- apply cons_vector; [| exact IHl].
  
  set (fs := fm_face)



Search size subseteq.


Compute elements (list_to_set [1;2;3]%positive : gset positive).
Compute elements ({[1;2;3]}%positive : gset positive).


Definition builder (facets : nat_facet_sc) : gset_sc.


Example diabolo_gset_sc : gset_sc.
Proof.
  
  eapply Build_gset_sc with (simplices := list_to_set $ list_to_set <$> [
    [];
    [1]; [2]; [3]; [4]; [5]; [6];
    [1; 2]; [2; 3]; [3; 1]; [2; 4]; [4; 5]; [5; 6]; [6; 4];
    [4; 5; 6]
  ]%positive).
  
  intros s Hs f Hf.
  
  

  rewrite elem_of_list_to_set in Hs.
  cbn beta iota delta [ fmap list_fmap ] in Hs.
  
  rewrite elem_of_list_In in Hs.
  destruct Hs; subst.
  - set_solver. unfold "⊆" in Hf. Locate set_subseteq_instance. 
  
  destruct Hs eqn:E.
  subst S.
   in Hs.
  Search elem_of list_to_set.
  rewrite 
  rewrite <- elem_of_elements in Hs.
  Search elements list_to_set.
  cbv in Hs.
  destruct Hs.
  cbn in Hs.
  Search elements elem_of.

 *)
