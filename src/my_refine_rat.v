(** This file is part of CoqEAL, the Coq Effective Algebra Library.
(c) Copyright INRIA and University of Gothenburg, see LICENSE *)
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq zmodp.
From mathcomp Require Import path choice fintype tuple finset bigop order.
From mathcomp Require Import ssralg ssrint ssrnum rat.

From CoqEAL Require Import hrel param refinements.

(******************************************************************************)
(* Non-normalized rational numbers refines SSReflects rational numbers (rat)  *)
(*                                                                            *)
(* rational == Type of non normalized rational numbers                        *)
(*                                                                            *)
(******************************************************************************)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.

Import GRing.Theory Order.Theory Num.Theory Refinements.Op.

(*************************************************************)
(* PART I: Defining datastructures and programming with them *)
(*************************************************************)
Section Q.

Variable Z : Type.

(* Generic definition of rationals *)
Definition Q := (Z * Z)%type.

(* Definition of operators on Q Z *)
Section Q_ops.

Local Open Scope computable_scope.

Context `{zero_of Z, one_of Z, add_of Z, opp_of Z, mul_of Z, eq_of Z, leq_of Z,
          lt_of Z}.
Context `{spec_of Z int}.

#[export] Instance zeroQ : zero_of Q := (0, 1).
#[export] Instance oneQ  : one_of Q  := (1, 1).
#[export] Instance addQ  : add_of Q  := fun x y =>
  (x.1 * y.2 + y.1 * x.2, x.2 * y.2).
#[export] Instance mulQ  : mul_of Q  := fun x y => (x.1 * y.1, x.2 * y.2).
#[export] Instance oppQ  : opp_of Q  := fun x   => (- x.1, x.2).
#[export] Instance eqQ   : eq_of Q   :=
  fun x y => (x.1 * y.2 == y.1 * x.2).
(* #[export] Instance leqQ  : leq_of Q  := (* Incorrect *)
  fun x y => (x.1 * y.2 <= y.1 * x.2). *)
#[export] Instance invQ : inv_of Q   := fun x   =>
  if      (x.1 == 0)%C   then 0
  else if (0 < x.1)      then (x.2, x.1)
                         else (- (x.2), (- x.1)).
#[export] Instance divQ : div_of Q   := fun x y => (x * y^-1).

#[export] Instance specQ : spec_of Q rat :=
  fun q => (spec q.1)%:Q / (spec q.2)%:Q.

(* Compute valq (1 / 0 : rat). *)

(* Embedding from Z to Q *)
#[export] Instance cast_ZQ : cast_of Z Q := fun x => (x, 1).

End Q_ops.
End Q.

Arguments specQ / _ _ _ : assert.

(***********************************************************)
(* PART II: Proving the properties of the previous objects *)
(***********************************************************)
Section Qint.

Instance zero_int : zero_of int     := 0%R.
Instance one_int  : one_of int      := 1%R.
Instance add_int  : add_of int      := +%R.
Instance opp_int  : opp_of int      := -%R.
Instance mul_int  : mul_of int      := *%R.
Instance eq_int   : eq_of int       := eqtype.eq_op.
Instance leq_int  : leq_of int      := Num.le.
Instance lt_int   : lt_of int       := Num.lt.
Instance spec_int : spec_of int int := spec_id.

(* rat_to_Qint = repr *) (* Qint_to_rat = \pi_rat *)

Definition rat_to_Qint (r : rat) : Q int := (numq r, denq r).
Definition Qint_to_rat (r : Q int) : rat := (r.1%:Q / r.2%:Q).

Lemma Qrat_to_intK : cancel rat_to_Qint Qint_to_rat.
Proof. by move=> x; rewrite /Qint_to_rat /= divq_num_den. Qed.

Goal ~ cancel Qint_to_rat rat_to_Qint.
intros H. specialize (H (1,0)). cbv in H. done. Qed.

Local Open Scope rel_scope.

Definition Rrat (r : rat) (q : Q int) : Prop :=
  (q.2 != 0) && (Qint_to_rat q == r). (* Why not rat_to_Qint? *)

Instance Rrat_spec : refines (Rrat ==> Logic.eq) spec_id spec.
Proof. by rewrite refinesE=> _ q /andP [_ /eqP <-]. Qed.

Lemma RratP (x : rat) (a : Q int) :
  refines Rrat x a -> a.2 != 0 /\ x = a.1%:~R / (a.2)%:~R.
Proof. by move=> /refinesP /andP [? /eqP <-]. Qed.

Lemma RratE (x : rat) (a : Q int) :
  refines Rrat x a -> x = a.1%:~R / (a.2)%:~R.
Proof. by move=> /refinesP /andP [? /eqP <-]. Qed.
(* Proof. by move=> rxa; rewrite -[x]/(spec_id _) [spec_id _]refines_eq. Qed. *)

(* We establish the connection of Q int with regard to rat *)
Instance Rrat_0 : refines Rrat 0 0%C.
Proof. by rewrite refinesE. Qed.

Instance Rrat_1 : refines Rrat 1 1%C.
Proof. by rewrite refinesE. Qed.

Instance Rrat_embed : refines (Logic.eq ==> Rrat) intr cast.
Proof.
  rewrite refinesE => n _ <-.
  by rewrite /Rrat /Qint_to_rat /fun_hrel /= mulr1.
Qed.

Instance Rrat_add : refines (Rrat ==> Rrat ==> Rrat) +%R +%C.
Proof.
  rewrite refinesE /Rrat=> x [na da] /andP [/= da_neq0 /eqP <-] y [nb db] /andP [/= na_neq0 /eqP <-] /=.
  (* apply refines_abstr2=> x [na da] /RratP [? ->] y [nb db] /RratP [? ->] /=. *)
  (* rewrite refinesE /Rrat /fun_hrel /Qint_to_rat /=. *)
  rewrite /Qint_to_rat /= /mul_op /mul_int /add_op /add_int.

  rewrite mulf_neq0 //=.
  rewrite addf_div ?intr_eq0 //.
  by rewrite ?(rmorphD, rmorphM).
Qed.

Instance Rrat_opp : refines (Rrat ==> Rrat) -%R -%C.
Proof.
  apply refines_abstr => x a /RratP [H ->].
  rewrite refinesE /Rrat /fun_hrel /Qint_to_rat H /=; simpC.
  by rewrite -mulNr rmorphN.
Qed.

Instance Rrat_mul : refines (Rrat ==> Rrat ==> Rrat) *%R *%C.
Proof.
  apply refines_abstr2=> x [na da] /RratP /= [da_ne0 /= ->]
                        y [nb dn] /RratP /= [db_ne0 /= ->].
  rewrite refinesE /Rrat /fun_hrel /Qint_to_rat /=; simpC.
  rewrite mulf_neq0 //=.
  rewrite mulrACA -invfM ?(rmorphD, rmorphM) /=.
  by rewrite ?rmorphM /=.
Qed.

Instance Rrat_inv : refines (Rrat ==> Rrat) GRing.inv inv_op.
Proof.
  apply refines_abstr => x [na da] /RratP /= [da_ne0 ->] /=.
  rewrite refinesE /Rrat /fun_hrel /Qint_to_rat /= /inv_op /invQ /=.
  rewrite /lt_op /lt_int; simpC.
  have [-> /=|na_neq0 /=] := altP (na =P 0).
    by rewrite !mul0r ?invr0.
  have [na_gt0|na_le0] /= := ltrP 0 na.
    by rewrite na_neq0 /= invfM invrK mulrC.
  rewrite oppr_eq0 na_neq0 /=.
  rewrite mulrNz mulNr -mulrN -invrN -rmorphN /=.
  by rewrite opprK invfM invrK mulrC.
Qed.

Instance Rrat_eq : refines (Rrat ==> Rrat ==> bool_R) eqtype.eq_op eq_op.
Proof.
  apply refines_abstr2=> x [na da] /RratP /= [da_ne0 /= ->]
                          y [nb dn] /RratP /= [db_ne0 /= ->].
  rewrite /eq_op /eqQ /=; simpC.
  rewrite GRing.eqr_div ?intr_eq0 //.
  rewrite -!rmorphM /= eqr_int.
  rewrite refinesE; exact: bool_Rxx.
Qed.

(* Instance Rrat_leq : refines (Rrat ==> Rrat ==> bool_R) Num.le leq_op.
Proof.
apply refines_abstr2=> x [na da] /RratP /= [da_ne0 /= ->]
                          y [nb dn] /RratP /= [db_ne0 /= ->].
rewrite /leq_op /leqQ /=; simpC.
rewrite ler_pdivrMr ?ltr0z //. 2: shelve.
rewrite mulrAC ler_pdivlMr ?ltr0z //. 2: shelve.
rewrite -!rmorphM /= ler_int.
rewrite refinesE; exact: bool_Rxx. Unshelve.
Qed. *)

Instance Rrat_div : refines (Rrat ==> Rrat ==> Rrat) divq div_op.
Proof.
  apply refines_abstr2=> x [na da] rx y [nb db] ry.
  by rewrite /divq /div_op /divQ; tc.
Qed.

(*************************************************************************)
(* PART III: We take advantage of parametricity to extend correcion of   *)
(* operation on Q int to correction of operations on Q Z, for any Z that *)
(* refines int                                                           *)
(*************************************************************************)
Parametricity zeroQ.
Parametricity oneQ.
Parametricity addQ.
Parametricity mulQ.
Parametricity oppQ.
Parametricity eqQ.
(* Parametricity leqQ. *)
Parametricity invQ.
Parametricity divQ.
Parametricity cast_ZQ.

Check oppQ_R.

Section Qparametric.

(* Z is a type that should implement int *)
Context (Z : Type).
Context (Rint : int -> Z -> Type).

Definition RratC : rat -> Q Z -> Type := (Rrat \o prod_R Rint Rint)%rel.

Context `{zero_of Z, one_of Z, add_of Z, opp_of Z, mul_of Z, eq_of Z,
          leq_of Z, lt_of Z}.
Context `{spec_of Z int}.

Context `{!refines Rint 0%R 0%C, !refines Rint 1%R 1%C}.
Context `{!refines (Rint ==> Rint) -%R -%C}.
Context `{!refines (Rint ==> Rint ==> Rint) +%R +%C}.
Context `{!refines (Rint ==> Rint ==> Rint) *%R *%C}.
Context `{!refines (Rint ==> Rint ==> bool_R) eqtype.eq_op eq_op}.
Context `{!refines (Rint ==> Rint ==> bool_R) Num.le leq_op}.
Context `{!refines (Rint ==> Rint ==> bool_R) Num.lt lt_op}.
Context `{!refines (Rint ==> Logic.eq) spec_id spec}.

#[export] Instance RratC_zeroQ : refines RratC 0 0%C.
Proof. (* We write out the complete proof *)
  unfold RratC. eapply refines_trans; [apply composable_comp|..].
  { apply Rrat_0. }
  rewrite refinesE.
  rewrite /zero_op.
  Check zeroQ_R. Print Ltac tc.
  apply zeroQ_R. (* We can now apply the parametricity lemma *)
  { eapply refinesP. assumption. }
  { eapply refinesP. assumption. }
Qed.

#[export] Instance RratC_oneQ : refines RratC 1 1%C.
Proof. (* We can use typeclass automation *)
  eapply refines_trans.
  1,2: typeclasses eauto.
  rewrite refinesE.
  eapply oneQ_R.
  eapply refinesP. tc.
Qed.

#[export] Instance RratC_cast_ZQ : refines (Rint ==> RratC) intr cast.
Proof.
  eapply refines_trans; tc.
  param cast_ZQ_R.
Qed.

#[export] Instance RratC_addQ : refines (RratC ==> RratC ==> RratC) +%R +%C.
Proof.
  eapply refines_trans; try typeclasses eauto.
  param addQ_R.
Qed.

#[export] Instance RratC_mulQ : refines (RratC ==> RratC ==> RratC) *%R *%C.
Proof. param_comp mulQ_R. Qed.

#[export] Instance RratC_oppQ : refines (RratC ==> RratC) -%R -%C.
Proof.
  param_comp oppQ_R.
  (* unfold RratC. eapply refines_trans. (* 1,2: tc. *)
  { apply composable_imply, composable_comp. }
  { apply Rrat_opp. }
  (* param oppQ_R. *)
  (** In order to apply the parametricity lemma, we need a goal of the form:
     `prod_R Rint Rint _ _`
     To do this, we temporarily remove the identity `refines` function
     using `refinesE` and introduce any variables. **)
  rewrite refinesE. intros ???.
  apply oppQ_R. (** We now apply the parametricity lemma **)
  2:{ (* eapply refinesP; tc. *) assumption. }
  intros ???. eapply refinesP. (* tc. *)
  eapply refines_apply. { eassumption. }
  by apply trivial_refines. *)
Qed.

#[export] Instance RratC_invQ : refines (RratC ==> RratC) GRing.inv inv_op.
Proof. param_comp invQ_R. Qed.

#[export] Instance RratC_divq : refines (RratC ==> RratC ==> RratC) divq div_op.
Proof. param_comp divQ_R. Qed.

#[export] Instance RratC_eqQ :
  refines (RratC ==> RratC ==> bool_R) eqtype.eq_op eq_op.
Proof. param_comp eqQ_R. Qed.

(* #[export] Instance RratC_leqQ : refines (RratC ==> RratC ==> bool_R) Num.le leq_op.
Proof. param_comp leqQ_R. Qed. *)

#[export] Instance RratC_spec : refines (RratC ==> Logic.eq) spec_id spec.
Proof.
  eapply refines_trans; tc.
  rewrite refinesE -[Rint]refinesE; move=> x y rxy.
  case: rxy=> i j rij p q rpq.
  by rewrite /spec /specQ /spec_int /= ![spec_id _]refines_eq.
Qed.

End Qparametric.
End Qint.

From CoqEAL Require Import pos binnat binint.

Section tests.

Typeclasses Opaque GRing.add GRing.mul GRing.exp.

Open Scope N_scope.

(* Lemma test : 10%num * 10%num * (9%num + 1%coq_nat) =
             1000%num. *)

(* Lemma refines_eq_refl A (x : A) : refines eq x x.
Proof. by rewrite refinesE. Qed.
Hint Resolve refines_eq_refl : core. *)

(* Example fruitmath : exists a b c : int, a%:Q/(b+c)%:Q + b%:Q/(a+c)%:Q + c%:Q/(a+b)%:Q = 4 :> rat.
exists (2%Z^266%Z + 2%Z^264%Z + 2%Z^261%Z + 2%Z^260%Z + 2%Z^258%Z + 2%Z^257%Z + 2%Z^252%Z + 2%Z^250%Z + 2%Z^249%Z + 2%Z^246%Z + 2%Z^245%Z + 2%Z^244%Z + 2%Z^241%Z + 2%Z^240%Z + 2%Z^239%Z + 2%Z^236%Z + 2%Z^230%Z + 2%Z^228%Z + 2%Z^225%Z + 2%Z^223%Z + 2%Z^222%Z + 2%Z^220%Z + 2%Z^219%Z + 2%Z^216%Z + 2%Z^214%Z + 2%Z^212%Z + 2%Z^211%Z + 2%Z^210%Z + 2%Z^206%Z + 2%Z^205%Z + 2%Z^204%Z + 2%Z^203%Z + 2%Z^199%Z + 2%Z^195%Z + 2%Z^194%Z + 2%Z^192%Z + 2%Z^191%Z + 2%Z^187%Z + 2%Z^186%Z + 2%Z^181%Z + 2%Z^176%Z + 2%Z^175%Z + 2%Z^170%Z + 2%Z^168%Z + 2%Z^167%Z + 2%Z^165%Z + 2%Z^164%Z + 2%Z^161%Z + 2%Z^156%Z + 2%Z^155%Z + 2%Z^150%Z + 2%Z^148%Z + 2%Z^146%Z + 2%Z^145%Z + 2%Z^144%Z + 2%Z^138%Z + 2%Z^136%Z + 2%Z^134%Z + 2%Z^131%Z + 2%Z^127%Z + 2%Z^124%Z + 2%Z^123%Z + 2%Z^122%Z + 2%Z^118%Z + 2%Z^117%Z + 2%Z^113%Z + 2%Z^110%Z + 2%Z^109%Z + 2%Z^108%Z + 2%Z^103%Z + 2%Z^102%Z + 2%Z^99%Z + 2%Z^98%Z + 2%Z^94%Z + 2%Z^90%Z + 2%Z^86%Z + 2%Z^84%Z + 2%Z^83%Z + 2%Z^82%Z + 2%Z^80%Z + 2%Z^78%Z + 2%Z^74%Z + 2%Z^73%Z + 2%Z^72%Z + 2%Z^68%Z + 2%Z^67%Z + 2%Z^65%Z + 2%Z^64%Z + 2%Z^63%Z + 2%Z^59%Z + 2%Z^57%Z + 2%Z^56%Z + 2%Z^54%Z + 2%Z^53%Z + 2%Z^52%Z + 2%Z^51%Z + 2%Z^50%Z + 2%Z^48%Z + 2%Z^47%Z + 2%Z^46%Z + 2%Z^45%Z + 2%Z^44%Z + 2%Z^43%Z + 2%Z^41%Z + 2%Z^38%Z + 2%Z^35%Z + 2%Z^34%Z + 2%Z^30%Z + 2%Z^29%Z + 2%Z^27%Z + 2%Z^26%Z + 2%Z^25%Z + 2%Z^24%Z + 2%Z^23%Z + 2%Z^20%Z + 2%Z^17%Z + 2%Z^16%Z + 2%Z^15%Z + 2%Z^7%Z + 2%Z^5%Z + 2%Z^3%Z + 2%Z^2%Z + 2%Z^1%Z + 2%Z^0%Z).
exists (2%Z^+264%N + 2%Z^+261%N + 2%Z^+260%N + 2%Z^+259%N + 2%Z^+258%N + 2%Z^+257%N + 2%Z^+254%N + 2%Z^+253%N + 2%Z^+252%N + 2%Z^+250%N + 2%Z^+248%N + 2%Z^+247%N + 2%Z^+245%N + 2%Z^+244%N + 2%Z^+243%N + 2%Z^+239%N + 2%Z^+238%N + 2%Z^+237%N + 2%Z^+236%N + 2%Z^+235%N + 2%Z^+230%N + 2%Z^+226%N + 2%Z^+224%N + 2%Z^+223%N + 2%Z^+222%N + 2%Z^+221%N + 2%Z^+220%N + 2%Z^+218%N + 2%Z^+217%N + 2%Z^+214%N + 2%Z^+213%N + 2%Z^+212%N + 2%Z^+211%N + 2%Z^+210%N + 2%Z^+206%N + 2%Z^+204%N + 2%Z^+201%N + 2%Z^+200%N + 2%Z^+198%N + 2%Z^+195%N + 2%Z^+194%N + 2%Z^+192%N + 2%Z^+190%N + 2%Z^+187%N + 2%Z^+178%N + 2%Z^+177%N + 2%Z^+176%N + 2%Z^+172%N + 2%Z^+171%N + 2%Z^+170%N + 2%Z^+169%N + 2%Z^+167%N + 2%Z^+166%N + 2%Z^+165%N + 2%Z^+162%N + 2%Z^+161%N + 2%Z^+160%N + 2%Z^+159%N + 2%Z^+157%N + 2%Z^+156%N + 2%Z^+155%N + 2%Z^+154%N + 2%Z^+150%N + 2%Z^+149%N + 2%Z^+147%N + 2%Z^+144%N + 2%Z^+141%N + 2%Z^+140%N + 2%Z^+138%N + 2%Z^+137%N + 2%Z^+136%N + 2%Z^+135%N + 2%Z^+132%N + 2%Z^+129%N + 2%Z^+128%N + 2%Z^+126%N + 2%Z^+122%N + 2%Z^+121%N + 2%Z^+119%N + 2%Z^+118%N + 2%Z^+115%N + 2%Z^+114%N + 2%Z^+113%N + 2%Z^+110%N + 2%Z^+105%N + 2%Z^+101%N + 2%Z^+99%N + 2%Z^+96%N + 2%Z^+95%N + 2%Z^+94%N + 2%Z^+93%N + 2%Z^+92%N + 2%Z^+90%N + 2%Z^+89%N + 2%Z^+86%N + 2%Z^+85%N + 2%Z^+84%N + 2%Z^+83%N + 2%Z^+82%N + 2%Z^+80%N + 2%Z^+78%N + 2%Z^+77%N + 2%Z^+73%N + 2%Z^+72%N + 2%Z^+71%N + 2%Z^+68%N + 2%Z^+67%N + 2%Z^+65%N + 2%Z^+64%N + 2%Z^+63%N + 2%Z^+62%N + 2%Z^+59%N + 2%Z^+57%N + 2%Z^+56%N + 2%Z^+53%N + 2%Z^+50%N + 2%Z^+49%N + 2%Z^+47%N + 2%Z^+46%N + 2%Z^+44%N + 2%Z^+43%N + 2%Z^+42%N + 2%Z^+37%N + 2%Z^+36%N + 2%Z^+35%N + 2%Z^+34%N + 2%Z^+33%N + 2%Z^+30%N + 2%Z^+29%N + 2%Z^+28%N + 2%Z^+27%N + 2%Z^+23%N + 2%Z^+19%N + 2%Z^+18%N + 2%Z^+16%N + 2%Z^+15%N + 2%Z^+14%N + 2%Z^+13%N + 2%Z^+12%N + 2%Z^+11%N + 2%Z^+9%N + 2%Z^+8%N + 2%Z^+7%N + 2%Z^+4%N + 2%Z^+3%N + 2%Z^+1%N + 2%Z^+0%N).
exists (2%Z^+261%N + 2%Z^+258%N + 2%Z^+256%N + 2%Z^+255%N + 2%Z^+254%N + 2%Z^+250%N + 2%Z^+248%N + 2%Z^+246%N + 2%Z^+245%N + 2%Z^+244%N + 2%Z^+240%N + 2%Z^+235%N + 2%Z^+234%N + 2%Z^+232%N + 2%Z^+229%N + 2%Z^+228%N + 2%Z^+227%N + 2%Z^+223%N + 2%Z^+222%N + 2%Z^+221%N + 2%Z^+220%N + 2%Z^+219%N + 2%Z^+216%N + 2%Z^+215%N + 2%Z^+214%N + 2%Z^+213%N + 2%Z^+206%N + 2%Z^+205%N + 2%Z^+202%N + 2%Z^+194%N + 2%Z^+193%N + 2%Z^+188%N + 2%Z^+184%N + 2%Z^+182%N + 2%Z^+180%N + 2%Z^+179%N + 2%Z^+177%N + 2%Z^+176%N + 2%Z^+170%N + 2%Z^+164%N + 2%Z^+162%N + 2%Z^+160%N + 2%Z^+159%N + 2%Z^+158%N + 2%Z^+157%N + 2%Z^+156%N + 2%Z^+155%N + 2%Z^+154%N + 2%Z^+152%N + 2%Z^+141%N + 2%Z^+138%N + 2%Z^+136%N + 2%Z^+134%N + 2%Z^+132%N + 2%Z^+131%N + 2%Z^+128%N + 2%Z^+127%N + 2%Z^+126%N + 2%Z^+125%N + 2%Z^+124%N + 2%Z^+122%N + 2%Z^+121%N + 2%Z^+120%N + 2%Z^+119%N + 2%Z^+118%N + 2%Z^+117%N + 2%Z^+115%N + 2%Z^+114%N + 2%Z^+113%N + 2%Z^+111%N + 2%Z^+107%N + 2%Z^+105%N + 2%Z^+104%N + 2%Z^+100%N + 2%Z^+97%N + 2%Z^+96%N + 2%Z^+86%N + 2%Z^+85%N + 2%Z^+84%N + 2%Z^+81%N + 2%Z^+79%N + 2%Z^+77%N + 2%Z^+75%N + 2%Z^+72%N + 2%Z^+69%N + 2%Z^+68%N + 2%Z^+64%N + 2%Z^+63%N + 2%Z^+62%N + 2%Z^+60%N + 2%Z^+56%N + 2%Z^+55%N + 2%Z^+52%N + 2%Z^+51%N + 2%Z^+50%N + 2%Z^+48%N + 2%Z^+47%N + 2%Z^+46%N + 2%Z^+45%N + 2%Z^+44%N + 2%Z^+41%N + 2%Z^+37%N + 2%Z^+36%N + 2%Z^+35%N + 2%Z^+34%N + 2%Z^+33%N + 2%Z^+32%N + 2%Z^+31%N + 2%Z^+30%N + 2%Z^+29%N + 2%Z^+28%N + 2%Z^+26%N + 2%Z^+21%N + 2%Z^+19%N + 2%Z^+16%N + 2%Z^+15%N + 2%Z^+11%N + 2%Z^+7%N + 2%Z^+2%N).
Timeout 5 coqeal.
Timeout 3 vm_compute. *)

Example taxicab422 : 59^4 + 158^4 = 133^4 + 134^4 :> int.
Proof.
  (* rewrite !natz -!exprnP. *)
  change ?n%:R with n%:Z; unfold "^".
  (* rewrite [X in X = _]RintE. *)

  (* Fail Timeout 5 reflexivity. *)
  Time by coqeal.
Qed.


Goal (0 == 0 :> rat).
by coqeal. Show Proof. (* Cheating! *)
Abort.

Goal (1 == 1 :> rat).
by coqeal.
Abort.

Goal (3%:~R / 4%:~R == - (- (3 * 10)%:Z)%:~R / (2 * 20)%N%:~R :> rat).
by coqeal.
Abort.

Goal ((3%:~R / 4%:~R) * (20%:~R / 15%:~R) == 1 :> rat).
eapply refines_goal. tc.
by coqeal.
Abort.

Goal ((1 / 2%:~R)^+3 == (1 / 2%:~R) - (3%:~R / 8%:~R) :> rat).
by coqeal.
Abort.

Goal ((1 / 10%:~R)^-1 == 10%:~R :> rat).
by coqeal.
Abort.

Goal ((1 / 15%:~R) / (2%:~R / 21%:~R) == 7%:~R / 10%:~R :> rat).
by coqeal.
Abort.

(* Lemma foo (P : bool -> Type) : *)
(*   P true -> *)
(*   P ((11*100+1)%N%:~R / (44*100)%N%:~R + (22*100-1)%N%:~R/(44*100)%N%:~R *)
(*      == 3%:~R / 4%:~R :> rat). *)
(* Proof. *)
(* Time by vm_compute. (* 20s *) *)
(* Restart. *)
(* Time by rewrite [X in _ -> P X]refines_boolE. (* 1s *) *)
(* (* TODO : deal with tons of successors => *) *)
(* (*    only possible through a plugin imo -- Cyril*) *)
(* Qed. *)

End tests.