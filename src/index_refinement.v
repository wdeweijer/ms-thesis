Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq ssralg.
From mathcomp Require Import path choice fintype tuple finset bigop.
Set Warnings "notation-overridden, ambiguous-paths".

From CoqEAL Require Import hrel param refinements.

From stdpp Require prelude options.

From thesis Require Import ssr_lib.

(* From thesis Require Import lib. *)
From thesis Require index.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory.
Import Refinements.Op.

Local Open Scope ring_scope.

Local Notation "''mI_' n" := (index.index n)
(at level 8, n at level 2, format "''mI_' n"). (* todo level? *)

Definition mind_to_cind (n : nat) : 'mI_n -> 'I_n :=
  fun '{| index.index_lt := H |} => Ordinal (introT ltP H).

Definition cind_to_mind (n : nat) : 'I_n -> 'mI_n :=
  fun '(@Ordinal _ _ H) => index.Index (elimT ltP H).

Lemma mind_to_cindK n : cancel (@cind_to_mind n) (@mind_to_cind n).
Proof. rewrite /cancel=>[[??]] /=. by apply: val_inj. Qed.

Lemma cind_to_mindK n : cancel (@mind_to_cind n) (@cind_to_mind n).
Proof. rewrite /cancel=>[[??]] /=. by eapply index.index_eq. Qed.

Lemma nat_of_ord_index n (i : 'mI_n) :
  nat_of_ord (mind_to_cind i) = index.nat_of_index i.
Proof. by case: i. Qed.

Lemma nat_of_index_ord n (i : 'I_n) :
  index.nat_of_index (cind_to_mind i) = nat_of_ord i.
Proof. by case: i. Qed.

Lemma mindex_enum n : map (@mind_to_cind n) (index.index_enum n) = enum 'I_n.
Proof.
  case: n=>[/=|n]; first by rewrite enumT unlock.
  remember n.+1.
  apply: eq_from_nth; rewrite size_map -length_size index.length_index_enum.
    by rewrite -cardE card_ord.
  move=> i H; apply: val_inj=> /=.
  erewrite nth_map; last by rewrite -length_size index.length_index_enum.
  rewrite nth_enum_ord // nat_of_ord_index -lookup_nth.
  move: H=>/ltP H; destruct (index.canonical_index H) as [k ->].
  by rewrite index.index_enum_lookup_nat.
  Unshelve. all:subst. exact ord0. exact index.ind0.
Qed.

Lemma cindex_enum n : index.index_enum n = map (@cind_to_mind n) (enum 'I_n).
Proof. apply/(canRL (mapK (@cind_to_mindK n)))/mindex_enum. Qed.

Section index.

Local Open Scope rel_scope.

Instance spec_mI : forall n, spec_of 'mI_n 'I_n := mind_to_cind.
Instance implem_mI : forall n, implem_of 'I_n 'mI_n := cind_to_mind.

Definition Rind (n : nat) : 'I_n -> 'mI_n -> Type := fun_hrel (@mind_to_cind n).

Instance Rind_spec n : refines (@Rind n ==> Logic.eq) spec_id spec.
Proof. by rewrite refinesE. Qed.

Instance Rind_implem n : refines (Logic.eq ==> @Rind n) implem_id implem.
Proof.
  rewrite refinesE /Rind /fun_hrel /implem  => x _ <-.
	apply mind_to_cindK.
Qed.

Instance Rind_0 (n : nat) :
  refines (@Rind n.+1) ord0 index.ind0. (* make into class *)
Proof. rewrite refinesE. by apply: val_inj. Qed. 

Instance Rind_nat (n : nat) :
  refines (@Rind n ==> nat_R) (@nat_of_ord n) index.nat_of_index. (* make into class *)
Proof.
  apply: refines_abstr => a [? ?] /refinesP /=.
  rewrite refinesE => <- /=.
  apply nat_Rxx.
Qed.

Instance Rind_S (n : nat) :
  refines (@Rind n ==> @Rind n.+1) (lift ord0) (@index.S_index n). (* make into class *)
Proof.
  apply: refines_abstr => a [k H] /refinesP <- /=. rewrite refinesE.
  by apply: ord_inj.
Qed.

End index.
