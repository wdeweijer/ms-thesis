From stdpp Require Import prelude options gmap sorting.
From thesis Require Import lib vector.


(* Record degen_op (n : nat) := {
	op_list :> list nat ;
	op_incr : LocallySorted (flip (<)) op_list ;
	(* op_incr : bool_decide (Sorted (flip (<)) op_list) ; *)
	(* op_incr : Sorted (flip (<)) op_list ; *)
	(* op_bounded : bool_decide (Forall2 (λ i k, k < n + (length op_list - i)) (seq 0 (length op_list)) op_list) ; *)
	op_bounded : Forall (λ '(i, k), k < n + (length op_list - i)) (zip (seq 0 (length op_list)) op_list) ;
	(* op_bounded : Forall2 (λ i k, k < n + (length op_list - i)) (seq 0 (length op_list)) op_list ; *)
	(* op_bounded : ∀ i, op_list !!! i < n + (length op_list - i) ; *)
}. *)

Record degen_op_subnormal (n : nat) := {
	op_list :> list nat ;
	op_bounded : Forall (λ '(i, k), k < n + (length op_list - i)) (zip (seq 0 (length op_list)) op_list) ;
}.

Record degen_op (n : nat) := {
	op_subnormal :> degen_op_subnormal n ;
	op_decr : LocallySorted (flip (<)) op_subnormal ;
}.

Definition degen_op_cod n (s : degen_op_subnormal n) := length (op_list s) + n.

Definition degen_op_subnormal' (dom cod : nat) := {s : degen_op_subnormal dom | degen_op_cod s = cod}.
Definition degen_op' (dom cod : nat) := {s : degen_op dom | degen_op_cod s = cod}.

(* Definition degen_op_subnormal_comp a b c (s : degen_op_subnormal' b c) (t : degen_op_subnormal' a b) : degen_op_subnormal' a c.
	destruct s as [[s Hs'] Hs], t as [[t Ht'] Ht]; cbn in *.
	unshelve refine ({| op_list := s ++ t |} ↾ _).
	2:{ cbn. rewrite app_length; lia. }
	apply Forall2_Forall.
	apply Forall_Forall2' in Hs'; [|done].
	apply Forall_Forall2' in Ht'; [|done].
	
	
	rewrite app_length.
	rewrite seq_app; simpl.
	apply Forall2_app.

	(* rewrite zip_with_app by done.
	apply Forall_app; split. *)
	- About Forall_weaken. About Forall2_impl.
	
	apply (Forall_weaken Hs').
		intros [] (? & ? & ? & E & ?)%elem_of_zip_with; simplify_eq;
			apply elem_of_seq in E as [_ E]; simpl in *.
		lia.
	-	match goal with |- context C [seq ?x ?y] => replace (seq x y) with (seq (x + 0) y) by by f_equal end.
		erewrite <- fmap_add_seq.
		Check Forall2_Forall.
		apply Forall2_Forall; rewrite Forall2_fmap_l; apply Forall_Forall2'; [by rewrite seq_length|]; unfold uncurry; cbn. (* ugly *)

		apply (Forall_weaken Ht').
		intros [] (? & ? & ? & E & ?)%elem_of_zip_with; simplify_eq;
			apply elem_of_seq in E as [_ E]; simpl in *.
		lia.
Qed. *)

(* Make a nicer way of creating these *)
Example example_op_subnormal : degen_op_subnormal 3 :=
	ltac:(refine {| op_list := [4;1] |}; (* done *) compute_done).
Example example_op : degen_op 3 := ltac:(refine {| op_subnormal := example_op_subnormal |}; (* done *) compute_done).
Example example_op' : degen_op' 3 5 := example_op ↾ eq_refl.

Definition id_op n : degen_op n := {|
	op_subnormal := {|
		op_list := [] ;
		op_bounded := Forall_nil_2 _
	|};
	op_decr := LSorted_nil _;
|}.

Global Instance : ∀ n, Inj (=) (=) (@op_list n).
Proof.
	intros ? [] []; simpl; intros ->; f_equal.
	apply Forall_pi; intros []; tc_solve. (* Get this to go automatically *)
Defined.

Global Instance : ∀ n, Inj (=) (=) (@op_subnormal n).
Proof.
	intros ? [] []; simpl; intros ->; f_equal.
	apply proof_irrel.
Defined.

Global Instance degen_op_subnormal_eq_dec n : EqDecision (degen_op_subnormal n).
Proof. unshelve eapply inj_eq_dec; try tc_solve. tc_solve. Defined.

Global Instance degen_op_eq_dec n : EqDecision (degen_op n).
Proof. unshelve eapply inj_eq_dec; try tc_solve. tc_solve. Defined.

Fixpoint postcomp_degen_op_aux (t : nat) (s : list nat) :=
	match s with
	| [] => [t]
	| s :: s' => if decide (t ≤ s)
		then (S s) :: postcomp_degen_op_aux t s'
		else t :: s :: s'
	end.

(* Compute foldr duplicate [0;1;2;3] [1;4;0].
Compute foldr duplicate [0;1;2;3] [5;1;0]. *)

Lemma degen_op_0 (s : degen_op 0) : ∃ n, op_list s = reverse (seq 0 n).
Proof.
	destruct s as [[l Hb] Hd]; simpl. induction l; simpl in *.
	{ by exists 0. }
	destruct IHl as [n ->].
	{ clear Hd. 
		apply Forall_cons in Hb as [_ H].
		rewrite <-fmap_S_seq in H.
		replace (seq 0 (length l)) with (id <$> seq 0 (length l)) by by rewrite list_fmap_id. (* bah *)
		revert H.
		by rewrite 2 zip_fmap_l, 2 Forall_fmap. }
		(* apply Forall_cons in Hb as [_ H].
		apply Forall_Forall2_2 in H; [|done].
		rewrite <-fmap_S_seq in H.
		apply Forall2_fmap_l in H. apply Forall_Forall2 in H as [H _]. eapply Forall_impl. 1: exact H.
		by intros []. (* too much work *) } *)
	{ clear Hb.	inv Hd; eauto using LocallySorted. }
	exists (S n).
	rewrite <-reverse_snoc, seq_S; repeat f_equal; simpl.
	apply Nat.le_antisymm.
	- clear Hd. rewrite Forall_forall in Hb; specialize (Hb (0,a) (elem_of_list_here _ _)); simpl in Hb.
		rewrite reverse_length, seq_length in Hb; lia.
	- clear Hb. destruct n; [lia|]. inv Hd.
		{ exfalso; simpl in H1; rewrite reverse_cons in H1. by eapply (app_cons_not_nil _ _ 0 H1). }
		apply (f_equal head) in H0; simpl in *.
		rewrite head_reverse, last_cons in H0.
		destruct n; [lia|]. rewrite last_seq in H0; simplify_eq; lia.
Qed.

Lemma foldr_duplicate_seq A (x : A) n : foldr duplicate [x] (reverse (seq 0 n)) = replicate (S n) x.
Proof.
	induction n; [done|].
	rewrite seq_S, reverse_snoc; simpl.	rewrite IHn.
	by rewrite duplicate_replicate.
Qed.

Lemma ding n : length $ foldr duplicate [0] (reverse (seq 0 n)) = S n.
Proof. by rewrite foldr_duplicate_seq, replicate_length. Qed.

Lemma degen_op_eq_duplicate n (s s' : degen_op n) :
	foldr duplicate (seq 0 (S n)) s = foldr duplicate (seq 0 (S n)) s' →
	s = s'.
Proof.
	intros H. do 2 (eapply inj; [tc_solve|]).
	destruct s as [[l Hb] Hd], s' as [[l' Hb'] Hd']; simpl op_subnormal in *; simpl op_list in *.

	induction l in Hb, Hd, l', Hb', Hd', H |- *.
	{ clear Hb Hd. rewrite seq_S in H. simpl in H. admit. }
	destruct l'. { clear IHl Hb' Hd'. simpl in *. admit. }
	simpl in H.
	erewrite (IHl _ _ l' _ _ _) in H.
	assert (a = n0) by admit. subst.
	f_equal.
	apply IHl.
Admitted.
(* 
	induction n; simpl.
	- revert H; simpl; destruct (degen_op_0 s) as [n ->], (degen_op_0 s') as [n' ->]. intros H.
		do 2 f_equal.
		apply (f_equal length) in H; rewrite 2 ding in H; eauto.
	-	simpl in H.
		destruct s as [[l Hb] Hd], s' as [[l' Hb'] Hd']; simpl in *.
Abort. *)

Lemma duplicate_postcomp_degen_op_aux A (l : list A) t s :
	foldr duplicate l (postcomp_degen_op_aux t s) = duplicate t (foldr duplicate l s).
Proof.
	induction s as [|s s' IHs]; simpl; [done|].
	case_decide as E; [|done]. (* case_match eqn:E. *) (* orthogonalize [eqn:E] vs [as E] *)
	rewrite (duplicate_duplicate_le _ E). by rewrite <- IHs.
Qed.

Lemma length_postcomp_degen_op_aux t s : length (postcomp_degen_op_aux t s) = S (length s).
Proof.
	induction s; simpl; [done|].
	case_decide; simpl; eauto.
Qed.

Definition degen_op_normalize_aux (s : list nat) :=
	foldr postcomp_degen_op_aux [] s.

Lemma length_degen_op_normalize_aux (s : list nat) :
	length (degen_op_normalize_aux s) = length s.
Proof.
	induction s; simpl; [done|].
	rewrite length_postcomp_degen_op_aux. eauto.
Qed.

Lemma duplicate_op_normalize_aux A (l : list A) s :
	foldr duplicate l s = foldr duplicate l (degen_op_normalize_aux s).
Proof.
	induction s; simpl; [done|].
	by rewrite duplicate_postcomp_degen_op_aux, IHs.
Qed.

Definition postcomp_degen_op (a b : nat) (t : {t' : nat | t' ≤ b}) (s : degen_op' a b) : degen_op' a (S b).
	destruct s as [[[s Hbounded] Hdecr] Hcod]; cbn in *.
	destruct t as [t Ht].
	unshelve eexists; [unshelve econstructor|]; [unshelve econstructor|..]; cbn.
	{ exact (postcomp_degen_op_aux t s). }
	2:{ (* destruct t as [t Ht]; destruct s as [[s Hs_sorted Hs_lt] Hs]; cbn in Hs. *)
		clear Hdecr Hcod Ht.
		induction s; [constructor|].
		ospecialize (IHs _). { cbn in Hbounded. apply Forall_cons in Hbounded as [_ ?].
		rewrite <- fmap_S_seq in H. Check zip_with_fmap_l. in H. Search (Forall _ (zip_with _ _ _)).
		case_decide. 2:{ constructor; [done|]; simpl; lia. }
		destruct s. { constructor; [done|]; simpl. eauto with arith. }
		case_decide. 2:{ constructor; [done|]; simpl. eauto with arith. }
		constructor; [done|]. simpl. clear H0 IHs H a b t. admit. (* easy *)
	}
	{	destruct t as [t Ht]; destruct s as [[s Hs_sorted Hs_lt] Hs]; cbn in Hs.
		rewrite !length_postcomp_degen_op_aux.
		admit. (* hard *)
	} }
	unfold degen_op_cod; simpl. (* slow! *)
	repeat case_match; simplify_eq; unfold degen_op_cod in *; simpl in *.
	by rewrite length_postcomp_degen_op_aux.
Admitted.

Compute λ f, foldr f 42 [0;1;2].
Compute λ f, foldl f 42 [0;1;2].

Definition postcomp_degen (a b c : nat) (t : degen_op' b c) (s : degen_op' a b) : degen_op' a c.


Section sset.

(* Nondegenerate aka geometric simplices *)
Variable nd : nat → Type.

(* An abstract simplex is a degeneracy operator together with a geometric simplex *)
Record dgen := {
	dgen_simp_dim : nat ;
	dgen_simp : nd dgen_simp_dim ;
	dgen_op : degen_op dgen_simp_dim ;
}.

Definition dgen_dim (s : dgen) : nat :=
	dgen_simp_dim s + length (dgen_op s).
