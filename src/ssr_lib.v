Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq ssralg.
From mathcomp Require Import path choice fintype tuple finset bigop order matrix.
Set Warnings "notation-overridden, ambiguous-paths".

From CoqEAL Require refinements seqmx.

From stdpp Require prelude options.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(*** Relating stdpp primitives to ssreflect ones *)
Section stdpp.
  Variables A B : Type.
  Variables f : A -> B.
  Variables a : A.
  Variables l : list A.
  Variables n m : nat.

  Lemma fmap_map : base.fmap f l = map f l.
  Proof. reflexivity. Qed.
  Lemma omap_pmap (g : A -> option B) : list.list_omap _ _ g l = pmap g l.
  Proof. reflexivity. Qed.
  Lemma seq_iota : List.seq m n = iota m n.
  Proof. reflexivity. Qed.
  Lemma length_size : List.length l = size l.
  Proof. reflexivity. Qed.
  Lemma default_default (p : option A) : option.default a p = odflt a p.
  Proof. reflexivity. Qed.
  Lemma iter_iter g : Nat.iter n g a = iter n g a.
  Proof. reflexivity. Qed.
  Lemma foldr_foldr (g : B -> A -> A) (k : list B) : list.foldr g a k = foldr g a k.
  Proof. reflexivity. Qed.
  Lemma foldl_foldl (g : A -> B -> A) (k : list B) : list.foldl g a k = foldl g a k.
  Proof. reflexivity. Qed.

  Instance : forall P, base.Decision (is_true P).
  Proof. intros P. unfold is_true. apply decidable.bool_eq_dec. Qed.
  Lemma filter_filter (P : pred A) : base.filter P l = filter P l.
  Proof. elim: l => //= a' ?. Admitted.

  Lemma replicate_nseq : list.replicate n a = nseq n a.
  Proof. elim: n => //= n' -> //. Qed.
  Lemma lookup_total_nth `{base.Inhabited A} : base.lookup_total n l = nth base.inhabitant l n.
  Proof. elim: l n=>[|???] [] //=. Qed.
  Lemma lookup_nth : option.default a (base.lookup n l) = nth a l n.
  Proof. elim: l n=>[|???] [] //=. Qed.

End stdpp.

(*** Extra ssreflect stuff ***)
(* I don't think it's really necessary to have all this transparent stuff,
  but it shouldn't really matter *)
(* Definition ord_enum_eq n : seq 'I_n := pmap (insub_eq _) (iota 0 n).

Lemma ord_enum_eqE p : ord_enum_eq p = enum 'I_p.
Proof.
  rewrite enumT unlock /=. (* todo learn from this *)
  rewrite /ord_enum_eq /ord_enum.
  apply: eq_pmap; exact: insub_eqE.
Qed. *)

Lemma size_ord_enum_eq n : size (seqmx.ord_enum_eq n) = n.
Proof. rewrite seqmx.ord_enum_eqE; apply size_enum_ord. Qed.

Definition enum_tuple_eq (n : nat) : n.-tuple 'I_n :=
  Tuple (introT eqP (size_ord_enum_eq n)).

(* would it be better to cast on the other side? *)
Lemma enum_tuple_eqE n : enum_tuple_eq n = tcast (card_ord n) [tuple of enum 'I_n].
Proof. apply val_inj; rewrite /= val_tcast /=; apply seqmx.ord_enum_eqE. Qed.

Lemma enum_tuple_eqE' n :
  tcast (esym (card_ord n)) (enum_tuple_eq n) = [tuple of enum 'I_n].
Proof. apply val_inj; rewrite /= val_tcast /=; apply seqmx.ord_enum_eqE. Qed.

Section matrix_tuple.
  Variables R : Type.
  Variables m n : nat.
  Implicit Type t : m.-tuple (n.-tuple R).
  Implicit Type A : 'M[R]_(m,n).

  (* Row majow *)
  Definition matrix_of_tuple t : 'M[R]_(m,n) :=
    \matrix_(i,j) tnth (tnth t i) j.

  Definition tuple_of_matrix A : m.-tuple (n.-tuple R) :=
    [tuple [tuple A i j | j < n] | i < m].

  Lemma matrix_of_tupleK : cancel matrix_of_tuple tuple_of_matrix.
  Proof.
    move=>t; do 2 (apply: eq_from_tnth=>?; rewrite tnth_mktuple).
    by apply mxE.
  Qed.

  Lemma tuple_of_matrixK : cancel tuple_of_matrix matrix_of_tuple.
  Proof.
    move=>A; apply matrixP=> ? ?; rewrite mxE.
    by do 2 rewrite tnth_mktuple.
  Qed.
  
End matrix_tuple.

Import Order.TTheory.

Lemma bigmin_path (disp : unit) {T : orderType disp} x0 (s : seq T) :
	path <=%O x0 s ->
	\big[Order.min/x0]_(i <- s) i = x0.
Proof.
	(* move=> Hp.
	rewrite big_seq.
	apply: Order.TotalTheory.bigmin_eq_id=> i Hin.
	by have /allP /(_ i Hin) := (order_path_min Order.le_trans Hp).
Qed. *)
	rewrite unlock.
	elim: s=> //= x s' IH /andP [Hle Hp].
	have {IH}-> := IH (path_le _ Hle Hp); last by apply le_trans.
	by apply min_r.
Qed.

Lemma sorted_bigmin (disp : unit) {T : orderType disp} x0 (s : seq T) :
  sorted <=%O s ->
  \big[Order.min/x0]_(i <- s) i = Order.min (head x0 s) x0.
Proof.
	case: s=> //= [|x s'].
		rewrite big_nil /Order.min; by case: ifP.
	rewrite big_cons=> Hp.
	have /allP Hall := (order_path_min le_trans Hp).
	rewrite big_seq.
	rewrite [RHS]minEle; case: ifP=> Hxx0.
		rewrite [LHS]minEle le_bigmin //.
	rewrite bigmin_eq_id ?minEle ?Hxx0 //.
	move=> i Hi; move: Hall Hxx0=> /(_ i Hi). admit. (* boring *)
Admitted.

Lemma sort_head (disp : unit) {T : orderType disp} x0 (s : seq T) :
  \big[Order.min/x0]_(i <- s) i = Order.min (head x0 (sort <=%O s)) x0.
Proof.
	rewrite (perm_big (op:=Order_min__canonical__SemiGroup_ComLaw) (sort <=%O s)) /=. (* wat *)
	2: rewrite perm_sym; apply/permPl; apply: perm_sort.
	by apply sorted_bigmin.
Qed.

Lemma ohead_Some_head T (x x0 : T) l : ohead l = Some x -> head x0 l = x.
Proof. by case: l=> //= ?? []. Qed.

Lemma ohead_Some T (x : T) s : ohead s = Some x -> exists s', s = x :: s'.
Proof. case: s=> //= ?? []->; by eexists. Qed. 

(* Why isn't a general finType ordered by its enum? *)
Lemma pick_least n (P : pred 'I_n) (i : 'I_n) : pick P = Some i -> forall j : 'I_n, (j < i)%O -> ~~ P j.
Proof.
rewrite /pick. move=> HP j Hj.
suffices: j \notin (enum P).
  apply contraNN. rewrite mem_filter /= => Pj.
  apply/andP; split => //. apply mem_index_enum.
move: HP. rewrite /enum_mem -enumT /= -Order.enum_ord filter_sort //; last by apply le_trans.
move=> HP. have [s' HP']:= (ohead_Some HP).
have: sorted <=%O (i :: s') by rewrite -HP'; apply: sort_sorted; apply: le_total.
move=> /= Hp. have := (order_path_min le_trans Hp). rewrite HP'.
rewrite in_cons negb_or=> asdf. apply/andP; split.
	move: Hj; rewrite lt_neqAle=> /andP [] //.
apply/negP=> E. move: asdf=> /allP /(_ _ E).
by rewrite (lt_geF Hj).
Qed. (* ugly *)


Lemma pick_find A (l : seq A) (P : pred A) (x0 : A) :
  [pick i : 'I_(size l) | P (nth x0 l i)] = insub (find P l).
Proof.
  case: (findP _) => [nP | i Hi /(_ x0)HP /(_ x0)Hleast].
  { rewrite insubN ?ltnn //.
    case: (pickP _) => /= [i Hi |?] //.
    exfalso. 
    move: nP => /has_nthP => /(_ x0) nP; apply nP.
    exists i; last by []. apply ltn_ord. }
	have:= @pick_least (size l) (fun i0 => P (nth x0 l i0)) _.
  case: (pickP _) => /= [j Hj | /(_ (Sub i Hi)) E].
	2:by rewrite E in HP.
	move/(_ j erefl)=> Hl.
	case: (i == j) /eqP.
		move=> ->. by rewrite valK.
	move=> /eqP sdf. exfalso.
	have /orP [Hlt|Hlt]:= Order.TotalTheory.lt_total sdf.
	- move: Hl=> /(_ (Ordinal Hi) Hlt) /=. by rewrite HP.
	- move: Hleast=> /(_ _ Hlt). by rewrite Hj.
Qed. (* ugly! *)


(*** CoqEAL stuff ***)
From CoqEAL Require Import hrel param refinements.
Import Refinements.Op.

Lemma refines_eq_refl A (x : A) : refines eq x x.
Proof. by rewrite refinesE. Qed.
Global Hint Resolve refines_eq_refl : core.

Lemma refines_goal_is_true (G G' : bool) : refines bool_R G G' -> is_true G -> is_true G'.
Proof. rewrite refinesE; by destruct 1. Qed.
