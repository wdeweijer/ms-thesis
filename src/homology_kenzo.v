From stdpp Require Import prelude options gmap sorting.
From thesis Require Import lib vector matrix smith.

(* Any simplex may be expressed in a unique way as a possibly iterated degeneracy
  of a non–degenerate simplex. *)

(* Example:             nondegen simplex    iterated degeneracy op *)
Compute foldr duplicate [0;1;2;3]           [5;1;0].

Fixpoint iter_duplicate {A} (d : list bool) (s : list A) {struct d} : list A :=
  match d, s with
  | [], _ => s
  | _, [] => [] (* forbidden? *)
  | true :: d', a :: s' => a :: iter_duplicate d' s
  | false :: d', a :: s' => a :: iter_duplicate d' s'
  end.

Compute iter_duplicate [true; true] [0;1;2;3].

Compute iter_duplicate [true; true; false; false; false; true] [0;1;2;3].
