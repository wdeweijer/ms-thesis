Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq zmodp.
From mathcomp Require Import path choice fintype finset bigop.
From mathcomp Require Import ssralg ssrnum.
Set Warnings "notation-overridden, ambiguous-paths".

Require Import Lia.

From thesis Require Import bin_nat_aux.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope nat_scope.

Import GRing.Theory Num.Theory.

#[global] Hint Resolve gcd0n gcdn0 gcd1n gcdn1 : core.

Fixpoint mygcdn (n : nat) : nat -> nat -> nat := fun a b =>
  match n with
    | O => 0 (* arbitrary, since n should be big enough *)
    | S n => match a with
         | O => b
         | _ => mygcdn n (b %% a) a
         end
  end.

Arguments mygcdn : simpl nomatch.

Definition mygcd_bound (a : nat) :=
  match a with
    | O => S O
    | _ => let n := S (Nat.log2 a) in (n+n)%nat
  end.

Definition mygcd_alt a b := mygcdn (S (mygcd_bound a)) a b.

Lemma mygcdnE n a b : mygcdn n.+1 a b = if a == 0 then b else mygcdn n (b %% a) a.
Proof. by case: a. Qed.

(** 1) We prove a weaker & easier bound. *)
Lemma mygcdn_linear_bound : forall n a b,
  a < n -> gcdn a b = mygcdn n a b.
Proof.
  elim=> [|n IHn] [|a] b /ltP H //=.
  rewrite -IHn. by rewrite gcdn_modl gcdnC.
  have {}H: a.+1 <= n by apply/ltP; lia.
  apply: (leq_ltn_trans _ H).
  by rewrite -ltnS ltn_mod.
Qed.

(** 2) For Euclid's algorithm, the worst-case situation corresponds
   to Fibonacci numbers. Let's define them: *)

Fixpoint fibonacci (n : nat) : nat :=
  match n with
    | O => 1
    | S O => 1
    | S (S n as p) => fibonacci p + fibonacci n
  end.

Arguments fibonacci : simpl nomatch.

Lemma fibonacci_incr :
  forall n m, n <= m -> fibonacci n <= fibonacci m.
Proof.
  move=> n m /leP. elim=> // {}m H IH.
  apply: (leq_trans IH); clear.
  case: m=> // m.
  by rewrite /= leq_addr.
Qed.

(** 3) We prove that fibonacci numbers are indeed worst-case:
   for a given number [n], if we reach a conclusion about [gcd(a,b)] in
   exactly [n+1] loops, then [fibonacci (n+1)<=a /\ fibonacci(n+2)<=b] *)

Lemma mygcdn_worst_is_fibonacci : forall n a b,
  0 < a < b ->
  gcdn a b = mygcdn (S n) a b ->
  mygcdn n a b <> mygcdn (S n) a b ->
  fibonacci n <= a /\
  fibonacci (S n) <= b.
Proof.
  case;
    first by move=> a b; repeat case: ltP=> //=; lia.
  elim=>[|n IHn] a b;
    first by repeat case: ltP=> //=; lia.
  case: a=> //= a.
  have [-> // | Hrgt0] := posnP (b %% a.+1).
  move=> Hab H1 H2.
  unshelve epose proof (IHn := IHn _ _ _ _ H2).
  { by rewrite Hrgt0 ltn_mod. }
  { by rewrite gcdn_modl gcdnC. }
  clear H1 H2 Hrgt0. move: IHn=> [Fr Fa].
  apply/andP; rewrite Fa /=.
  apply ltnW in Hab; rewrite -divn_gt0 // in Hab.

  move: Fr Fa Hab.
  rewrite /divn modn_def; case: edivnP=> q r -> _ /=.

  rewrite /addn /addn_rec /muln /muln_rec;
    repeat move=> /leP ?; apply/leP.
  nia.
Qed.

(** 3b) We reformulate the previous result in a more positive way. *)

Lemma mygcdn_ok_before_fibonacci : forall n a b,
  0 < a < b -> a < fibonacci n ->
  gcdn a b = mygcdn n a b.
Proof.
  case=>[|n];
    first by move=> a b; repeat case: ltP=> //=; lia.
  case=>[|a];
     first by move=> b /andP [/ltP]; lia.
  move Ek: (a.+2 - n.+1) => k. elim: k n a Ek => [|k IHk] n a H.
  { move=> ? _ _. apply mygcdn_linear_bound. by move/eqP: H; rewrite subn_eq0. }

  move=> b H0 H1.
  have H2 := mygcdn_worst_is_fibonacci H0.
  have H3 : gcdn a.+1 b = mygcdn n.+2 a.+1 b.
    apply: IHk=> //=.
    - move: H; clear; rewrite /subn /subn_rec; lia. 
    - by apply ltn_addr.
  have {}H2 := H2 _ H3. rewrite H3. clear k IHk H H0 H3.
  symmetry; apply/eqP/negbNE/negP=>/eqP H3.
  have [H4 _]:= H2 H3.
  move: H1 H4. repeat case: leP=> //; lia.
Qed.

(** 4) The proposed bound leads to a fibonacci number that is big enough. *)

Lemma mygcd_bound_fibonacci :
  forall a, 0 < a -> a < fibonacci (mygcd_bound a).
Proof.
  case=> // a _.
  rewrite /mygcd_bound size_log.
  rewrite -[X in X < _]Pnat.Nat2Pos.id //.
  elim: (BinPos.Pos.of_nat a.+1)=> [p IHp | p IHp | //] /=.
  all: rewrite addnS addSn.
  all: move: IHp; case: (BinPos.Pos.size_nat p + BinPos.Pos.size_nat p)
    => [/= /ltP| n /ltP IHp]; first by lia.
  all: rewrite (Pnat.Pos2Nat.inj_xI, Pnat.Pos2Nat.inj_xO) /= /addn /addn_rec; apply/ltP; lia.
Qed.

Lemma mygcdn_is_gcd n a b : (mygcd_bound a <= n)%nat ->
  gcdn a b = mygcdn (S n) a b.
Proof.
  case: a=> // a.
  have := (@mygcd_bound_fibonacci a.+1 (ltn0Sn _)).
  have: 1 < mygcd_bound a.+1.
    by rewrite /= addnS addSn.
  case: (mygcd_bound a.+1)=> // m.
  case: n=> //= n.
  rewrite /divn modn_def.
  case: edivnP=> q r -> /= H2 H0 H3 H.
  rewrite gcdnMDl.
  case: r H2=> // r H2.
  rewrite gcdnC; apply: mygcdn_ok_before_fibonacci; first by [].
  have := fibonacci_incr H; move: H2 H3.
  repeat move=> /leP ?; apply/ltP; lia.
Qed.

Lemma gcd_is_gcd :
  forall a b, gcdn a b = mygcd_alt a b.
Proof. by rewrite /mygcd_alt; intros; apply mygcdn_is_gcd. Qed.
