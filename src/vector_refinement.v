Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq.
From mathcomp Require Import path choice fintype tuple finset bigop order.
From mathcomp Require Import vector ssralg ssrint matrix mxalgebra.
Set Warnings "notation-overridden, ambiguous-paths".

From CoqEAL Require Import hrel param refinements.

From stdpp Require prelude options.

(* From thesis Require lib. (* want to import? *) *)
From thesis Require index vector.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.

Import Refinements.Op.

Declare Scope my_computable_scope.
Delimit Scope my_computable_scope with MC.

Section classes.

(* Class hzero_of I B := hzero_op : forall m n : I, B m n.
Local Notation "0" := hzero_op : hetero_computable_scope. *)

(* Class hmul_of I B := hmul_op : forall m n p : I, B m n -> B n p -> B m p.
Local Notation "*m%HC" := hmul_op.
Local Notation "x *m y" := (hmul_op x y) : hetero_computable_scope. *)

(* Class hopp I B := hopp_op : forall m n : I, B m n -> B m n. *)
(* Local Notation "- x" := (hopp_op x) : hetero_computable_scope. *)

(* Class heq_of I B := heq_op : forall m n : I, B m n -> B m n -> bool.
Local Notation "x == y" := (heq_op x y) : hetero_computable_scope. *)

Local Open Scope nat_scope.

Class usubmx_of B :=
  usubmx_op : forall (m1 m2 n : nat), B (m1 + m2) n -> B m1 n.
Class const_mx_of A B := const_mx_op : forall (m n : nat), A -> B m n.

(* Class map_mx_of A B C D :=
  map_mx_op : (A -> B) -> C -> D. *)


End classes.

(* Typeclasses Transparent hzero_of hmul_of heq_of const_mx_of. *)

(* Section index_op.

Variable N : Type.

Context `{lt_of N}.
Context `{zero_of N}.
Context `{eq_of N}.
(* Context `{one_of N, add_of N}. *)
Context `{spec_of N nat, implem_of nat N}.

Structure Cindex (b : N) := CIndex {
  n :> N ;
  index_lt : (n < b)%C ;
}.
 *)


Section vector_op.
Import vector.

Variable A : Type.
Variable I : nat -> Type.

Definition tuple_of_vector (n : nat) : vector A n -> n.-tuple A :=
  fun '(Vector _ H) => Tuple (introT eqP H).

Definition vector_of_tuple (n : nat) : n.-tuple A -> vector A n :=
  fun '(Tuple _ H) => Vector (elimT eqP H).

Lemma tuple_of_vectorK n : cancel (@tuple_of_vector n) (@vector_of_tuple n).
Proof. move=> [l H] /=. by apply vector_eq. Qed.

Lemma vector_of_tupleK n : cancel (@vector_of_tuple n) (@tuple_of_vector n).
Proof. move=> [l H] /=. by apply: val_inj => /=. Qed.

Context `{eq_of A}.
Context `{forall n, implem_of 'I_n (I n)}.
(* map_vec_of *)
(* lookup *)

End vector_op.
