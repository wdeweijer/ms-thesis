Require Import ZArith QArith.

Search (Z -> Q).
Print inject_Z.


Open Scope Q_scope.

Check (2 : Q) / (3 : Q).

Check Qdiv .
Check Qplus.
Check Qplus 2 3.
Check Qeq_bool 2 3.

Example fruitmath : exists a b c : Z,
    let a := inject_Z a in
    let b := inject_Z b in
    let c := inject_Z c in
    ( a/(b+c) + b/(a+c) + c/(a+b)) == 4.
Proof.
    exists 154476802108746166441951315019919837485664325669565431700026634898253202035277999%Z.
    exists 36875131794129999827197811565225474825492979968971970996283137471637224634055579%Z.
    exists 4373612677928697257861252602371390152816537558161613618621437993378423467772036%Z.
    Timeout 5 reflexivity.

Compute 2/3 : bigQ.

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq zmodp.
From mathcomp Require Import path choice fintype tuple finset bigop order.
From mathcomp Require Import ssralg ssrint ssrnum rat.

From CoqEAL Require Import hrel param refinements pos binnat binint rational.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.

Import GRing.Theory Order.Theory Num.Theory Refinements.Op.

Local Typeclasses Opaque GRing.add.
Local Typeclasses Opaque GRing.mul.
Local Typeclasses Opaque GRing.exp.


Example fruitmath : exists a b c : rat, a/(b+c) + b/(a+c) + c/(a+b) = 4 :> rat.
Check implem.
Check intz.
Check intr 2 : rat.

Example fruitmath : exists a b c : int, a%:Q/(b+c)%:Q + b%:Q/(a+c)%:Q + c%:Q/(a+b)%:Q = 4 :> rat.
exists 4%Z, 1%Z, 3%Z.
Search (_%Z%:~R).
Print ratz.
Search ratz.
rewrite (intz 4%Z).

Timeout 5 coqeal.
Timeout 3 vm_compute.

Goal 59%Z ^+ 4 + 158%Z ^+ 4 = 133%Z ^+ 4 + 134%Z ^+ 4.
Proof.
  (* Fail Timeout 5 reflexivity. *)
  Time by coqeal.
Qed.

Example taxicab422 : 59^4 + 158^4 = 133^4 + 134^4 :> int.
Proof.
  (* Fail Timeout 5 reflexivity. *)
  replace (4 : ssrint_int__canonical__GRing_SemiRing) with 4%Z by reflexivity.
  replace (59 : ssrint_int__canonical__GRing_UnitRing) with 59%Z by reflexivity.
  replace (158 : ssrint_int__canonical__GRing_UnitRing) with 158%Z by reflexivity.
  replace (133 : ssrint_int__canonical__GRing_UnitRing) with 133%Z by reflexivity.
  replace (134 : ssrint_int__canonical__GRing_UnitRing) with 134%Z by reflexivity.
  rewrite -!exprnP.
  Time Timeout 5 coqeal; reflexivity.
Qed.