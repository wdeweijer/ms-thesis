From stdpp Require Import prelude options.
Require Import Arith Lia.
Require Import ZArith BinNat BinPos.
Require Import PeanoNat BinPos Pnat.

Section div.
Open Scope N_scope.

Fixpoint positive_N_div_eucl (a b : positive) : N * N :=
  match a with
  | xH => if (2 <=? b)%positive then (0, 1) else (1, 0)
  | xO a' =>
    let (q, r) := positive_N_div_eucl a' b in
    let r' := 2 * r in
    if r' <? Npos b then (2 * q, r') else (2 * q + 1, r' - Npos b)
  | xI a' =>
    let (q, r) := positive_N_div_eucl a' b in
    let r' := 2 * r + 1 in
    if r' <? Npos b then (2 * q, r') else (2 * q + 1, r' - Npos b)
  end.

Lemma positive_N_div_eucl_eq_pos (a b : positive) :
  prod_map Z.of_N Z.of_N (positive_N_div_eucl a b) = Z.pos_div_eucl a (Zpos b).
Proof.
  induction a; cbn.
  all: repeat case_match; cbn in *; simplify_eq; try done.
  all: rewrite ?Z.ltb_lt, ?Z.ltb_nlt, ?Z.leb_le, ?Z.leb_nle,
               ?N.ltb_lt, ?N.ltb_nlt, ?N.leb_le, ?N.leb_nle,
               ?Pos.ltb_lt, ?Pos.ltb_nlt, ?Pos.leb_le, ?Pos.leb_nle in *; try lia.
  all: rewrite ?N2Z.inj_sub, ?N2Z.inj_add, ?N2Z.inj_mul by lia; simpl; repeat f_equal.
Qed.

Lemma positive_N_div_eucl_div_mod a b : let (q, r) := positive_N_div_eucl a b in
  N.pos a = N.pos b * q + r /\ r < N.pos b.
Proof.
  pose proof (positive_N_div_eucl_eq_pos a b).
  pose proof (Z.pos_div_eucl_eq a (Z.pos b) eq_refl).
  pose proof (Z.pos_div_eucl_bound a (Z.pos b) eq_refl).
  repeat case_match; simpl in *; simplify_eq; lia.
Qed.

End div.

Lemma positive_binary_induction (A : nat -> Prop) :
  A 1 -> (forall n, A n -> A (2 * n)) -> (forall n, A n -> A (S (2 * n)))
  -> forall n, A (S n).
Proof.
  intros H HO HI n.
  setoid_rewrite <- Nat2Pos.id; [|easy].
  induction (Pos.of_nat (S n)).
  - rewrite Pos2Nat.inj_xI. apply HI, IHp.
  - rewrite Pos2Nat.inj_xO. apply HO, IHp.
  - apply H.
Qed.

(* Also in Coq 8.20 as Nat.binary_induction *)
Lemma binary_induction (A : nat -> Prop) :
  A 0 -> (forall n, A n -> A (2 * n)) -> (forall n, A n -> A (S (2 * n)))
  -> forall n, A n.
Proof.
  intros H HO HI [|n]; [assumption|].
  apply positive_binary_induction; try assumption.
  exact (HI 0 H).
Qed.

#[global] Hint Resolve Nat.lt_0_succ : core.
Arguments Pos.size_nat : simpl nomatch.
Arguments Pos.of_nat : simpl nomatch.

Lemma Pos_of_nat_xO n : n <> 0 -> Pos.of_nat (2 * n) = xO (Pos.of_nat n).
Proof. intros. now rewrite Nat2Pos.inj_mul. Qed.

Lemma Pos_of_nat_xI n : n <> 0 -> Pos.of_nat (S (2 * n)) = xI (Pos.of_nat n).
Proof. intros. now rewrite Nat2Pos.inj_succ, Nat2Pos.inj_mul by lia. Qed.

Lemma size_log n : S (Nat.log2 n) = Pos.size_nat (Pos.of_nat n).
Proof.
  induction n using binary_induction; [easy| |].
  all: destruct n; [easy|].
  2: setoid_rewrite <-Nat.add_1_r at 2.
  all: rewrite ?Nat.log2_double, ?Nat.log2_succ_double by easy.
  all: rewrite ?Pos_of_nat_xO, ?Pos_of_nat_xI by easy; simpl; now f_equal.
Qed.
