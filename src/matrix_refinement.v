(* Copy of coqeal seqmx *)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq ssralg.
From mathcomp Require Import path choice fintype tuple finset bigop matrix.

From CoqEAL Require Import hrel param refinements trivial_seq.

From stdpp Require prelude options.

From thesis Require lib. (* want to import? *)
From thesis Require index vector matrix.
From thesis Require Import index_refinement.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory.
Import Refinements.Op.

Local Open Scope ring_scope.

Declare Scope hetero_computable_scope.
Delimit Scope hetero_computable_scope with HC.

Section classes.

Class hzero_of I B := hzero_op : forall m n : I, B m n.
(* Local Notation "0" := hzero_op : hetero_computable_scope. *)

Class hmul_of I B := hmul_op : forall m n p : I, B m n -> B n p -> B m p.
(* Local Notation "*m%HC" := hmul_op. *)
(* Local Notation "x *m y" := (hmul_op x y) : hetero_computable_scope. *)

(* Class hopp I B := hopp_op : forall m n : I, B m n -> B m n. *)
(* Local Notation "- x" := (hopp_op x) : hetero_computable_scope. *)

Class heq_of I B := heq_op : forall m n : I, B m n -> B m n -> bool.
(* Local Notation "x == y" := (heq_op x y) : hetero_computable_scope. *)

Local Open Scope nat_scope.

(* TODO: Remove this and get a better way for extracting elements *)
Class top_left_of A B := top_left_op : A -> B.

Class usubmx_of B :=
  usubmx_op : forall (m1 m2 n : nat), B (m1 + m2) n -> B m1 n.
Class dsubmx_of B :=
  dsubmx_op : forall (m1 m2 n : nat), B (m1 + m2) n -> B m2 n.
Class lsubmx_of B :=
  lsubmx_op : forall (m n1 n2 : nat), B m (n1 + n2) -> B m n1.
Class rsubmx_of B :=
  rsubmx_op : forall (m n1 n2 : nat), B m (n1 + n2) -> B m n2.
Class ulsubmx_of B :=
  ulsubmx_op : forall (m1 m2 n1 n2 : nat), B (m1 + m2) (n1 + n2) -> B m1 n1.
Class ursubmx_of B :=
  ursubmx_op : forall (m1 m2 n1 n2 : nat), B (m1 + m2) (n1 + n2) -> B m1 n2.
Class dlsubmx_of B :=
  dlsubmx_op : forall (m1 m2 n1 n2 : nat), B (m1 + m2) (n1 + n2) -> B m2 n1.
Class drsubmx_of B :=
  drsubmx_op : forall (m1 m2 n1 n2 : nat), B (m1 + m2) (n1 + n2) -> B m2 n2.
Class row_mx_of B :=
  row_mx_op : forall (m n1 n2 : nat), B m n1 -> B m n2 -> B m (n1 + n2).
Class col_mx_of B :=
  col_mx_op : forall (m1 m2 n : nat), B m1 n -> B m2 n -> B (m1 + m2) n.
Class block_mx_of B :=
  block_mx_op : forall (m1 m2 n1 n2 : nat),
    B m1 n1 -> B m1 n2 -> B m2 n1 -> B m2 n2 -> B (m1 + m2) (n1 + n2).

Class const_mx_of A B := const_mx_op : forall (m n : nat), A -> B m n.

Class map_mx_of A B C D :=
  map_mx_op : (A -> B) -> C -> D.

Class enum_of (I : nat -> Type) := (* Is this a good idea? *)
  enum_op : forall (n : nat), vector.vector (I n) n.

Class nat_of (I : nat -> Type) := (* and this? *)
  nat_op n : I n -> nat.

End classes.

Global Typeclasses Transparent hzero_of hmul_of heq_of top_left_of usubmx_of dsubmx_of
            lsubmx_of rsubmx_of ulsubmx_of ursubmx_of dlsubmx_of drsubmx_of
            row_mx_of col_mx_of block_mx_of const_mx_of map_mx_of.

Notation "0" := hzero_op : hetero_computable_scope.
(* Notation "- x" := (hopp_op x) : hetero_computable_scope. *)
Notation "x == y" := (heq_op x y) : hetero_computable_scope.
Notation "*m%HC" := hmul_op.
Notation "x *m y" := (hmul_op x y) : hetero_computable_scope.

Section extra_seq.

Variables (T1 T2 T3 : Type) (f : T1 -> T2 -> T3).

Fixpoint zipwith (s1 : seq T1) (s2 : seq T2) :=
  if s1 is x1 :: s1' then
    if s2 is x2 :: s2' then f x1 x2 :: zipwith s1' s2' else [::]
  else [::].

Lemma zipwithE s1 s2 : zipwith s1 s2 = [seq f x.1 x.2 | x <- zip s1 s2].
Proof. by elim: s1 s2 => [|x1 s1 ihs1] [|x2 s2] //=; rewrite ihs1. Qed.

Fixpoint foldl2 (f : T3 -> T1 -> T2 -> T3) z (s : seq T1) (t : seq T2) :=
  if s is x :: s' then
    if t is y :: t' then foldl2 f (f z x y) s' t' else z
  else z.

End extra_seq.

Parametricity zipwith.
Parametricity foldl2.

Section seqmx_op.

Variable A B : Type.
Variable I : nat -> Type.

(* Definition seqmx {A} := seq (seq A). *)
(* Definition hseqmx {A} := fun (_ _ : nat) => @seqmx A. *)
Definition hseqmx {A} := (fun m n => @matrix.matrix A m n).

(* Local Notation vmap := (base.fmap (FMap:=vector.vector_fmap)). *)
Local Notation vmap := vector.vector_fmap.

Context `{base.Inhabited A}. (* good idea? *)
Context `{zero_of A, one_of A, add_of A, opp_of A, mul_of A, eq_of A}.
Context `{forall n, implem_of 'I_n (I n)}.
Context `{forall n, eq_of (I n)}. (* good? *)
Context `{enum_of I, nat_of I}.

Definition seqmx_of_fun m n (f : I m -> I n -> A) : hseqmx m n :=
  vmap (fun i => vmap (f i) (enum_op n)) (enum_op m).

(* Definition mkseqmx_ord m n (f : 'I_m -> 'I_n -> A) : seqmx :=
  let enum_n := ord_enum_eq n in
  map (fun i => map (f i) enum_n) (ord_enum_eq m). *)

Global Instance const_seqmx : const_mx_of A hseqmx :=
  fun m n => matrix.mx_const.

Global Instance map_seqmx m n : map_mx_of A B (@hseqmx A m n) (@hseqmx B m n) :=
  @matrix.map_mx A B m n.

Definition zipwith_seqmx := @matrix.mx_zipwith A.

Global Instance seqmx0 : hzero_of hseqmx :=
  fun m n => const_seqmx m n 0%C.

Local Infix "!#" := vector.vector_lookup_total
	(at level 10). (* todo level? *)
