Require Import ZArith Lia.

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div seq.
From mathcomp Require Import path choice fintype tuple finset bigop order.
From mathcomp Require Import ssralg zmodp ssrnum ssrint intdiv.

From CoqEAL Require Import hrel param refinements pos binnat binint.

From thesis Require Import bin_nat_aux nat_gcd_alt_modified.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory Order.Theory Num.Theory Refinements Op.

Class gcd_of A := gcd_op : A -> A -> A.
#[export] Hint Mode gcd_of + : typeclass_instances.

Class sgn_of A := sgn_op : A -> A.
#[export] Hint Mode sgn_of + : typeclass_instances.

Class abs_of A B := abs_op : A -> B.
#[export] Hint Mode abs_of + - : typeclass_instances. (* -? *)

Class ediv_of A := ediv_op : A -> A -> A * A.
#[export] Hint Mode ediv_of + : typeclass_instances.

Lemma nat_of_pos_bin p : nat_of_bin (N.pos p) = nat_of_pos p.
Proof. by []. Qed.

Lemma N_to_natE : N.to_nat =1 nat_of_bin.
Proof. case=> [|p] //=. apply to_natE. Qed.

Lemma nat_of_pos_is_pos (p : positive) : 0 < nat_of_pos p.
Proof. elim: p => [p IHp|p IHp|] //=; by rewrite NatTrec.trecE double_gt0. Qed.

Definition my_pos_of_positive (a : positive) : pos :=
  (Sub (nat_of_pos a) (nat_of_pos_is_pos _)).

Section my_pos_of_positiveE.
Local Transparent pos_of_positive positive_of_pos.
Lemma my_pos_of_positiveE : pos_of_positive =1 my_pos_of_positive.
Proof.
  move=> p; rewrite /pos_of_positive /my_pos_of_positive.
  apply: val_inj=> /=; rewrite val_insubd to_nat_gt0.
  apply to_natE.
Qed.
End my_pos_of_positiveE.

Instance gcd_pos : gcd_of pos.
Proof.
  refine (fun m n => Sub (gcdn (val m) (val n)) _).
  abstract by rewrite gcdn_gt0 !valP.
Defined.

Lemma inj_divide a b : Nat.divide (Pos.to_nat a) (Pos.to_nat b) <-> Pos.divide a b.
Proof.
  split; intros [c H].
  - exists (Pos.of_nat c). apply Pos2Nat.inj.
    rewrite Pos2Nat.inj_mul Nat2Pos.id //. lia.
  - exists (Pos.to_nat c). lia.
Qed.

Lemma nat_of_bin_inj a b : nat_of_bin a = nat_of_bin b -> a = b.
Proof. move=> H. by rewrite -[LHS]nat_of_binK -[RHS]nat_of_binK H. Qed.

Lemma nat_of_divide_bin a b : Nat.divide (nat_of_bin a) (nat_of_bin b) <-> N.divide a b.
Proof.
  split; intros [c H].
  - exists (bin_of_nat c). apply: nat_of_bin_inj.
    by rewrite nat_of_mul_bin bin_of_natK.
  - exists (nat_of_bin c). by rewrite -[RHS]nat_of_mul_bin -H.
Qed.

Lemma divideP a b : reflect (Nat.divide a b) (a %| b).
Proof. apply: (iffP idP); by setoid_rewrite (Bool.reflect_iff _ _ dvdnP). Qed.

Instance Rpos_gcd : refines (Rpos ==> Rpos ==> Rpos) gcd_pos Pos.gcd.
Proof.
  rewrite refinesE => _ x <- _ y <-. apply: val_inj=> /=.
  rewrite !my_pos_of_positiveE /= -!to_natE.
  symmetry; apply: gcdn_def.
  1,2:apply/divideP; apply inj_divide. apply Pos.gcd_divide_l. apply Pos.gcd_divide_r.
  move=> [|d'].
  rewrite dvd0n; move=> /eqP H; exfalso. eapply (Nat.lt_neq _ _ (Pos2Nat.is_pos _)); eauto.
  setoid_rewrite <- (Bool.reflect_iff _ _ (divideP _ _)).
  rewrite <- (Nat2Pos.id d'.+1), !inj_divide; last by [].
  apply Pos.gcd_greatest.
Qed.

Section binnat_gcd.

#[export] Instance gcd_N : gcd_of N := N.gcd.

Instance Rnat_gcd : refines (Rnat ==> Rnat ==> Rnat) gcdn gcd_N.
Proof.
  rewrite refinesE => _ x <- _ y <-; rewrite /Rnat /fun_hrel.
  symmetry; apply: gcdn_def.
  1,2:apply/divideP; apply nat_of_divide_bin. apply N.gcd_divide_l. apply N.gcd_divide_r.
  move=> d /divideP Hx /divideP Hy; apply/divideP; move: Hx Hy.
  do 3 (rewrite -[X in Nat.divide X _]bin_of_natK nat_of_divide_bin; try move=> ?). (* eh *)
  by apply: N.gcd_greatest.
Qed.

Definition pos_nat_ediv (a b : pos) : nat * nat :=
  edivn (val a) (val b).

Instance Rposnat_ediv : refines (Rpos ==> Rpos ==> prod_R Rnat Rnat) pos_nat_ediv positive_N_div_eucl.
Proof.
  rewrite refinesE => _ x <- _ y <-; rewrite /Rnat /Rpos /fun_hrel.
  apply: prod_RI; rewrite /prod_hrel.
  rewrite /pos_nat_ediv !my_pos_of_positiveE /=.
  have:= positive_N_div_eucl_div_mod x y.
  case: positive_N_div_eucl=> [q r] /= [/(f_equal nat_of_bin) E Eb].
  rewrite nat_of_add_bin nat_of_mul_bin !nat_of_pos_bin mulnC in E.
  rewrite {}E edivn_eq //=.
  by move/Rnat_ltP: Eb.
Qed.

Check Rnat_div_eucl.

End binnat_gcd.

Section binint_gcd.

Variable N P : Type.

Local Notation Z := (binint.Z N P).

Context `{gcd_of N, gcd_of P}.
Context `{cast_of P N}.

#[export] Instance gcdZ : gcd_of Z := fun a b => @Zpos N P
  match a, b with
  | Zpos na, Zpos nb => gcd_op na nb
  | Zneg pa, Zpos nb => gcd_op (cast_op pa) nb
  | Zpos na, Zneg pb => gcd_op na (cast_op pb)
  | Zneg pa, Zneg pb => cast_op (gcd_op pa pb)
  end.

Context `{zero_of N, eq_of N}.
Context `{zero_of Z, one_of Z, opp_of Z}. (* ? *)

#[export] Instance sgn_Z : sgn_of Z := fun a =>
  match a with
  | Zpos n => if (n == 0)%C then 0%C else 1%C
  | Zneg _ => (- (1))%C
  end.

#[export] Instance abs_Z : abs_of Z N := fun a =>
  match a with
  | Zpos n => n
  | Zneg p => cast p
  end.

Context `{add_of Z, sub_of Z, mul_of Z}.
Context `{edivNN : ediv_of N}. (* remove name somehow *)
Context `{sub_of N, one_of N}.

#[export] Instance ediv_Z : ediv_of Z := fun a b =>
  match a, b with
  | Zpos a', Zpos b' =>
    if (b' == 0)%C then (0, a)%C else
    let (q,r) := edivNN a' b' in (cast q, cast r)%C
  | Zneg a', Zpos b' =>
    if (b' == 0)%C then (0, a)%C else
    let (q,r) := edivNN ((cast a') - 1)%C b' in
    (- (1 + cast q), b - 1 - cast r)%C
  | Zpos a', Zneg b' =>
    let (q,r) := edivNN a' (cast b') in (- cast q, cast r)%C
  | Zneg a', Zneg b' =>
    let (q,r) := edivNN ((cast a') - 1)%C (cast b') in 
    (1 + cast q, -b - 1 - cast r)%C
  end.

Fixpoint egcd_ding (n : nat) : N -> N -> Z * Z * N := fun a b =>
  match n with
  | O => (0, 0, 0)%C
  | S n =>
    if (a == 0)%C then (0, 1, b)%C else
    let (q,r) := ediv_op b a in
    let '(x,y,g) := egcd_ding n r a in
    (y - (cast q)*x, x, g)%C
  end.

End binint_gcd.

Definition edivz (a b : int) : int * int := (a %/ b, a %% b)%Z.

Instance ediv_N : ediv_of N := N.div_eucl.
(* 
Compute let (a,b) := (0 : N, 0 : N) in let '(x,y,g) := egcd_ding (N:=N) (P:=positive) 10 a b in (x * (cast a) + y * (cast b) == cast g)%C.
Compute let (a,b) := (1 : N, 0 : N) in let '(x,y,g) := egcd_ding (N:=N) (P:=positive) 10 a b in (x * (cast a) + y * (cast b) == cast g)%C.
Compute let (a,b) := (0 : N, 1 : N) in let '(x,y,g) := egcd_ding (N:=N) (P:=positive) 10 a b in (x * (cast a) + y * (cast b) == cast g)%C.
Compute let (a,b) := (1 : N, 1 : N) in let '(x,y,g) := egcd_ding (N:=N) (P:=positive) 10 a b in (x * (cast a) + y * (cast b) == cast g)%C.
Compute let (a,b) := (2 : N, 3 : N) in let '(x,y,g) := egcd_ding (N:=N) (P:=positive) 10 a b in (x * (cast a) + y * (cast b) == cast g)%C.
Compute let (a,b) := (12 : N, 15 : N) in let '(x,y,g) := egcd_ding (N:=N) (P:=positive) 10 a b in (x * (cast a) + y * (cast b) == cast g)%C. *)

Section binint_gcd_theory.

Notation Znp := (Z nat pos).

Local Instance zero_nat : zero_of nat := 0%N.
Local Instance one_nat  : one_of nat  := 1%N.
Local Instance add_nat  : add_of nat  := addn.
Local Instance sub_nat  : sub_of nat  := subn.
Local Instance mul_nat  : mul_of nat  := muln.
Local Instance exp_nat  : exp_of nat nat := expn.
Local Instance mod_nat  : mod_of nat := modn.
Local Instance leq_nat  : leq_of nat  := ssrnat.leq.
Local Instance lt_nat   : lt_of nat  := ssrnat.ltn.
Local Instance eq_nat   : eq_of nat   := eqtype.eq_op.

Local Instance gcd_nat : gcd_of nat := gcdn.
Local Instance ediv_nat : ediv_of nat := edivn.

Lemma gcdn_gcdz a b : Posz (gcdn a b) = gcdz (Posz a) (Posz b).
Proof. done. Qed.

Instance Rint_gcd : refines (Rint ==> Rint ==> Rint) gcdz (gcd_op (gcd_of:=(gcdZ (P:=pos)))). (* clean up *)
Proof.
  rewrite refinesE=> _ x <- _ y <-; rewrite /Rint /fun_hrel.
  case: x; case: y => ?? //=. all: rewrite ?gcdzN ?gcdNz //.
Qed.

Instance Rint_sgn : refines (Rint ==> Rint) (sgz (R:=ssrint_int__canonical__Num_NumDomain)) sgn_op.
Proof.
  rewrite refinesE=> _ x <-; rewrite /Rint /fun_hrel.
  case: x=> [[|n]|p] //=.
  rewrite val_insubd sgzN /sgz /=; f_equal.
  case E: (val_of_pos p)=> //=.
  have := valP p=> /=; by rewrite E.
Qed.

Instance Rint_abs : refines (Rint ==> nat_R) absz abs_op. (* nat_R ? *)
Proof.
  rewrite refinesE=> _ x <-; rewrite /Rint /fun_hrel.
  case: x=> [n|p] /=; rewrite ?abszN; apply nat_Rxx.
Qed.

Instance Rint_ediv : refines (Rint ==> Rint ==> prod_R Rint Rint) edivz ediv_op.
  rewrite refinesE => _ x <- _ y <-; rewrite /Rint /fun_hrel.
  apply: prod_RI; rewrite /prod_hrel /ediv_op.
  case: x y=> [x|[[|x] Hx]] [[|y]|[[|y] Hy]] //=; rewrite /ediv_nat.
  all: repeat progress rewrite /cast /cast_op /cast_NZ /cast_pos_nat /cast_nat_pos
    /opp_op /oppZ /zero_op /zeroZ /zero_nat /one_op /one_nat /oneZ /add_op /addZ /add_nat
      /sub_op /subZ /sub_nat /eq_op /eq_nat /leq_op /leq_nat /=. (* extend simpC *)
  1,4: by rewrite divz0 modz0.
  all: rewrite edivn_def /=; split; clear.
  all: rewrite ?(divzN, modzN) ?(divz_nat, modz_nat) ?(divNz_nat, modNz_nat) //=.
  all: rewrite ?subSS ?subn0 ?opprK -?predn_int //=.
  1: case E: (x %/ y.+1)=> //=.
  1,2: by rewrite val_insubd.
  all: case: leqP=> /= E; last rewrite val_insubd subn_gt0 E.
  all: rewrite -subzn; last by done + apply ltnW.
  all: by done + by rewrite opprB.
Qed.

Lemma nat_of_succ_bin (n : N) : nat_of_bin (N.succ n) = (nat_of_bin n).+1.
Proof. case: n=> //= p. by rewrite /N.succ nat_of_pos_bin nat_of_succ_pos. Qed.

Tactic Notation "case_match" "eqn" ":" ident(Hd) :=
  match goal with
  | H : context [ match ?x with _ => _ end ] |- _ => case Hd: x H=> H
  | |- context [ match ?x with _ => _ end ] => case Hd: x
  end.
Ltac case_match :=
  let H := fresh in case_match eqn:H.

Lemma dingE (n : nat) (a b : N) :
  nat_of_bin (egcd_ding (P:=positive) n a b).2 = mygcdn n (nat_of_bin a) (nat_of_bin b).
Proof.
  rewrite /egcd_ding /mygcdn.
  elim: n a b=> // n IHn a b.
  elim/N.case_analysis: a=> //= a; simpC.
    have ->: N.succ a == 0%C = false; first by apply/eqP; apply N.neq_succ_0.
  rewrite {1}nat_of_succ_bin.
  rewrite modn_def.
  rewrite {1}/ediv_op /ediv_N.
  have:= Rnat_div_eucl=> /refinesP /(_ b b erefl (N.succ a) (N.succ a) erefl) /prod_RE [_ <-]. (* yuk *) (* or -> ?*)
  rewrite -{}IHn.
  case: (N.div_eucl b (N.succ a))=> [q r] /=.
  by repeat case_match.
Qed.

Instance ding : refines (nat_R ==> Rnat ==> Rnat ==> Rnat) mygcdn (fun n a b => (egcd_ding (P:=positive) n a b).2).
Proof.
  rewrite refinesE => _ n /nat_R_eq -> _ a <- _ b <-; rewrite /Rnat /fun_hrel.
  apply dingE.
Qed.

Lemma specZE : (@spec Znp int (@specZ nat pos cast_pos_nat spec_nat)) =1 int_of_Z.
Proof. done. Qed.

Lemma eding n a b : let '(x,y,g) := egcd_ding (N:=nat) (P:=pos) n a b in (int_of_Z x * a + (int_of_Z y) * b)%R = g.
elim: n a b=> //= n IHn a b.
case: a=> [|a] /=.
  by rewrite mul0r add0r mul1r.
rewrite /ediv_op /ediv_nat.
case: edivnP=> q r /= -> Hr.
specialize (IHn r a.+1).
destruct (egcd_ding n r a.+1) as [[x y] g].
rewrite -{}IHn.

erewrite (refinesP Rint_sub).
2:{ apply refinesP. apply Rint_specZ_r. } (* automate these? *)
2:{ apply refinesP. refines_apply1. refines_apply1. apply Rint_mul. refines_apply1. apply Rint_Posz. rewrite refinesE. apply erefl. apply Rint_specZ_r. }
rewrite PoszD PoszM !specZE.
move: (int_of_Z y) (Posz q) (int_of_Z x) (Posz a.+1) (Posz r)=> Y Q X A R. (* loses Hr, dont need it *)
rewrite mulrC mulrBr mulrA mulrDr mulrA addrA.
rewrite -[(A * Y - A * Q * X + X * Q * A)%R]addrA [(- (A * Q * X) + X * Q * A)%R]addrC.
(* grrr *)
Admitted.

(* We have shown that egcd_ding gives bezout coefficients just like egcdz does, but these are not unique: *)
Compute let (a,b) := (2 : N, 3 : N) in egcd_ding (N:=N) (P:=positive) 10 a b.
Compute egcdz 2 3.

Open Scope ring_scope.

Lemma mul_eq_y_implies_x_eq_1 (x y : int) : y != 0 -> x * y = y -> x = 1.
Proof.
  move=> y_neq0 /(f_equal (fun x => x %/ y)%Z).
  by rewrite divzz y_neq0 /= mulzK.
Qed.

Lemma bezout_coprime (a b x y : int) : x != 0 -> a != 0 \/ b != 0 -> x * a + y * b = gcdz a b -> gcdz x y = 1.
Proof.
  move=> Hx /nandP Hab.
  have /(_ a b) /dvdzP [x' {1}->] := dvdz_gcdl.
  have /(_ a b) /dvdzP [y' {2}->] := dvdz_gcdr.
  rewrite !mulrA -mulrDl [x * x']mulrC [y * y']mulrC.
  move=> H.
  have:= mul_eq_y_implies_x_eq_1 _ H.
  rewrite gcdz_eq0 Hab=> /(_ isT) /eqP {}H; apply/eqP; move: H.
  apply: contraLR=> /negPf Hne1.
  have /(_ x y) /dvdzP [x'' {1}->] := dvdz_gcdl.
  have /(_ x y) /dvdzP [y'' {2}->] := dvdz_gcdr.
  rewrite !mulrA -mulrDl.
  rewrite intUnitRing.mulzn_eq1. rewrite /gcdz eqz_nat in Hne1.
  by rewrite Hne1 Bool.andb_false_r.
Qed.

Fixpoint egcd_ding_nat (n : nat) : nat -> nat -> int * int * nat := fun a b =>
  match n with
  | O => (0, 0, 0)
  | S n =>
    if (a == 0) then (0, 1, b) else
    let (q,r) := edivn b a in
    let '(x,y,g) := egcd_ding_nat n r a in
    (y - (Posz q)*x, x, g)
  end.

Compute egcdn_rec 3 2 10 [:: 4; 1; 3; 2].


Lemma egcd_dingP (m n : int) :
  let '(u,v,g) := egcd_ding (N:=nat) (P:=pos) (implem m) (implem n) in
  egcdz_spec m n (int_of_Z u, int_of_Z v).

End binint_gcd_theory.

Locate egcdz.
Print egcdn.

Local Instance ediv_N : ediv_of N := N.div_eucl. (* export *)

(* Now for Bezout *)

(* Section binint_egcd.
Check egcdz : int -> int -> int * int.
About egcdn.
(* dvdring.v defines "egcdr". *)

Compute Znumtheory.extgcd 12 15.
About Znumtheory.extgcd_rec.
Print Znumtheory.extgcd. *)

(* Section binint_egcd.

Variable N P : Type.

Local Notation Z := (binint.Z N P).


Arguments mygcdn : simpl nomatch.

Definition mygcd_bound (a : nat) :=
  match a with
    | O => S O
    | _ => let n := S (Nat.log2 a) in (n+n)%nat
  end.

Definition mygcd_alt a b := mygcdn (S (mygcd_bound a)) a b. *)

(* Fixpoint Zgcd_ex_fuel (n : nat) (a b : Z) : Z * (Z * Z) :=
	match n with
	| O => (0, (0,0))%Z (* Is 1 in the original, but 0 helps with bezout... *)
	| S n => match a with
		| Z0 => (Z.abs b, (0, Z.sgn b))%Z
		| Zpos _ =>
			let (q,r) := Z.div_eucl b a in
			let '(g, (x,y)) := Zgcd_ex_fuel n r a in
			(g, (y - q*x, x))%Z
		| Zneg a => let a := (Zpos a) in
			let (q,r) := Z.div_eucl b a in
			let '(g, (x,y)) := Zgcd_ex_fuel n r a in
			(g, (-(y - q*x), x))%Z
		end
	end.

Definition Zgcd_ex_bound (a : Z) :=
	match a with
		| Z0 => S O
		| Zpos p => let n := Pos.size_nat p in (n+n)%nat
		| Zneg p => let n := Pos.size_nat p in (n+n)%nat
	end. *)

(* Module gcd.
	From thesis Require Import Zgcd_alt_modified.

	Lemma Zgcd_ex_bezout n a b g x y : Zgcd_ex_fuel n a b = (g, (x,y)) → g = x*a + y*b.
		induction n in a,b,g,x,y |- *; simpl.
		1: intros ?; by simplify_eq.
		case_match.
		{ intros ?; simplify_eq. by rewrite Z.add_0_l, Z.mul_comm, Z.sgn_abs. }
		all: repeat case_match; intros ?; simplify_eq.
		all: apply IHn in H1; subst.
		all: pose proof (Z.div_eucl_eq b (Z.pos p) ltac:(done)); rewrite H0 in H; subst; lia.
	Qed.

	Lemma Zgcd_alt_ex n a b : (Zgcd_ex_fuel n a b).1 = Zgcdn n a b.
	Proof.
		induction n in a,b |- *; simpl; [done|].
		unfold Z.modulo.
		destruct a; simpl; [done|rewrite <- IHn..].
		all: repeat case_match; simpl in *.
	Qed.
End gcd. *)

Context `{gcd_of N, gcd_of P}.
Context `{cast_of P N}.


Instance ding : refines (Rint ==> Rint ==> prod_R Rint Rint) egcdz (fun a b => fst (Znumtheory.extgcd a b)).

