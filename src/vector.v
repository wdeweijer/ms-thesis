From stdpp Require Import prelude options.
From thesis Require Import lib index.

Section vector.
	Variable A : Type.
	Variable n : nat.

	Structure vector := Vector {
		list_of_vector :> list A ;
		#[canonical=no] vector_length : length list_of_vector = n ;
		(* TODO: Rename this to length_vector? *)
	}.
	Global Arguments Vector l length_l : rename.

	Implicit Type v : vector.

	Lemma vector_eq v1 v2 : v1 =@{list A} v2 → v1 = v2.
	Proof.
		destruct v1 as [? H1], v2 as [? H2]; simpl; intros He.
		subst; f_equal. apply eq_pi, _.
	Qed.

	Global Instance list_of_vector_inj : Inj (=) (=) list_of_vector.
	Proof. intros ??; apply vector_eq. Qed.

	Global Instance vector_eq_dec `{EqDecision A} : EqDecision vector.
	Proof.
		intros [l1 H1] [l2 H2]. destruct (list_eq_dec l1 l2); [left | right].
		- by apply vector_eq.
		- intro; simplify_eq.
	Qed.

	Definition stdvector_to_vector (v : vector.vec A n) : vector :=
		Vector (length_vec_to_list v).

	Definition vector_to_stdvector (v : vector) : vector.vec A n.
		destruct v as [l <-].
		apply vector.list_to_vec.
	Defined.

	Lemma vector_to_stdvector_to_list (v : vector) : vec_to_list (vector_to_stdvector v) = list_of_vector v.
	Proof. destruct v as [l E]; simpl; subst. apply vec_to_list_to_vec. Qed.

	Lemma stdvector_to_vector_to_list (v : vector.vec A n) : list_of_vector (stdvector_to_vector v) = vec_to_list v.
	Proof. reflexivity. Qed.

	Lemma vector_to_stdvector_to_vector : Cancel (=) stdvector_to_vector vector_to_stdvector.
	Proof. intros v. apply vector_eq. apply vector_to_stdvector_to_list. Qed.

	Lemma stdvector_to_vector_to_stdvector : Cancel (=) vector_to_stdvector stdvector_to_vector.
	Proof. intros v. apply vec_to_list_inj2. apply vector_to_stdvector_to_list. Qed.

	(* Lemma vnth_default v : 'I_n → A.
	Proof.
		rewrite <-(vector_length v).
		destruct (list_of_vector v); simpl; [|done].
		intros []. by edestruct Nat.nlt_0_r.
	Defined. *)

	(* Definition vnth v i := nth i v (vnth_default v i). *)

	(* Global Instance vector_lookup_total `{Inhabited A} : LookupTotal 'I_n A vector :=
		fun i v => (list_of_vector v) !# (nat_of_index i). *)

	(* Coercion vector_lookup_total : vector >-> Funclass. *)

	(* Definition mk_vector (l : list A) (H : length l =? n = true) : vector :=
		Vector l ltac:(by apply Nat.eqb_eq). *)

	Definition phantom_vector {v} (H : @phantom (list A) v) := v.

	Definition vectorize v mkT : vector :=
		mkT (let '@Vector _ tP := v return length v = n in tP).

End vector.

Global Arguments list_of_vector {A n}.
Global Hint Resolve vector_length : core.

(* Why not use 
	From Coq Require Import ssreflect.
	Compute [the (vector nat 3) of [1;2;3] by list_of_vector].
	Compute [the (vector _ _) of [1;2;3]].
*)
Notation "[ 'vec' 'of' v ]" := (phantom_vector (Phantom v))
	(at level 0, format "[ 'vec'  'of'  v ]").
Notation "[ 'vector2' 'of' v ]" := (vectorize (@Vector _ _ v))
	(at level 0, format "[ 'vector2'  'of'  v ]").
Notation "[ 'vec' x1 ; .. ; xn ]" := [vec of x1 :: .. [xn] ..]
	(at level 0, format "[ 'vec' '['  x1 ; '/'  .. ; '/'  xn ']' ]").
Notation "[ 'vec' ]" := [vec of []]
	(at level 0, format "[ 'vec' ]").

Canonical nil_vector {T} : vector T 0 := Vector length_nil.

Canonical cons_vector n T (x : T) (v : vector T n) : vector T (S n).
Proof. apply Vector with (x :: v); simpl. by f_equal. Defined.

Section destruct_vector.
	Context {A : Type} {n : nat}.
	Implicit Type v : vector A (S n).

	Definition destruct_vector v : A * vector A n.
	Proof.
		destruct v as [[|a l'] H]; [done|].
		injection H as H.
		split; [exact a | eapply Vector, H].
	Defined.

	Definition hd_vector v : A := (destruct_vector v).1.

	Canonical tail_vector v : vector A n.
	Proof. apply Vector with (tail v). by rewrite tail_length, vector_length. Defined.

	Lemma vector_S v : v = cons_vector (hd_vector v) (tail_vector v).
	Proof. destruct v as [[|a l'] H]; [done|]. by apply vector_eq. Qed.

	Lemma tail_vector_destruct v : tail_vector v = (destruct_vector v).2.
	Proof. destruct v as [[|a l'] H]; [done|]. by apply vector_eq. Qed.
End destruct_vector.

Lemma vector_0 A (v : vector A 0) : v = nil_vector.
Proof. by apply vector_eq, nil_length_inv. Qed.

(* Since vector is a refinement type, this no longer holds judgmentally. *)
Lemma vector_tail_cons A n (a : A) (v : vector A n) : tail_vector (cons_vector a v) = v.
Proof. by apply vector_eq. Qed.

(* TODO: profile the difference between this and vector_lookup_total *)
Definition vector_lookup A : ∀ n, LookupTotal 'I_n A (vector A n) :=
	fix go n :=
	match n with
	| 0 => λ devil _, False_rect A (index_0_false devil)
	| S n => λ i v, match destruct_index i with
		| inl _ => hd_vector v
		| inr p => go n (`p) (tail_vector v)
		end
	end.

Arguments vector_lookup _ _ _ _ : simpl nomatch.
Notation "v !# i" := (vector_lookup i v) (at level 10). (* todo level? *)

Lemma cons_vector_lookup_0 A n (a : A) (v : vector A n) :
	(cons_vector a v) !# 0 = a.
Proof. done. Qed.

(* Since vector is a refinement type, this no longer holds definitionally. *)
Lemma cons_vector_lookup_S A n (a : A) (v : vector A n) i :
	(cons_vector a v) !# (S_index i) = v !# i.
Proof.
	unfold vector_lookup at 1. cbn.
	f_equal.
	{ by apply index_eq. }
	apply vector_tail_cons.
Qed.

Definition vector_lookup_total A n `{Inhabited A} (v : vector A n) : 'I_n → A := λ i,
	(list_of_vector v) !!! (nat_of_index i).

Definition vector_lookup_nat_total A n `{Inhabited A} (v : vector A n) : nat → A := λ n,
	(list_of_vector v) !!! n.

Lemma vector_lookup_lookup_total A n `{Inhabited A} (v : vector A n) (i : 'I_n) :
	v !# i = vector_lookup_total v i.
Proof.
	induction i using index_case; rewrite (vector_S v); [done|].
	rewrite cons_vector_lookup_S.
	apply IHi.
Qed.

Lemma vector_lookup_list_lookup_1 A n (v : vector A n) (i : 'I_n) :
	(list_of_vector v) !! (nat_of_index i) = Some (v !# i).
Proof.
	induction i using index_case; rewrite (vector_S v); [done|].
	rewrite cons_vector_lookup_S.
	apply IHi.
Qed.

Lemma vector_lookup_is_Some A n (v : vector A n) (i : 'I_n) :
	is_Some (list_of_vector v !! nat_of_index i).
Proof. eexists. apply vector_lookup_list_lookup_1. Qed.

Lemma vector_lookup_list_lookup A n (v : vector A n) (i : 'I_n) a :
	v !# i = a ↔ (list_of_vector v) !! (nat_of_index i) = Some a.
Proof. rewrite vector_lookup_list_lookup_1; naive_solver. Qed.

Lemma vector_lookup_list_lookup_2 (A : Type) (n : nat) (v : vector A n) (i : 'I_n) (a : A) :
	list_of_vector v !! nat_of_index i = Some a →
	v !# i = a.
Proof. apply vector_lookup_list_lookup. Qed.

Canonical app_vector {A n m} (v : vector A n) (w : vector A m) : vector A (n + m).
Proof. apply Vector with (v ++ w). by rewrite length_app; f_equal. Defined.

Canonical drop_vector A n m (v : vector A (n + m)) : vector A m.
Proof. apply Vector with (drop n v). rewrite length_drop, vector_length. auto with arith. Defined.

Canonical take_vector A n m (v : vector A (n + m)) : vector A n.
Proof. apply Vector with (take n v). rewrite length_take, vector_length. auto with arith. Defined.

Canonical replicate_vector A (x : A) n : vector A n.
Proof. apply Vector with (replicate n x). apply length_replicate. Defined.

Canonical seq_vector (n start : nat) : vector nat n :=
	Vector (length_seq start n).

Canonical rotate_vector A n (k : nat) (v : vector A n) : vector A n.
Proof. apply Vector with (rotate k v). by rewrite length_rotate. Defined.

Canonical zip_with_vector A B C n (f : A → B → C)
	(v : vector A n) (w : vector B n) : vector C n.
Proof.
	apply Vector with (zip_with f v w).
	rewrite length_zip_with_r_eq; [done|].
	by trans n.
Defined.

Global Instance vector_insert_nat A n : Insert nat A (vector A n).
Proof.
	intros i a' v. apply Vector with (<[ i := a']> (list_of_vector v)).
	by rewrite length_insert.
Defined.
(* Canonical vector_insert_nat. *)

Global Instance vector_insert A n : Insert 'I_n A (vector A n).
Proof.
	intros i a' v. apply Vector with (<[ (nat_of_index i) := a']> (list_of_vector v)).
	by rewrite length_insert.
Defined.
Canonical vector_insert.

Global Instance vector_inhabited A `{Inhabited A} n : Inhabited (vector A n) :=
	populate [vec of replicate n inhabitant].

Global Instance vector_fmap {n} : FMap (λ A, vector A n).
Proof.
	intros A B f v.
	apply Vector with (f <$> (list_of_vector v)).
	by rewrite length_fmap.
Defined.

Canonical vector_fmap.

Lemma vector_fmap_compose A B C (f : A → B) (g : B → C) n (v : vector A n) :
	fmap (FMap:=vector_fmap) (g ∘ f) v =
		fmap (FMap:=vector_fmap) g (fmap (FMap:=vector_fmap) f v).
Proof. apply vector_eq; apply list_fmap_compose. Qed.

(* Check [vec of S <$> [1;2;3]].
Fail Check S <$> [vec 1;2;3]. (* sigh *)
Check @fmap _ vector_fmap _ _ S [vec 1;2;3]. *)

Definition vec_of_fun A n (f : nat → A) : vector A n :=
	[vec of f <$> (seq 0 n)].

(* Compute vec_of_fun 10 (λ n, n) : list nat. *)

Canonical index_enum_vector n : vector 'I_n n :=
	Vector (@length_index_enum n).

Definition vec_of_finfun A n (f : 'I_n → A) : vector A n :=
	[vec of f <$> (index_enum n)].

(* Compute vec_of_finfun (λ i, [vec of seq 3 5] !# i) : list nat.
Compute (vec_of_fun 5 (λ i, i + 3)) !# (ind_default0 2). *)

(* Lemma case_0 A (P : vector A 0 -> Prop) (H : P nil_vector) v : P v.
Proof. rewrite vector_0. exact H. Qed. *)

Section vector_induction.
	Variable (A : Type) (P : ∀ {n}, vector A n → Prop).
	Variable P0 : P nil_vector.
	Variable PS : ∀ a n (v : vector A n), P v → P (cons_vector a v).

	Lemma vector_induction : ∀ n (v : vector A n), P v.
	Proof using P0 PS.
		induction n; intros v.
		- rewrite vector_0. exact P0.
		- rewrite vector_S. apply PS. apply IHn.
	Qed.
(* 
		intros n [l Hl].
		induction l in n, Hl |- *; simpl in *; subst.
		- exact P0.
		- erewrite vector_eq.
			+ apply PS, IHl.
			+ done.
		Unshelve. done. (* ugly *)
	Defined. *)

End vector_induction.

Lemma vector_induction2 A (P : ∀ {n} (v1 v2 : vector A n), Prop) :
	P nil_vector nil_vector →
	(∀ a1 a2 n (v1 v2 : vector A n), P v1 v2 → P (cons_vector a1 v1) (cons_vector a2 v2)) →
	∀ n (v1 v2 : vector A n), P v1 v2.
Proof.
	intros H0 HS n v1 v2.
	induction v1 using vector_induction.
	- by rewrite vector_0.
	- rewrite vector_S. apply HS, IHv1.
Qed.

Section vector_recursion.
	Variable (A : Type) (B : nat → Type).
	Context `{∀ n, Inhabited (B n)}.
	Variable (Bnil : B 0).
	Variable (Bcons : A → ∀ n, list A → B n → B (S n)).

	Definition vector_recursion n (v : vector A n) : B n :=
		(fix go (n : nat) (v : list A) : B n :=
		match n, v with
		| S n, a :: v => Bcons a v (go n v)
		| 0, [] => Bnil
		| _, _ => inhabitant (* impossible *)
		end) n (list_of_vector v).

	Lemma vector_recursion_nil : vector_recursion nil_vector = Bnil.
	Proof. reflexivity. Defined.

	Lemma vector_recursion_cons (a : A) n (v : vector A n) :
		vector_recursion (cons_vector a v) = Bcons a (list_of_vector v) (vector_recursion v).
	Proof. reflexivity. Defined.
End vector_recursion.

Lemma vec_of_lookup A n (v : vector A n) :
	vec_of_finfun (λ i, v !# i) = v.
Proof.
	apply vector_eq; simpl.
	apply list_eq_same_length with (n:=n); [done..|]; intros i x y Hin.
	rewrite list_lookup_fmap. rewrite index_enum_lookup.
	rewrite (index_option_lt_eq Hin); simpl.
	intros [=<-] Hy.
	by rewrite vector_lookup_list_lookup.
Qed.

Lemma lookup_vec_of_finfun A n (f : 'I_n → A) i :
	(vec_of_finfun f) !# i = f i.
Proof.
	erewrite vector_lookup_list_lookup.
	simpl.
	destruct n; [done|].
	rewrite list_lookup_fmap.
	rewrite index_enum_lookup.
	by rewrite index_option_nat_of_index_id.
Qed.

Lemma lookup_vec_of_fun A n (f : nat → A) (i : 'I_n) :
	(vec_of_fun n f) !# i = f i.
Proof.
	rewrite vector_lookup_list_lookup; simpl.
	rewrite list_lookup_fmap.
	by rewrite lookup_seq_lt.
Qed.

Lemma vector_lookup_eq A n (v1 v2 : vector A n) :
	(∀ i, v1 !# i = v2 !# i) → v1 = v2.
Proof.
	intros H0.
	apply vector_eq.
	apply list_eq. intros i'.

	destruct (le_lt_dec n i').
	- rewrite 2 lookup_ge_None_2; by rewrite ?vector_length.
	- specialize (H0 (Index l)).
		(* by rewrite vector_lookup_list_lookup, <- vector_lookup_list_lookup_1 in H0. *)
		rewrite <- (nat_of_indexE l), 2 vector_lookup_list_lookup_1.
		by f_equal.
Qed.

Lemma vec_of_finfun_ext A n (f g : 'I_n → A) :
	(∀ i, f i = g i) → vec_of_finfun f = vec_of_finfun g.
Proof.
	intros E.
	apply vector_eq; simpl.
	by apply list_fmap_ext.
Qed.

Section vector_x.
	Context {A : Type}.
	Context {n : nat}.
	Implicit Types i : 'I_n.
	Implicit Types v : vector A n.

	Definition vector_x i1 i2 v :=
		let a1 := v !# i1 in let a2 := v !# i2 in
		<[ i1 := a2 ]> (<[ i2 := a1 ]> v). (* want {[ ; ]} *)

	Lemma vector_x_commute i1 i2 v : vector_x i1 i2 v = vector_x i2 i1 v.
	Proof.
		destruct (decide (i1 =@{nat} i2)).
		- by rewrite (index_eq e).
		- apply vector_eq; simpl.
		  by apply list_insert_commute.
	Qed.

	Lemma vector_x_alias i v : vector_x i i v = v.
	Proof.
		apply vector_eq; simpl.
		rewrite list_insert_insert, list_insert_id; [done|].
		apply vector_lookup_list_lookup_1.
	Qed.

End vector_x.

Definition vector_find {A} n (P : A → Prop) `{∀ x, Decision (P x)}
	(v : vector A n) : option ('I_n * A).
Proof.
	destruct (list_find P v) as [[i a]|] eqn:E;
		[apply Some|exact None]. (* monadic *)
	split; [|exact a]; apply Index with i.
	apply list_find_Some in E as [E _].
	erewrite <- vector_length; by eapply lookup_lt_Some.
Defined.
Global Arguments vector_find {A} {n} P {_} v.

Lemma vector_zip_with_lookup A B C n
	(v1 : vector A n) (v2 : vector B n) i (f : A → B → C) :
	[vec of zip_with f v1 v2] !# i = f (v1 !# i) (v2 !# i).
Proof.
	rewrite vector_lookup_list_lookup; simpl.
	apply lookup_zip_with_Some; do 2 eexists; split; [done|].
	split; apply vector_lookup_list_lookup_1.
Qed.

Lemma vector_fmap_lookup A B n (f : A → B) (v : vector A n) i :
	(fmap (M := λ A, _) f v) !# i = f (v !# i).
Proof.
	rewrite vector_lookup_list_lookup; simpl.
	rewrite list_lookup_fmap_Some; eexists; split; [|done].
	apply vector_lookup_list_lookup_1.
Qed.

Lemma app_vector_lookup_plus_bump A m n
	(v : vector A m) (w : vector A n) (i : 'I_m) :
	[vec of v ++ w] !# (index_plus_bump i) = v !# i.
Proof.
	rewrite vector_lookup_list_lookup; simpl.
	rewrite lookup_app_l by by rewrite vector_length.
	apply vector_lookup_list_lookup_1.
Qed.

Lemma app_vector_lookup_plus A m n
	(v : vector A m) (w : vector A n) (i : 'I_n) :
	[vec of v ++ w] !# (index_plus i) = w !# i.
Proof.
	rewrite vector_lookup_list_lookup; simpl.
	rewrite lookup_app_r; rewrite vector_length; [|auto with arith].
	rewrite <- vector_lookup_list_lookup_1.
	f_equal. eauto with arith.
Qed.

Lemma app_vector_lookup A m n
	(v : vector A m) (w : vector A n) (i : 'I_(m + n)) :
	(∃ j : 'I_m, [vec of v ++ w] !# i = v !# j ∧ i = index_plus_bump j) ∨
	(∃ j : 'I_n, [vec of v ++ w] !# i = w !# j ∧ i = index_plus j).
Proof.
	destruct (index_split i) as [[j ->] | [j ->]]; [left | right]; exists j;
		(split; [|done]);
		[apply app_vector_lookup_plus_bump | apply app_vector_lookup_plus].
Qed.

Lemma vector_take_drop A m n (v : vector A (m + n)) :
	v = app_vector (take_vector v) (drop_vector v).
Proof.
	apply vector_eq; simpl.
	symmetry; apply take_drop.
Qed.

Lemma take_vector_cons_vector A (a : A) n (v : vector A n) :
	@take_vector _ 1 n (cons_vector a v) = [vec a].
Proof. by apply vector_eq. Qed.

Lemma drop_vector_cons_vector A (a : A) n (v : vector A n) :
	@drop_vector _ 1 n (cons_vector a v) = v.
Proof. by apply vector_eq. Qed.

Canonical delete_vector A n (i : 'I_(S n)) (v : vector A (S n)) : vector A n.
Proof.
	apply Vector with (delete (nat_of_index i) (list_of_vector v)).
	rewrite length_delete by (apply lookup_lt_is_Some; by rewrite vector_length).
	rewrite vector_length; simpl. by rewrite Nat.sub_0_r.
Defined.

From CoqEAL Require hrel param refinements.

Global Set Bullet Behavior "Strict Subproofs".
Goal True /\ True.
split. - done. Abort.

Import hrel param refinements.
Import Refinements.Op.

Section Z_vector.
	Variable Z : Type.

	Context `{zero_of Z, add_of Z, mul_of Z}.
	Local Open Scope computable_scope.

	Definition vector_add n (v1 v2 : vector Z n) : vector Z n :=
		[vec of zip_with add_op v1 v2].	

	Definition vector_scale n (c : Z) (v : vector Z n) : vector Z n :=
		fmap (FMap:=vector_fmap) (mul_op c) v.

	Definition vector_total n (v : vector Z n) : Z := foldr add_op 0%C v.

	Definition inner_prod n (v1 v2 : vector Z n) : Z :=
		vector_total [vec of zip_with mul_op v1 v2].

	Lemma vector_add_lookup n v1 v2 (i : 'I_n) : (vector_add v1 v2) !# i = (v1 !# i + v2 !# i).
	Proof. unfold vector_add. by erewrite vector_zip_with_lookup. Qed.

	Lemma vector_scale_lookup c n (v : vector Z n) i : (vector_scale c v) !# i = (c * v !# i).
	Proof. unfold vector_scale. by erewrite vector_fmap_lookup. Qed.

	(* list_of_vector is a natural transformation *)
	Lemma list_of_vector_fmap A B (f : A → B) n (v : vector A n) :
		list_of_vector (fmap (FMap:=vector_fmap) f v) = f <$> (list_of_vector v).
	Proof. reflexivity. Qed.
End Z_vector.

Global Instance zero_Z : zero_of Z := 0%Z.
Global Instance one_Z : one_of Z := 1%Z.
Global Instance add_Z : add_of Z := Z.add.
Global Instance mul_Z : mul_of Z := Z.mul.
Global Instance opp_Z : opp_of Z := Z.opp.

Ltac simpC_Z := unfold add_op, add_Z, mul_op, mul_Z, zero_op, zero_Z, opp_op, opp_Z in *.

Section Z_vector_binint.
	Lemma vector_total_add n (v1 v2 : vector Z n) :
		vector_total (vector_add v1 v2) = (vector_total v1 + vector_total v2)%Z.
	Proof.
		revert n v1 v2. apply vector_induction2.
		{ done. }
		unfold vector_total; cbn; intros.
		simpC_Z.
		lia.
	Qed.

	Lemma vector_total_scale n x (v : vector Z n) :
		vector_total (vector_scale x v) = (x * vector_total v)%Z.
	Proof. apply Zsum_list_scale. Qed.

	Lemma inner_prod_comm n (v w : vector Z n) :
		inner_prod v w = inner_prod w v.
	Proof.
		unfold inner_prod; f_equal.
		eapply vector_lookup_eq; intros i.
		erewrite 2 vector_zip_with_lookup.
		apply Z.mul_comm.
	Qed.

	Lemma vector_scale_inner_prod_l n x (v w : vector Z n) :
		(x * inner_prod v w)%Z = inner_prod (vector_scale x v) w.
	Proof.
		unfold inner_prod, vector_scale; cbn.
		rewrite list_of_vector_fmap, zip_with_fmap_l.
		revert n v w; apply vector_induction2; cbn; simpC_Z; lia.
	Qed.

	Lemma vector_scale_inner_prod_r n x (v w : vector Z n) :
		(x * inner_prod v w)%Z = inner_prod v (vector_scale x w).
	Proof.
		by rewrite inner_prod_comm, vector_scale_inner_prod_l, inner_prod_comm.
	Qed.

	Lemma inner_prod_add_r n (v w1 w2 : vector Z n) :
		inner_prod v (vector_add w1 w2) = (inner_prod v w1 + inner_prod v w2)%Z.
	Proof.
		unfold inner_prod.
		rewrite <- vector_total_add.
		f_equal.
		eapply vector_lookup_eq; intros i.
		erewrite vector_zip_with_lookup, !vector_add_lookup, !vector_zip_with_lookup.
		apply Z.mul_add_distr_l.
	Qed.

	Lemma inner_prod_add_l n (v1 v2 w : vector Z n) :
		inner_prod (vector_add v1 v2) w = (inner_prod v1 w + inner_prod v2 w)%Z.
	Proof.
		by rewrite inner_prod_comm, inner_prod_add_r,
			(inner_prod_comm w v1), (inner_prod_comm w v2).
	Qed.

End Z_vector_binint.
